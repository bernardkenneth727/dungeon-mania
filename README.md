# Dungeon Mania 

- This project incorporates use of Java and object-oriented design to create a maze-like game where the player is confronted by obstacles and must reach the exit. 
- Object-oriented programming principles including abstraction, inheritance and encapsulation were used in the design of this project. 
- The UML diagram for this program is available and is called 'design.pdf'. The requireents were based on the assumptions in the 'assumptions.md' file. 

# Instructions to Run Game: 
1. Clone the gitlab repository to a location locally 
2. Open the 'src/main/java/App.java' file within VS code. 
3. Click the 'run' button and this will open the application. 






<img src="game_screen.png">