# Assumptions
Assumptions based on different parts of the spec.
## 3.1 - Character 
* There will be only 1 player in the game. 

## 3.2 - Static entity
* Door & Key
    * A key will only open 1 door 
    * Keys are specific to the door (i.e. door's key Id =  key Id)
* Boulder & Switch 
    * Boulders can go on any switch 
    * Only boulders can activate switches 
    * Only the player can move boulders 
* Zombie Toast Spawner
    * Player can walk on top of zombie spawners

## 3.3 - Moving Entity
* Player 
    * starts with 10.11 health points for standard 
    * starts with 8 health points for hard 
    * Has 1 attack damage  
* Spiders 
    * will deal damage 
    * Attackdmg is 1 point damage 
    * have 3 health points
* Zombies 
    * deal damage 
    * Attackdmg is 4 point damage 
    * have 5 health points
* Mercenary 
    * Attackdmg is 4 point damage 
    * have 5 health points
    * 1 treasure transforms a mercenary to an ally
*	Spawning Enemies
    *	Spiders spawn every 25 ticks
    *	There can be a maximum of 6 spiders at a time in the game
    *	Zombies only spawn from the zombie toast spawner
    *	Mercenaries spawn from where the player initially started from
    *	Mercenaries spawn every 45 ticks
    *   Mercenary will not spawn unless there is an enemy that is already or has already been present in the game
    *	Assassins have a 30% chance of spawning instead of a mercenary

## 3.4 - Collectable Entities
* Potion Assumptions
    * The Invisibility Potion and Invincibility Potion will have a limit of 10 ticks
    * When the player is invincible, the zombies and spiders don’t initiate battle or move onto player but otherwise keep to their normal path 
* Bomb 
    * When detonated, it will destroy ALL entities within a 2 block radius except for player 
    * Bombs can only be detonated when the player has placed down the bomb
* Sword 
    * Has durability of 10 hits
    * Has attack damage of 1.8
    * Picking up another sword will be a separate entity
* Armour
    * Has durability of 15 hits 
    * When a player defeats an enemy with armour that has not broken, the durability of the armour will not change. (i.e. enemy defeated with armour 5 durability left. Player now has armour of 5 durability)
    * Picking up more armour will be a separate entity
* Sun Stone 
    * Can be used instead of treasure for bribing and completing the treasure goal
* Anduril 
    * Does three times the amount of sword damage 

### 3.4 - Rare Collectable Entities 
* 15% chance of receiving a rare item upon winning a battle	
    * There is a 10% chance of winning The One Ring
    * There is a 5% chance of winning Anduril
* The One Ring when used will continue the battle as if the player now has full health

## 3.5 - Buildable Entities 
* Bow
    * Has a durability of 15 shots at an enemy and then it breaks and disappears from inventory
    * Does twice the attack of sword
    * Can shoot straight distances of any length (no diagonal)
* Shield
    * Does not have a durability / has unlimited uses project spec specifies that it must have a durability so will have 20.
    * Makes the enemy attacks 1/2 of the original attack
    * When crafting a shield, it prioritises using sun stone over treasure and treasure over key since keys can unlock doors and are unique
* Midnight Armour
    * Does not have a durability
    * Provides a protection level of 1.5 (between Armour and Shield)
    * Does damage half attack damage of sword
    * A player can have both midnight armour and armour and will get the effects of both.
* Sceptre
    * Does not have durability
    * Mind control is activated through the interact method, by choosing a mercenary or assassin
    * A player can use a sceptre to mind control any mercenary or assassin on the field, not just in their cardinal range of 2.
* Once a buildable has been made, the items used to make it are removed from the player’s inventory.

## 3.6 - Battles 
* Battle commences when a player and an enemy are adjacent
* The player moves onto enemy after wining battle
* The enemy moves onto player after wining battle
* When mercenary’s are in radius of a player, while the player moves at one block per tick, the enemy mercenary moves at 2 per tick

## 3.7 - Game Modes
* Peaceful Mode
    * Enemies will not initiate battle or attack player 
    * Player will essentially never die, battles will never occur if the character is in radius of enemy, only the player attacking the enemy. 
* Hard Mode
    * In hard mode, invincibility potions will still spawn but will have no effect 
    * Players will start with 20% less health points than standard mode (8 health points)

## 3.8 - Goals
* The order of the goals does not matter except getting to the exit must be completed last. 
* Unless the goal is specified to collect all treasures, not all treasures have to be collected to pass the level
* Unless the goal is specified to place all boulders on a switch, not all have to be moved. 

## Inventory
* You can't add moving or static entity to the inventory
* No limit to how much player can carry

## Dungeon Response
* When a new game is created, the map is instantly loaded. 

## Dungeon Mania Controller 
* The String input for dungeon will be the name of the map file needed (then when saving, it adds a time stamp)
* Attempting to interact with an entity that is not interactable with will not change interact but also not raise an exception.
* Dungeon Map size is maxed out at -50 and 50 in all directions 

## Milestone 3 
 
### 1.1 - Bosses
* Assassin 
    * Attackdmg is 6 point damage 
    * have 8 health points
    * Moves the same as the Mercenary
    * 1 treasure and 1 one ring transforms a assassin to an ally
    * when a Mercenary Spawns, there is a 30% chance that assassins will spawn instead
* Hydra 
    * Attackdmg is 5 point damage 
    * have 10 health points
    * Same rules for movement as the Zombie Toast
    * 50% chance of increasing health rather than decreasing health when attacked with normal weapons
    * Anduril has 100% chance of decreasing health
    * spawns every 50 ticks

### 1.2 - Path Finding 
* Swamp Tile 
    * All moving entities are affected by swamp tile except for Player
### 2.2 - Generate Dungeon 
* The generation of a map is between (0,0) and (50,50)
* There are is border of walls surrounding the whole map 
* Can only generate wall, player, exit and enemies through enemy spawner 
### 2.3 - Logic Switches 
* A electrical entity can only be one of AND, OR, XOR, NOT or CO_AND for logic. 
* When a switch is activated, all wires that are attached through a continuous path adjacent will also be activated
* Once an adjacent switch/wire has been deactiated, the lightbulb/doorswitch adjacent will also turn off if Logic conditions are not met
* Switch Doors do not have a key 
