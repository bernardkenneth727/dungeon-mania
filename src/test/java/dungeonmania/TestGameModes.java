package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import dungeonmania.entities.Inventory;
import dungeonmania.entities.Player;
import dungeonmania.entities.itemEntity.collectableEntity.potion.Potion;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Direction;

public class TestGameModes {


    @Test
    //Testing one zombie spawns at the 15th tick
    public void TestZombieSpawnHard() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("zombieSpawner", "hard"));

        // move the player and spawn a single zombie
        for (int i = 0; i < 15; i++) {
            dungeon.tick(null, Direction.DOWN);
        }
        //Checks that one zombie has spawned
        assertTrue(GameFile.getEntities().stream().anyMatch(e->e.getType().equals("zombie_toast")));

    }
    
    //assert right health for hard game mode
    @Test 
    public void TestHealthHard() {
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();

        // create new game with gamemode hard
        assertDoesNotThrow(() -> dungeon.newGame("healthpotion", "hard"));
        Player player = (Player) GameFile.getPlayer(GameFile.getEntities());

        //assert starting health
        assertTrue(player.getHealth() == 8); 
        
    }

    @Test //test invincible potion inability in hard mode
    public void TestInvincibilityHard() {
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();

        // create new game with gamemode hard
        assertDoesNotThrow(() -> dungeon.newGame("invinciblepotion", "hard"));
        GameFile game = dungeon.getCurrentGame();
        Player player = (Player) GameFile.getPlayer(GameFile.getEntities());
        String id = player.getId(); 

        // move into potion  
        dungeon.tick(null, Direction.RIGHT);

        //check inventory 
        Inventory inventory = player.getInventory(); 
        assertTrue(inventory.numberOf("invincibility_potion") > 0 ); 

        //use potion - mercenary should still move on player 
        Potion invincibilityPotion = (Potion) inventory.getItem("invincibility_potion");
        dungeon.tick(invincibilityPotion.getId(), Direction.NONE);

        //assert player is dead
        assertTrue(game.getEnt(id) == null );
    }


    @Test //test peaceful battle with enemies not attacking
    public void TestPeacefulBattle() {
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();

        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("invinciblepotion", "peaceful"));
        Player player = (Player) GameFile.getPlayer(GameFile.getEntities());
        double playerHealthPre = player.getHealth();

        // move into battle 
        dungeon.tick(null, Direction.RIGHT);
        dungeon.tick(null, Direction.RIGHT);
        dungeon.tick(null, Direction.RIGHT);


        //assert player health is full
        Player player2 = (Player) GameFile.getPlayer(GameFile.getEntities());
        double playerHealthPost = player2.getHealth();
        assertTrue(playerHealthPre == playerHealthPost);
    }
}
