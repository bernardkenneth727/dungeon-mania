package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


import org.junit.jupiter.api.Test;

import dungeonmania.exceptions.InvalidActionException;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Direction;
import dungeonmania.util.Position;


public class TestMercAlly {

    //Checks if the merc is now ally, based on number of treasures in inventory

    @Test
    public void TestMercBribe() {
        DungeonManiaController dungeon = new DungeonManiaController();
        
        assertDoesNotThrow(() -> dungeon.newGame("mercenary_bribe", "Standard"));
        GameFile game = dungeon.getCurrentGame();
        // Player has collected 2 treasures in inventory. 
        game.tick(Direction.RIGHT);

        // Player moves one more spot and now bribes mercenary
        game.tick(Direction.RIGHT);
        dungeon.interact("4");
        //Check that the mercenary is now ally. Have not figured out how we determine hes ally or not. maybe state pattern?
        //Checks invenroty no longer has coins
        assertTrue(game.getInventory().numberOf("treasure") == 1);
    }

    @Test
    public void TestNoLongerAttackAllyMerc() {

        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("mercenary_bribe", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // Mercenary should move one position towards player
        game.tick(Direction.RIGHT);
        game.tick(Direction.RIGHT);
        dungeon.interact("4");
        game.tick(Direction.DOWN);
        game.tick(Direction.RIGHT);
        game.tick(Direction.UP);
        assertTrue(GameFile.getEntities().stream().anyMatch(e->e.getType().equals("mercenary")));
        assertTrue(game.getPositionEntity("1").equals(game.getPositionEntity("4")));
    }



    @Test
    public void TestMultipleAllies() {

        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("mercenaryAllyMultiple", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // Mercenary should move one position towards player
        game.tick(Direction.RIGHT);
        game.tick(Direction.RIGHT);
        dungeon.interact("4");
        game.tick(Direction.RIGHT);
        dungeon.interact("5");
        game.tick(Direction.RIGHT);
        game.tick(Direction.UP);

        assertTrue(game.getPositionEntity("4").equals(new Position(4, 0))); 
    }

    @Test
    //If player does not have enough gold, exception is thrown
    //Directions may not be correct 
    public void TestNotEnoughGold() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("mercenary_bribe", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // Mercenary should move one position towards player
        game.tick(Direction.DOWN);
        game.tick(Direction.RIGHT);
        game.tick(Direction.RIGHT);
        // Player should have no coins in inventory
        assertTrue(game.getInventory().numberOf("treasure") == 0);

        // Player should not be able to interact with mercenary
        assertThrows(InvalidActionException.class, () -> dungeon.interact("4"));
    }

    @Test
    //If player has enough gold but is not adjacent to the given entity
    public void TestNotAdjacentMerc() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("mercenary_bribe", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // Mercenary should move one position towards player
        game.tick(Direction.RIGHT);
        // Player should have no coins in inventory
        assertTrue(game.getInventory().numberOf("treasure") == 1);

        // Player should not be able to interact with mercenary
        assertThrows(InvalidActionException.class, () -> dungeon.interact("4"));
    }
}