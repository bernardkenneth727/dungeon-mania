package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import dungeonmania.exceptions.InvalidActionException;
import dungeonmania.gameFile.GameFile;
import dungeonmania.response.models.DungeonResponse;
import dungeonmania.util.Direction;

@TestInstance(value = Lifecycle.PER_CLASS)

public class TestSceptre {
    @Test
    // Testing building a sceptre.
    public void testBuildSceptre() {
        // The initial state of the game has nothing in the player's inventory.
        DungeonManiaController game = new DungeonManiaController();
        DungeonResponse initialState = game.newGame("sceptre_basic", "Peaceful");
        assertTrue(initialState.getInventory().isEmpty());

        // After moving 3 spots to the right, the player should have all the items in their inventory needed to make a sceptre.
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        DungeonResponse gottenBuildingItems = game.tick(null, Direction.RIGHT);
        assertEquals(3, gottenBuildingItems.getInventory().size());
        assertTrue(gottenBuildingItems.getInventory().stream().anyMatch(e -> e.getType().equals("wood")));
        assertTrue(gottenBuildingItems.getInventory().stream().anyMatch(e -> e.getType().equals("key")));
        assertTrue(gottenBuildingItems.getInventory().stream().anyMatch(e -> e.getType().equals("sun_stone")));

        // The player should now be able to build a sceptre and we check that it is in the player's inventory.
        assertDoesNotThrow(() -> {
            DungeonResponse builtSceptre = game.build("sceptre");
            assertTrue(builtSceptre.getInventory().stream().anyMatch(e -> e.getType().equals("sceptre")));
            assertEquals(1, builtSceptre.getInventory().size());
        });

    }

    @Test
    // Testing using a sceptre to make a mercenary an ally.
    public void testMercenaryAlly() {
        DungeonManiaController game = new DungeonManiaController();
        DungeonResponse initialState = game.newGame("sceptre_basic", "Standard");
        assertTrue(initialState.getInventory().isEmpty());

        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        

        // Building a sceptre.
        DungeonResponse builtSceptre = game.build("sceptre");
        assertTrue(builtSceptre.getInventory().stream().anyMatch(e -> e.getType().equals("sceptre")));

        // Getting the id of the mercenary to interact with in order to mind control or make as an ally.
        String mercenaryId = builtSceptre.getEntities()
            .stream()
            .filter(e -> e.getType().equals("mercenary"))
            .findFirst()
            .get()
            .getId();
        
        // Getting the health of the player before interacting with the mercenary.
        double playerHealthBeforeInteraction = GameFile.getPlayer(GameFile.getEntities()).getHealth();

        // Showing that the player has now interacted with the mercenary, with a sceptre.
        assertDoesNotThrow(() -> game.interact(mercenaryId));

        // After the mercenary has been mind controlled and is an ally, the player's health should not change.
        game.tick(null, Direction.UP);
        game.tick(null, Direction.DOWN);
        assertEquals(GameFile.getPlayer(GameFile.getEntities()).getHealth(), playerHealthBeforeInteraction);

    }

    @Test
    // Testing using a sceptre to make an assassin an ally.
    public void testAssassinAlly() {
        DungeonManiaController game = new DungeonManiaController();
        game.newGame("sceptre_assassin", "Standard");

        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        

        // Building a sceptre.
        DungeonResponse builtSceptre = game.build("sceptre");
        assertTrue(builtSceptre.getInventory().stream().anyMatch(e -> e.getType().equals("sceptre")));

        // Getting the id of the mercenary to interact with in order to mind control or make as an ally.
        String assassinId = builtSceptre.getEntities()
            .stream()
            .filter(e -> e.getType().equals("assassin"))
            .findFirst()
            .get()
            .getId();
        
        // Getting the health of the player before interacting with the assassin.
        double playerHealthBeforeInteraction = GameFile.getPlayer(GameFile.getEntities()).getHealth();

        // Showing that the player can now interact with the assassin, with a sceptre.
        assertDoesNotThrow(() -> game.interact(assassinId));

        // After the assassin has been mind controlled and is an ally, the player's health should not change even though the player has gone over the assassin.
        game.tick(null, Direction.UP);
        game.tick(null, Direction.DOWN);
        assertEquals(GameFile.getPlayer(GameFile.getEntities()).getHealth(), playerHealthBeforeInteraction);

    }

    @Test
    // Testing controlling the mind of ally running out after 10 ticks.
    public void testOutOfTicks() {
        DungeonManiaController game = new DungeonManiaController();
        DungeonResponse initialState = game.newGame("sceptre_basic", "Standard");
        assertTrue(initialState.getInventory().isEmpty());

        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        

        // Building a sceptre.
        DungeonResponse builtSceptre = game.build("sceptre");
        assertTrue(builtSceptre.getInventory().stream().anyMatch(e -> e.getType().equals("sceptre")));

        // Getting the id of the mercenary to interact with in order to mind control or make as an ally.
        String mercenaryId = builtSceptre.getEntities()
            .stream()
            .filter(e -> e.getType().equals("mercenary"))
            .findFirst()
            .get()
            .getId();
        
        // Getting the health of the player before interacting with the mercenary.
        double playerHealthBeforeInteraction = GameFile.getPlayer(GameFile.getEntities()).getHealth();

        // Showing that the player has now interacted with the mercenary, with a sceptre.
        assertDoesNotThrow(() -> game.interact(mercenaryId));
        
        // The player has moved 10 times, 5 times up and 5 times down, so 10 ticks have passed.
        for (int i = 0; i < 5; i++) {
            game.tick(null, Direction.DOWN);
            game.tick(null, Direction.UP);
        } 

        game.tick(null, Direction.DOWN);
        game.tick(null, Direction.UP);
        
        // Getting the health of the player after now battling with the mercenary, since the player's mind control ran out.
        double playerHealthAfterOutOfTicks = GameFile.getPlayer(GameFile.getEntities()).getHealth();
        assertTrue(playerHealthAfterOutOfTicks < playerHealthBeforeInteraction);
        

    }

    @Test
    // Testing not having enough resources to build a sceptre.
    public void testNotEnoughResourcesSceptre() {
        // Creating a new game and moving 1 spot to the right.
        DungeonManiaController game = new DungeonManiaController();
        game.newGame("sceptre_basic", "Peaceful");
        game.tick(null, Direction.RIGHT);

        // The player at this point has only collected 1 wood, so will be unable to build a sceptre.
        assertThrows(InvalidActionException.class, () -> game.build("sceptre"));

        // The player should not have a sceptre, but should still have a wood in their inventory.
        DungeonResponse failingToBuildSceptre = game.tick(null, Direction.DOWN);
        assertFalse(failingToBuildSceptre.getInventory().stream().anyMatch(e -> e.getType().equals("sceptre")));
        assertTrue(failingToBuildSceptre.getInventory().stream().anyMatch(e -> e.getType().equals("wood")));
    }

    @Test
    // Testing alternates to using wood and treasure, instead using arrows and a key.
    public void testBuildSceptreAlternates() {
        // Creating a new game and the player's inventory is empty.
        DungeonManiaController game = new DungeonManiaController();
        DungeonResponse initialState = game.newGame("sceptre_alternate", "Peaceful");
        assertTrue(initialState.getInventory().isEmpty());

        // The player is moving to collect all the items required to build a sceptre, 2 arrows, 1 treasure and a sun stone.
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        DungeonResponse gottenAllItems = game.tick(null, Direction.RIGHT);

        // The player should now have 4 items in their inventory including a treasure and 2 arrows.
        assertEquals(4, gottenAllItems.getInventory().size());
        assertTrue(gottenAllItems.getInventory().stream().anyMatch(e -> e.getType().equals("treasure")));
        assertEquals(2, gottenAllItems.getInventory().stream().filter(e -> e.getType().equals("arrow")).count());

        // The player has now built a sceptre.
        assertDoesNotThrow(() -> game.build("sceptre"));
        
        // The player now has a sceptre in their inventory, and only a sceptre, since all other items were used to make it.
        DungeonResponse afterBuilding = game.tick(null, Direction.DOWN);
        assertTrue(afterBuilding.getInventory().stream().anyMatch(e -> e.getType().equals("sceptre")));
        assertEquals(1, afterBuilding.getInventory().size());
    }

    @Test
    // Testing a second sun stone is used to make a sceptre instead of either treasure or key.
    public void testBuildSceptreTwoSunStone() {
        DungeonManiaController game = new DungeonManiaController();
        DungeonResponse initialState = game.newGame("sceptre_alternate", "Peaceful");
        assertTrue(initialState.getInventory().isEmpty());

        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        // Moving another spot to the right that has a sun stone.
        DungeonResponse gottenAllItems = game.tick(null, Direction.RIGHT);

        // The player now has 2 sun stones.
        assertEquals(2, gottenAllItems.getInventory().stream().filter(e -> e.getType().equals("sun_stone")).count());

        // The player should be able to build a sceptre.
        assertDoesNotThrow(() -> {
            DungeonResponse builtSceptre = game.build("sceptre");
            // The player has used all sun stones to built the sceptre.
            assertFalse(builtSceptre.getInventory().stream().anyMatch(e -> e.getType().equals("sun_stone")));
        });
        

    }

}
