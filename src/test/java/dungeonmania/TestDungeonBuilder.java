package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import dungeonmania.gameFile.GameFile;
import dungeonmania.response.models.DungeonResponse;
import dungeonmania.util.Direction;
import dungeonmania.util.Position;

@TestInstance(value = Lifecycle.PER_CLASS)
public class TestDungeonBuilder {
    @Test
    // Tests throwing an invalid gamemode exception.
    public void testInvalidGamemode() {
        DungeonManiaController game = new DungeonManiaController();

        assertThrows(IllegalArgumentException.class, () -> game.generateDungeon(3, 3, 45, 45, "insane"));
        assertThrows(IllegalArgumentException.class, () -> game.generateDungeon(3, 3, 45, 45, "super_hard"));
        assertThrows(IllegalArgumentException.class, () -> game.generateDungeon(3, 3, 45, 45, "crazy"));
        assertThrows(IllegalArgumentException.class, () -> game.generateDungeon(3, 3, 45, 45, "normal"));
    }

    @Test
    // Tests that there is an exit generated in the dungeon.
    public void testExitGenerated() {
        DungeonManiaController game = new DungeonManiaController();

        // Generates a new dungeon.
        DungeonResponse initialState = game.generateDungeon(7, 9, 45, 37, "Hard");

        // The dungeon has an exit.
        assertTrue(initialState.getEntities().stream().anyMatch(e -> e.getType().equals("exit")));
        
        // The exit position should be the same as the end x and y specified.
        Position exitPosition = initialState.getEntities()
            .stream()
            .filter(e -> e.getType().equals("exit"))
            .findFirst()
            .get()
            .getPosition();
        
        assertEquals(exitPosition, new Position(45, 37));
    }

    @Test
    // Tests that the player spawns correctly on the dungeon.
    public void testPlayerGenerated() {
        DungeonManiaController game = new DungeonManiaController();

        // Generates a new dungeon.
        DungeonResponse initialState = game.generateDungeon(11, 17, 29, 15, "Standard");

        // The dungeon has a player.
        assertTrue(initialState.getEntities().stream().anyMatch(e -> e.getType().equals("player")));
        
        // The player's position should be the same as the start x and y specified.
        Position playerPosition = initialState.getEntities()
            .stream()
            .filter(e -> e.getType().equals("player"))
            .findFirst()
            .get()
            .getPosition();
        
        assertEquals(playerPosition, new Position(11, 17));
        assertEquals(playerPosition, GameFile.getPlayer(GameFile.getEntities()).getPosition());

        // The player's invenory should be empty and the player should be able to go a direction without an exception being called.
        assertTrue(initialState.getInventory().isEmpty());

        assertDoesNotThrow(() -> {
            game.tick(null, Direction.RIGHT);
            game.tick(null, Direction.LEFT);
            game.tick(null, Direction.DOWN);
            game.tick(null, Direction.UP);
        });
    }

    @Test
    // Tests that enemies can still spawn.
    public void testEnemiesSpawn() {
        DungeonManiaController game = new DungeonManiaController();

        // Generates a new dungeon.
        game.generateDungeon(23, 9, 45, 37, "Peaceful");

        // Player moves 100 ticks.
        for (int i = 0; i < 100; i++) {
            game.tick(null, Direction.RIGHT);
        }

        // The game should now have at least 1 spider, mercenary or assassin that has not been defeated by the player.
        DungeonResponse afterAllSpawning = game.tick(null, Direction.RIGHT);
        assertTrue(
            afterAllSpawning.getEntities().stream().anyMatch(e -> e.getType().equals("spider")) 
            || afterAllSpawning.getEntities().stream().anyMatch(e -> e.getType().equals("mercenary")) 
            || afterAllSpawning.getEntities().stream().anyMatch(e -> e.getType().equals("assassin"))
        );

    }

    @Test
    // Tests that walls spawn.
    public void testWalls() {
        DungeonManiaController game = new DungeonManiaController();

        // Generates a new dungeon.
        DungeonResponse initialState = game.generateDungeon(13, 15, 17, 19, "Peaceful");

        // There should be walls in the dungeon, at least 196 since they are enforced around the maze.
        assertTrue(initialState.getEntities()
            .stream()
            .anyMatch(e -> e.getType().equals("wall")));
        
        assertTrue(initialState.getEntities().stream().filter(e -> e.getType().equals("wall")).count() >= 196);
    }

}