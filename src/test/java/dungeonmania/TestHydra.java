package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Direction;
import dungeonmania.util.Position;

public class TestHydra {
    
    @Test 
    public void testHydraSpawn() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();

        assertDoesNotThrow(() -> dungeon.newGame("assassinSpawner", "Hard"));

        for (int i = 0; i < 30; i++) {
            dungeon.tick(null, Direction.DOWN);
            dungeon.tick(null, Direction.RIGHT);
        }

        // check that hydra has spawned at least once 
        long counterFirst = GameFile.getEntities().stream().filter(t -> t.getType().equals("hydra")).collect(Collectors.counting());

        assertTrue(counterFirst == 1);
    }
    @Test 
    public void testHydraDoesNotSpawn() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();

        assertDoesNotThrow(() -> dungeon.newGame("assassinSpawner", "Standard"));

        for (int i = 0; i < 30; i++) {
            dungeon.tick(null, Direction.DOWN);
            dungeon.tick(null, Direction.RIGHT);
        }
        // check that hydra has not spawned at all
        long counterFirst = GameFile.getEntities().stream().filter(t -> t.getType().equals("hydra")).collect(Collectors.counting());
        assertTrue(counterFirst == 0);
    }


    @Test
    public void testHydraMovement() {
        // should be the same as Zombie 
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();

        assertDoesNotThrow(() -> dungeon.newGame("hydraMovement", "Hard"));

        dungeon.tick(null, Direction.RIGHT);
        GameFile game = dungeon.getCurrentGame();
        // check that the hydra has moved 
        assertFalse(game.getPositionEntity("2").equals(new Position(2, 1)));

    }
    @Test
    public void testHydraBattleSuccessfulAttack() {
        DungeonManiaController dungeon = new DungeonManiaController();
        for (int i = 0; i < 100; i++) {
            // create new game with gamemode standard
            dungeon.newGame("battleHydraWin", "Hard");
            
            dungeon.tick(null, Direction.LEFT);
            if (GameFile.getPlayer(GameFile.getEntities()) != null) {
                assertFalse(GameFile.getEntities().stream().anyMatch(e->e.getType().equals("hydra")));
            } else {
                assertTrue(GameFile.getPlayer(GameFile.getEntities()) == null);
                assertTrue(GameFile.getEntities().stream().anyMatch(e->e.getType().equals("hydra")));
            }
        }    
    }
    @Test
    public void testHydraBattleLoseAnduril() {
        DungeonManiaController dungeon = new DungeonManiaController();
                
        // create new game with gamemode standard
        dungeon.newGame("battleHydraLose", "Hard");

        dungeon.tick(null, Direction.LEFT);
        assertTrue(GameFile.getPlayer(GameFile.getEntities()) != null);
        assertFalse(GameFile.getEntities().stream().anyMatch(e->e.getType().equals("hydra")));
    }
}

