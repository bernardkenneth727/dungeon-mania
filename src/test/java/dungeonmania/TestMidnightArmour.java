package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import dungeonmania.exceptions.InvalidActionException;
import dungeonmania.gameFile.GameFile;
import dungeonmania.response.models.DungeonResponse;
import dungeonmania.util.Direction;

@TestInstance(value = Lifecycle.PER_CLASS)
public class TestMidnightArmour {
    @Test
    // Testing building midnight armour with the correct resources available.
    public void testBuildMidnightArmour() {
        DungeonManiaController game = new DungeonManiaController();
        game.newGame("midnight_armour", "Standard");

        // Moving onto the items for midnight armour, inventory should now be of size 2.
        game.tick(null, Direction.RIGHT);
        DungeonResponse gottenResourcesForMidnightArmour = game.tick(null, Direction.RIGHT);
        assertEquals(2, gottenResourcesForMidnightArmour.getInventory().size());

        // The player should now be able to build midnight armour without an exception being thrown and 
        // midnight armour should appear in the player's inventory.
        assertDoesNotThrow(() -> {
            DungeonResponse builtMidnightArmour = game.build("midnight_armour");
            assertTrue(builtMidnightArmour.getInventory()
                .stream()
                .anyMatch(e -> e.getType().equals("midnight_armour"))
            );
            assertEquals(1, builtMidnightArmour.getInventory().size());
        });

    }

    @Test
    // Testing not having enough resources to build a midnight armour.
    public void testNotEnoughResourcesBuildMidnightArmour() {
        DungeonManiaController game = new DungeonManiaController();
        game.newGame("midnight_armour", "Standard");

        // Only getting one of the items for midnight armour, the player's inventory should only contain 1 item.
        DungeonResponse someItemsGotten = game.tick(null, Direction.RIGHT);
        assertEquals(1, someItemsGotten.getInventory().size());

        // Attempting to build a midnight armour should throw an exception that the player does not have enough resources to build a midnight armour.
        assertThrows(InvalidActionException.class, () -> game.build("midnight_armour"));
        
        // The player should now not have midnight armour in their inventory.
        DungeonResponse notBuiltMidnightArmour = game.tick(null, Direction.DOWN);
        assertFalse(notBuiltMidnightArmour.getInventory().stream().anyMatch(e -> e.getType().equals("midnight_armour")));
    }

    @Test
    // Testing not being able to build midnight armour due to zombies being in game.
    public void testNotBuildMidnightArmourZombies() {
        DungeonManiaController game = new DungeonManiaController();
        game.newGame("midnight_armour_zombie", "Standard");

        // Moving onto the items for midnight armour, inventory should now be of size 2.
        game.tick(null, Direction.RIGHT);
        DungeonResponse gottenResourcesForMidnightArmour = game.tick(null, Direction.RIGHT);
        assertEquals(2, gottenResourcesForMidnightArmour.getInventory().size());

        // Showing that there is a zombie in the map.
        assertTrue(gottenResourcesForMidnightArmour.getEntities()
            .stream()
            .anyMatch(e -> e.getType().equals("zombie_toast"))
        );

        // The player will now attempt to make a midnight armour, but will fail as there is a 
        // zombie in this map.
        assertThrows(InvalidActionException.class, () -> game.build("midnight_armour"));
        
        DungeonResponse afterBuildAttempt = game.tick(null, Direction.DOWN);
        assertFalse(afterBuildAttempt.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("midnight_armour"))
        );
    }

    @Test
    // Testing battling with only midnight armour, since it provides protection and weapon damage.
    public void testBattleMidnightArmour() {
        // Testing a game which has a built midnight armour on the player battling a spider versus another game without the midnight armour on battling the spider and comparing health.
        DungeonManiaController game1 = new DungeonManiaController();

        game1.newGame("midnight_armour_enemies", "Standard");

        game1.tick(null, Direction.RIGHT);
        game1.tick(null, Direction.RIGHT);

        // Showing the player now has a midnight armour.
        DungeonResponse builtMidnightArmour = game1.build("midnight_armour");
        assertTrue(builtMidnightArmour.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("midnight_armour"))
        );

        game1.tick(null, Direction.RIGHT);
        game1.tick(null, Direction.RIGHT);

        // Getting the player's health after battling the spider with midnight armour.
        double playerHealth1 = GameFile.getPlayer(GameFile.getEntities()).getHealth();

        // Testing the second game without the midnight armour.
        DungeonManiaController game2 = new DungeonManiaController();

        game2.newGame("midnight_armour_enemies", "Standard");

        game2.tick(null, Direction.RIGHT);
        game2.tick(null, Direction.RIGHT);
        game2.tick(null, Direction.RIGHT);
        // The player should not have midnight armour in their inventory.
        DungeonResponse notHavingMidnightArmour = game2.tick(null, Direction.RIGHT);
        assertFalse(notHavingMidnightArmour.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("midnight_armour"))
        );

        // Getting the health for the player after battling the spider without the midnight armour.
        double playerHealth2 = GameFile.getPlayer(GameFile.getEntities()).getHealth();

        // The player's health from game 1 should be greater than game 2 since in game 1 the player had midnight armour.
        assertNotEquals(playerHealth1, playerHealth2);
        assertTrue(playerHealth1 > playerHealth2);


    }

}
