package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import dungeonmania.gameFile.*;
import dungeonmania.util.Direction;

public class TestBattle {
    
    @Test
    public void TestBattleMercenaryPlayerWin() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
                
        // create new game with gamemode standard
        dungeon.newGame("battleMercenary", "Standard");
        GameFile game = dungeon.getCurrentGame();
        if (game.getEnt("2").getHasArmour()) {
            dungeon.tick(null, Direction.RIGHT);
            System.out.println("in here");
            assertTrue(GameFile.getPlayer(GameFile.getEntities()) == null);
        }
        else {
            dungeon.tick(null, Direction.RIGHT);
            assertTrue(GameFile.getPlayer(GameFile.getEntities()) != null);
            assertFalse(GameFile.getEntities().stream().anyMatch(e->e.getType().equals("mercenary")));
        }
    }


    @Test
    public void TestBattleSpiderPlayerWin() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
                
        // create new game with gamemode standard
        dungeon.newGame("battleSpider", "Standard");
        dungeon.tick(null, Direction.LEFT);
        assertTrue(GameFile.getPlayer(GameFile.getEntities()) != null);
        assertFalse(GameFile.getEntities().stream().anyMatch(e->e.getType().equals("spider")));
    }

    @Test
    public void TestBattleZombieAdjacentPlayerWin() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
                
        // create new game with gamemode standard
        dungeon.newGame("battleZombieAdjacent", "Standard");
        GameFile game = dungeon.getCurrentGame();
        
        if (game.getEnt("2").getHasArmour()) {
            dungeon.tick(null, Direction.LEFT);
            assertTrue(GameFile.getPlayer(GameFile.getEntities()) == null);
        }
        else {
            dungeon.tick(null, Direction.LEFT);
            assertTrue(GameFile.getPlayer(GameFile.getEntities()) != null);
            assertFalse(GameFile.getEntities().stream().anyMatch(e->e.getType().equals("zombie_toast")));
        }
    }

}
