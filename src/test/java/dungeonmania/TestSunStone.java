package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import dungeonmania.gameFile.GameFile;
import dungeonmania.response.models.DungeonResponse;
import dungeonmania.util.Direction;
import dungeonmania.util.Position;

public class TestSunStone {

    @Test
    // Testing for unlocking a door with sunstone 
    public void TestUnlock() {
        DungeonManiaController game = new DungeonManiaController();

        game.newGame("doors_and_sunstone", "standard");

        // Getting the dungeon responses after getting stone and then moving through a door.
        DungeonResponse afterCollectingStone = game.tick(null, Direction.RIGHT);
        DungeonResponse afterMovingThroughDoor = game.tick(null, Direction.RIGHT);

        // Getting the positions of player at these responses.
        Position playerPositionAfterCollectingStone = afterCollectingStone.getEntities()
            .stream()
            .filter(e -> e.getType().equals("player"))
            .findFirst()
            .get()
            .getPosition();
        
        Position playerPositionAfterMovingThroughDoor = afterMovingThroughDoor.getEntities()
        .stream()
        .filter(e -> e.getType().equals("player"))
        .findFirst()
        .get()
        .getPosition();

        // The player has now moved through the door, so the player's positions for those should be false, but the player is adjacent to their position before.
        assertFalse(playerPositionAfterCollectingStone.equals(playerPositionAfterMovingThroughDoor));
        assertTrue(Position.isAdjacent(playerPositionAfterMovingThroughDoor, playerPositionAfterCollectingStone));
    }

    @Test
    public void TestSunstoneMercBribe() {
        DungeonManiaController dungeon = new DungeonManiaController();
        
        assertDoesNotThrow(() -> dungeon.newGame("mercenary_sunstone", "Standard"));
        GameFile game = dungeon.getCurrentGame();
        
        // Mercenary should move one position towards player
        game.tick(Direction.RIGHT);
        game.tick(Direction.RIGHT);
        dungeon.interact("3");
        game.tick(Direction.DOWN);
        game.tick(Direction.RIGHT);
        game.tick(Direction.UP);
        assertTrue(GameFile.getEntities().stream().anyMatch(e->e.getType().equals("mercenary")));
        assertTrue(game.getPositionEntity("1").equals(game.getPositionEntity("3")));
    }
    

}


