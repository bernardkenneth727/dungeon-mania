package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import dungeonmania.entities.SwitchDoor;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Direction;

public class TestLogicSwitches {
    @Test 
    public void TestANDLightAdj() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("lightAdjacentAND", "Standard");

        GameFile game = dungeon.getCurrentGame();
       
        assertTrue(game.getEnt("4").getType().equals("light_bulb_off"));

        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("4").getType().equals("light_bulb_off"));

        dungeon.tick("", Direction.UP);
        dungeon.tick("", Direction.RIGHT);
        dungeon.tick("", Direction.RIGHT);
        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("4").getType().equals("light_bulb_on"));

        // take one boulder off light should go off
        dungeon.tick("", Direction.RIGHT);
        dungeon.tick("", Direction.DOWN);
        dungeon.tick("", Direction.LEFT);
        assertTrue(game.getEnt("4").getType().equals("light_bulb_off"));
    }
    @Test 
    public void TestORLightAdj() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("lightAdjacentOR", "Standard");

        GameFile game = dungeon.getCurrentGame();
        
        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("4").getType().equals("light_bulb_on"));
        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("4").getType().equals("light_bulb_off"));

        dungeon.tick("", Direction.UP);
        dungeon.tick("", Direction.UP);
        dungeon.tick("", Direction.RIGHT);
        dungeon.tick("", Direction.RIGHT);

        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("4").getType().equals("light_bulb_on"));
        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("4").getType().equals("light_bulb_off"));
    }
    @Test 
    public void TestXORLightAdj() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("lightAdjacentXOR", "Standard");

        GameFile game = dungeon.getCurrentGame();

        assertTrue(game.getEnt("4").getType().equals("light_bulb_off"));

        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("4").getType().equals("light_bulb_on"));

        dungeon.tick("", Direction.UP);
        dungeon.tick("", Direction.RIGHT);
        dungeon.tick("", Direction.RIGHT);
        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("4").getType().equals("light_bulb_off"));

        // take one boulder off light should go off
        dungeon.tick("", Direction.RIGHT);
        dungeon.tick("", Direction.DOWN);
        dungeon.tick("", Direction.LEFT);
        assertTrue(game.getEnt("4").getType().equals("light_bulb_on"));

    }
    @Test 
    public void TestNOTLightAdj() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("lightAdjacentNOT", "Standard");

        GameFile game = dungeon.getCurrentGame();
        
        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("4").getType().equals("light_bulb_off"));
        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("4").getType().equals("light_bulb_on"));

        dungeon.tick("", Direction.UP);
        dungeon.tick("", Direction.UP);
        dungeon.tick("", Direction.RIGHT);
        dungeon.tick("", Direction.RIGHT);

        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("4").getType().equals("light_bulb_off"));
        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("4").getType().equals("light_bulb_on"));
    }
    @Test 
    public void TestCO_ANDpass() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("lightCOAND", "Standard");

        GameFile game = dungeon.getCurrentGame();

        assertTrue(game.getEnt("10").getType().equals("light_bulb_off"));

        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("10").getType().equals("light_bulb_on"));

        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("10").getType().equals("light_bulb_off"));
    }
    @Test 
    public void TestANDLightWire() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("lightWireAND", "Standard");

        GameFile game = dungeon.getCurrentGame();
       
        assertTrue(game.getEnt("10").getType().equals("light_bulb_off"));

        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("10").getType().equals("light_bulb_on"));

        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("10").getType().equals("light_bulb_off"));

    }
    @Test 
    public void TestXORLightWire() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("lightWireXOR", "Standard");

        GameFile game = dungeon.getCurrentGame();
        assertTrue(game.getEnt("10").getType().equals("light_bulb_off"));
        assertTrue(game.getEnt("11").getType().equals("light_bulb_off"));

        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("10").getType().equals("light_bulb_off"));
        assertTrue(game.getEnt("11").getType().equals("light_bulb_off"));

    }
    @Test 
    public void TestNOTLightWire() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("lightWireNOT", "Standard");

        GameFile game = dungeon.getCurrentGame();
        
        assertTrue(game.getEnt("10").getType().equals("light_bulb_off"));

        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("10").getType().equals("light_bulb_off"));

        dungeon.tick("", Direction.DOWN);
        assertTrue(game.getEnt("10").getType().equals("light_bulb_on"));
    }

///////////////////////////// Door Switch Tests ////////////////////////////
    
    @Test 
    public void TestORDoorAdj() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("doorAdjacentOR", "Standard");
        
        assertTrue(dungeon.getCurrentGame().getEnt("4").getType().equals("switch_door"));

        dungeon.tick("", Direction.DOWN);
        SwitchDoor door = (SwitchDoor) dungeon.getCurrentGame().getEnt("4");
        assertTrue(door.getIsOn());
        dungeon.tick("", Direction.DOWN);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("4");
        assertTrue(door.getIsOn() == false);

        dungeon.tick("", Direction.UP);
        dungeon.tick("", Direction.UP);
        dungeon.tick("", Direction.RIGHT);
        dungeon.tick("", Direction.RIGHT);

        dungeon.tick("", Direction.DOWN);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("4");
        assertTrue(door.getIsOn());
        dungeon.tick("", Direction.DOWN);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("4");
        assertTrue(door.getIsOn() == false);
    }
    @Test 
    public void TestXORDoorAdj() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("doorAdjacentXOR", "Standard");

        assertTrue(dungeon.getCurrentGame().getEnt("4").getType().equals("switch_door"));

        SwitchDoor door = (SwitchDoor) dungeon.getCurrentGame().getEnt("4");
        assertTrue(door.getIsOn() == false);

        dungeon.tick("", Direction.DOWN);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("4");
        assertTrue(door.getIsOn());

        dungeon.tick("", Direction.UP);
        dungeon.tick("", Direction.RIGHT);
        dungeon.tick("", Direction.RIGHT);
        dungeon.tick("", Direction.DOWN);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("4");
        assertTrue(door.getIsOn() == false);

        // take one boulder off light should go on
        dungeon.tick("", Direction.RIGHT);
        dungeon.tick("", Direction.DOWN);
        dungeon.tick("", Direction.LEFT);
        dungeon.tick("", Direction.LEFT);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("4");
        assertTrue(door.getIsOn());
    }
    @Test 
    public void TestNOTDoorAdj() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("doorAdjacentNOT", "Standard");

        assertTrue(dungeon.getCurrentGame().getEnt("4").getType().equals("switch_door"));

        SwitchDoor door = (SwitchDoor) dungeon.getCurrentGame().getEnt("4");
        assertTrue(door.getIsOn() == false);

        dungeon.tick("", Direction.DOWN);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("4");
        assertTrue(door.getIsOn() == false);
        dungeon.tick("", Direction.DOWN);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("4");
        assertTrue(door.getIsOn());

        dungeon.tick("", Direction.UP);
        dungeon.tick("", Direction.UP);
        dungeon.tick("", Direction.RIGHT);
        dungeon.tick("", Direction.RIGHT);

        dungeon.tick("", Direction.DOWN);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("4");
        assertTrue(door.getIsOn() == false);
        dungeon.tick("", Direction.DOWN);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("4");
        assertTrue(door.getIsOn());
    }
    @Test 
    public void TestCO_ANDDoorpass() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("doorCOAND", "Standard");

        assertTrue(dungeon.getCurrentGame().getEnt("10").getType().equals("switch_door"));

        SwitchDoor door = (SwitchDoor) dungeon.getCurrentGame().getEnt("10");
        assertTrue(door.getIsOn() == false);

        dungeon.tick("", Direction.DOWN);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("10");
        assertTrue(door.getIsOn());

        dungeon.tick("", Direction.DOWN);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("10");
        assertTrue(door.getIsOn() == false);
    }
    @Test 
    public void TestANDDoorWire() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("doorWireAND", "Standard");

        assertTrue(dungeon.getCurrentGame().getEnt("10").getType().equals("switch_door"));

        SwitchDoor door = (SwitchDoor) dungeon.getCurrentGame().getEnt("10");
        assertTrue(door.getIsOn() == false);

        dungeon.tick("", Direction.DOWN);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("10");
        assertTrue(door.getIsOn());

        dungeon.tick("", Direction.DOWN);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("10");
        assertTrue(door.getIsOn() == false);

    }
    @Test 
    public void TestXORDoorWire() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("doorWireXOR", "Standard");

        assertTrue(dungeon.getCurrentGame().getEnt("10").getType().equals("switch_door"));

        SwitchDoor door = (SwitchDoor) dungeon.getCurrentGame().getEnt("10");
        assertTrue(door.getIsOn() == false);

        dungeon.tick("", Direction.DOWN);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("10");
        assertTrue(door.getIsOn() == false);

    }
    @Test 
    public void TestNOTDoorWire() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("doorWireNOT", "Standard");
        
        SwitchDoor door = (SwitchDoor) dungeon.getCurrentGame().getEnt("10");
        assertTrue(door.getIsOn() == false);

        dungeon.tick("", Direction.DOWN);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("10");
        assertTrue(door.getIsOn() == false);

        dungeon.tick("", Direction.DOWN);
        door = (SwitchDoor) dungeon.getCurrentGame().getEnt("10");
        assertTrue(door.getIsOn());
    }
}
