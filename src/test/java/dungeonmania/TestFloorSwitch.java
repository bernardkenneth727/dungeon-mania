package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import dungeonmania.response.models.DungeonResponse;
import dungeonmania.response.models.EntityResponse;
import dungeonmania.util.Direction;
import dungeonmania.util.Position;

@TestInstance(value = Lifecycle.PER_CLASS)
public class TestFloorSwitch{

    @Test
    // Testing right position of switch
    public void testSwitchPosition() {
        DungeonManiaController game = new DungeonManiaController();

        game.newGame("boulders", "Standard");

        // Moves to the spot before moving a boulder onto a switch.
        DungeonResponse response = game.tick(null, Direction.RIGHT);
        List<EntityResponse> switchTest = response.getEntities();

        // Shows that at the position desired to trigger the switch, there is currently a switch.
        assertTrue(
            switchTest.stream().anyMatch(e -> e.getPosition().equals(new Position(5, 3)) && e.getType().equals("switch"))
        );
        
    }


   
    @Test
    // Testing boulder can be on switch.
    public void testBoulderOnSwitch() {
        DungeonManiaController game = new DungeonManiaController();

        game.newGame("boulders", "Standard");

        // Moves the boulder onto the switch to be triggered.
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.DOWN);
        DungeonResponse afterSwitch = game.tick(null, Direction.RIGHT);
        List<EntityResponse> afterSwitchEntities = afterSwitch.getEntities();

        // Shows that at that position is shared by boulder & switch
        assertTrue(
            afterSwitchEntities.stream().anyMatch(e -> e.getPosition().equals(new Position(5, 3)) && e.getType().equals("boulder"))    
        );
        assertTrue(
            afterSwitchEntities.stream().anyMatch(e -> e.getPosition().equals(new Position(5, 3)) && e.getType().equals("switch"))  
        );

    }


}
