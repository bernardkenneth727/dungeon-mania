
package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import dungeonmania.entities.*;
import dungeonmania.entities.enemy.Enemy;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.*;

public class TestZombie {
    @Test
    //Testing one zombie can move and does not remain in same position
    public void TestZombieMoves() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("zombieMovement", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // move the player and spawn a single zombie
        game.tick(Direction.UP);
        assertFalse(game.getPositionEntity("2").equals(new Position(2, 1)));
    }


    @Test
    //Testing one zombie cannot move through walls
    public void TestZombieWall() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("zombieWallBoulder", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // move the player and spawn a single zombie
        game.tick(Direction.UP);
        assertTrue(game.getPositionEntity("2").equals(new Position(0, 0)));
    }

    @Test
    //Testing one zombie cannot move through doors
    public void TestZombieDoor() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("zombieDoor", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // move the player and spawn a single zombie
        game.tick(Direction.UP);
        game.tick(Direction.LEFT);
        game.tick(Direction.LEFT);
        game.tick(Direction.RIGHT);
        game.tick(Direction.RIGHT);
        game.tick(Direction.RIGHT);
        game.tick(Direction.RIGHT);
        game.tick(Direction.RIGHT);
        game.tick(Direction.RIGHT); 
        game.tick(Direction.RIGHT);
        game.tick(Direction.RIGHT);
        game.tick(Direction.RIGHT); 
        game.tick(Direction.RIGHT); 
        game.tick(Direction.RIGHT); 
        game.tick(Direction.RIGHT); 
        game.tick(Direction.RIGHT);
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("2").getX() > (new Position(2, 0).getX()));
    }

    @Test
    //Testing one zombie cannot move through walls
    public void TestZombieDoorClosed() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("zombieDoorClosed", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // move the player and spawn a single zombie
        game.tick(Direction.UP);

        assertTrue(game.getPositionEntity("2").equals(new Position(0, 0)));
    }

    @Test
    //Testing one zombie cannot move through walls
    public void TestZombieOnEnemy() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("enemyEnclosed", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // move the player and spawn a single zombie
        game.tick(Direction.UP);
        game.tick(Direction.UP);

        assertTrue(game.getPositionEntity("2").equals(new Position(0, 0)));
    }

    @Test
    public void TestZombieOnBoulder() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("zombieWallBoulder", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        
        game.tick(Direction.UP);
        game.tick(Direction.UP);

        assertTrue(game.getPositionEntity("2").equals(new Position(0, 0)));
    }

    @Test
    //Checks if armour is spwned on zombies
    public void TestRandomZombieArmour() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("zombieArmour", "Standard"));

        int counter = 0;
        List<Entity> zombies = GameFile.getEntities().stream().filter(p -> p.getType().equals("zombie_toast")).collect(Collectors.toList());
        for (Entity zombie : zombies) {
            Enemy zomb = (Enemy) zombie;
            if (zomb.isHasArmour()) {
                counter++;
            }
        }
        assertTrue(counter != 0);
        
     }
}
