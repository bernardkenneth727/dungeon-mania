package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import dungeonmania.gameFile.*;
import dungeonmania.gameFile.goals.*;
import dungeonmania.util.Direction;

public class TestGoals {

    @Test 
    public void testGoalsDetails() {
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("boulders", "Standard"));
        GameFile game = dungeon.getCurrentGame();
        String goalStr = game.getGoals();
        GoalController goal = game.getGoal();

        // check that the goal information is correct
        assertTrue(goalStr != null);
        assertTrue(goalStr.equals(":boulders"));
        assertTrue(goal.getGoal().equals("boulders"));
        // check that the goal is of the correct type
        assertTrue(goal.getGoalDefined() instanceof BoulderGoal);
    }

    @Test 
    public void testGoalCompletionExit() {
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("exitGoal", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        String goalStr = game.getGoals();
        GoalController goal = game.getGoal();

        // check that the goal information is correct
        assertTrue(goalStr != null);
        assertTrue(goalStr.equals(":exit"));
        assertTrue(goal.getGoal().equals("exit"));
        // check that the goal is of the correct type
        assertTrue(goal.getGoalDefined() instanceof ExitGoal);

        //assert goal completion
        assertTrue(game.checkGoalComplete() == false);
        
        //walk towards exit
        dungeon.tick("none", Direction.DOWN);
        assertTrue(game.checkGoalComplete() == true); 
    }

    @Test
    public void testGoalCompleteTreasure() {
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();
                
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("treasureGoal", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        String goalStr = game.getGoals();
        GoalController goal = game.getGoal();

        // check that the goal information is correct
        assertTrue(goalStr != null);
        assertTrue(goalStr.equals(":treasure"));
        assertTrue(goal.getGoal().equals("treasure"));

        //assert goal completion
        assertTrue(game.checkGoalComplete() == false);

        dungeon.tick("none", Direction.DOWN); // pick up treasure
        assertTrue(game.checkGoalComplete() == true);
    }

    @Test
    public void testGoalCompleteBoulder() {
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();
                
        // create new game with gamemode standard
        //assertDoesNotThrow(() -> dungeon.newGame("switchGoal", "Standard"));
        dungeon.newGame("switchGoal", "Standard");
        GameFile game = dungeon.getCurrentGame();

        String goalStr = game.getGoals();
        GoalController goal = game.getGoal();

        // check that the goal information is correct
        assertTrue(goalStr != null);
        assertTrue(goalStr.equals(":boulders"));
        assertTrue(goal.getGoal().equals("boulders"));

        //assert goal completion
        assertTrue(game.checkGoalComplete() == false);

        dungeon.tick("none", Direction.DOWN);
        assertTrue(game.checkGoalComplete() == true);
    }

    @Test
    public void testGoalCompleteEnemy() {
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();
                
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("enemyGoal", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        String goalStr = game.getGoals();
        GoalController goal = game.getGoal();

        // check that the goal information is correct
        assertTrue(goalStr != null);
        assertTrue(goalStr.equals(":enemies"));
        assertTrue(goal.getGoal().equals("enemies"));

        //assert goal completion
        assertTrue(game.checkGoalComplete() == false);

        dungeon.tick("none", Direction.RIGHT); // pick up sword
        dungeon.tick("none", Direction.DOWN); // battle mercenary and win

        assertTrue(game.checkGoalComplete() == true);
    }

    @Test
    public void testGoalCompleteTreasureANDEnemy() {
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();
                
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("treasureANDEnemyGoal", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        String goalStr = game.getGoals();
        
        // check that the goal information is correct
        assertTrue(goalStr != null);
        assertTrue(goalStr.equals(":enemies AND :treasure"));

        //assert goal completion
        assertTrue(game.checkGoalComplete() == false);

        dungeon.tick("none", Direction.DOWN); // pick up sword 
        dungeon.tick("none", Direction.DOWN); // battle mercenary
        dungeon.tick("none", Direction.RIGHT);
        dungeon.tick("none", Direction.RIGHT);
        dungeon.tick("none", Direction.UP);
        dungeon.tick("none", Direction.UP); // pick up treasure

        assertTrue(game.checkGoalComplete() == true);
    }

    @Test
    public void testGoalCompleteTreasureOREnemy1() {
    // COMPLETE GOAL BY DEFEATING ENEMY
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();
                
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("treasureOREnemyGoal", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        String goalStr = game.getGoals();

        // check that the goal information is correct
        assertTrue(goalStr != null);
        assertTrue(goalStr.equals(":enemies OR :treasure"));

        //assert goal completion
        assertTrue(game.checkGoalComplete() == false);
        dungeon.tick("none", Direction.DOWN); // pick up sword
        dungeon.tick("none", Direction.DOWN); // battle mercenary
        assertTrue(game.checkGoalComplete() == true);
    }

    @Test //fix
    public void testGoalCompleteTreasureOREnemy2() {
    // COMPLETE GOAL BY PICKING UP TREASURE
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();
                
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("treasureOREnemyGoal", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        String goalStr = game.getGoals();

        // check that the goal information is correct
        assertTrue(goalStr != null);
        assertTrue(goalStr.equals(":enemies OR :treasure"));

        //assert goal completion
        assertTrue(game.checkGoalComplete() == false);
        dungeon.tick("none", Direction.RIGHT); // pick up treasure
        dungeon.tick("none", Direction.RIGHT);
        assertTrue(game.checkGoalComplete() == true);
    }


    @Test
    public void testGoalCompleteSwitchANDExit() {
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();
                
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("boulderExitGoal", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        String goalStr = game.getGoals();

        // check that the goal information is correct
        assertTrue(goalStr != null);
        assertTrue(goalStr.equals(":exit AND :boulders"));

        //assert goal completion
        assertTrue(game.checkGoalComplete() == false);
        dungeon.tick("none", Direction.RIGHT); // push boulder onto switch 
        dungeon.tick("none", Direction.DOWN);
        dungeon.tick("none", Direction.LEFT); // step on exit 
        assertTrue(game.checkGoalComplete() == true);
    }

    @Test
    public void testGoalCompleteTreasureANDEnemyOrExit1() {
    // COMPLETE GOAL BY GOING TO EXIT
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();
                
        // create new game with gamemode standard
        //assertDoesNotThrow(() -> dungeon.newGame("treasureANDenemyORexit", "Standard"));
        dungeon.newGame("treasureANDenemyORexit", "Standard");
        GameFile game = dungeon.getCurrentGame();

        String goalStr = game.getGoals();
        // check that the goal information is correct
        assertTrue(goalStr != null);
        assertTrue(goalStr.equals(":treasure AND :enemies OR :exit"));

        //assert goal completion
        assertTrue(game.checkGoalComplete() == false);

        dungeon.tick("none", Direction.DOWN); // pick up sword
        dungeon.tick("none", Direction.DOWN); // pick up treasure
        dungeon.tick("none", Direction.DOWN); // step on exit
        assertTrue(game.checkGoalComplete() == true);
        
    }

    @Test
    public void testGoalCompleteTreasureANDEnemyOrExit2() {
    // COMPLETE GOAL BY DEFEATING MERCENARY
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();
                
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("treasureANDenemyORexit", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        String goalStr = game.getGoals();
        // check that the goal information is correct
        assertTrue(goalStr != null);
        assertTrue(goalStr.equals(":treasure AND :enemies OR :exit"));

        //assert goal completion
        assertTrue(game.checkGoalComplete() == false);

        dungeon.tick("none", Direction.DOWN); // pick up sword
        dungeon.tick("none", Direction.DOWN); // pick up treasure
        dungeon.tick("none", Direction.RIGHT); // battle mercenary
        assertTrue(game.checkGoalComplete() == true);
        
    }
}

