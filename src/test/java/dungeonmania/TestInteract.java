package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import dungeonmania.exceptions.InvalidActionException;
import dungeonmania.response.models.DungeonResponse;
import dungeonmania.util.Direction;

@TestInstance(value = Lifecycle.PER_CLASS)
public class TestInteract {
    @Test
    // Basic interactions to remove a zombie toast spawner.
    public void testInteractSpawner() {
        DungeonManiaController game = new DungeonManiaController(); 
        DungeonResponse initialState = game.newGame("interact_zombies", "Peaceful"); 

        // At the start, there should be a zombie toast spawner on to floor.
        assertTrue(initialState.getEntities()
            .stream()
            .anyMatch(e -> e.getType().equals("zombie_toast_spawner")));
        
        // We need to get the id so it can be passed into the interact method.
        String spawnerId = initialState.getEntities()
            .stream()
            .filter(e -> e.getType().equals("zombie_toast_spawner"))
            .findFirst()
            .get()
            .getId();
        
        // The player will now have a sword they can use on the zombie toast spawner to remove it.
        DungeonResponse gettingSword = game.tick(null, Direction.RIGHT);
        assertTrue(gettingSword.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("sword")));
        
        // After getting a sword, there should still be the zombie toast spawner on the floor.
        assertTrue(gettingSword.getEntities()
            .stream()
            .anyMatch(e -> e.getType().equals("zombie_toast_spawner")));

        // Now there is no zombie toast spawner on the floor.
        DungeonResponse removedSpawner = game.interact(spawnerId);
        assertFalse(removedSpawner.getEntities()
            .stream()
            .anyMatch(e -> e.getType().equals("zombie_toast_spawner")));

    }

    @Test
    // Attempting to remove a spawner without a weapon in inventory, should throw an exception.
    public void testInteractSpawnerNoWeapon() {
        DungeonManiaController game = new DungeonManiaController(); 
        DungeonResponse initialState = game.newGame("interact_zombies", "Peaceful"); 

        // We need to get the id so it can be passed into the interact method.
        String spawnerId = initialState.getEntities()
            .stream()
            .filter(e -> e.getType().equals("zombie_toast_spawner"))
            .findFirst()
            .get()
            .getId();

        // Moving without collecting the sword and asserting that the player does not have a sword.
        game.tick(null, Direction.DOWN);
        game.tick(null, Direction.RIGHT);

        DungeonResponse adjacentSpawner = game.tick(null, Direction.RIGHT);
        assertFalse(adjacentSpawner.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("sword")));

        // The player is adjacent to the zombie spawner, but because they do not have a sword, an exception will be thrown.
        assertThrows(InvalidActionException.class, () -> game.interact(spawnerId));

    }

    @Test
    // Attempting to interact with a spawner when the player is not cardinally adjacent to the spawner, will throw an exception.
    public void testInteractSpawnerOutOfRange() {
        DungeonManiaController game = new DungeonManiaController(); 
        DungeonResponse initialState = game.newGame("interact_zombies", "Peaceful"); 

        // We need to get the id so it can be passed into the interact method.
        String spawnerId = initialState.getEntities()
            .stream()
            .filter(e -> e.getType().equals("zombie_toast_spawner"))
            .findFirst()
            .get()
            .getId();
        
        // The player will now have a sword (as shown in the first test).
        game.tick(null, Direction.RIGHT);

        // The player is now out of range of the zombie spawner.
        game.tick(null, Direction.LEFT);

        // Since the player is not cardinally adjacent to the spawner, interact will raise an exception.
        assertThrows(InvalidActionException.class, () -> game.interact(spawnerId));

        game.tick(null, Direction.LEFT);
        game.tick(null, Direction.LEFT);

        // Even further out, the interact method will raise an exception.
        assertThrows(InvalidActionException.class, () -> game.interact(spawnerId));

    }

    @Test
    // Attempting to interact with an invalid id should raise an exception.
    public void testInteractInvalidId() {
        DungeonManiaController game = new DungeonManiaController(); 
        game.newGame("interact_zombies", "Peaceful"); 

        assertThrows(IllegalArgumentException.class, () -> game.interact("thisIsAnInvalidId123"));
        assertThrows(IllegalArgumentException.class, () -> game.interact("notAValidId"));
        assertThrows(IllegalArgumentException.class, () -> game.interact("spider_spawner"));
        assertThrows(IllegalArgumentException.class, () -> game.interact("zombie_spawner"));

    }

    @Test
    // Attempting to interact with an item that is not interactable will not do anything
    public void testInteractNonInteractable() {
        DungeonManiaController game = new DungeonManiaController(); 
        DungeonResponse initialState = game.newGame("interact_zombies", "Peaceful"); 

        // Gets the sword id of the sword on the floor of the map to attempt to interact with.
        String swordId = initialState.getEntities().stream().filter(e -> e.getType().equals("sword")).findFirst().get().getId();

        // Attempting to interact with the sword will not change the inventory or entities list.
        DungeonResponse interactSword = game.interact(swordId);
        assertEquals(interactSword.getInventory().size(), initialState.getInventory().size());
        assertEquals(interactSword.getEntities().size(), initialState.getEntities().size());
        

    }
    
}
