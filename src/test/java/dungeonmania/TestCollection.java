package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import dungeonmania.response.models.DungeonResponse;
import dungeonmania.util.Direction;

@TestInstance(value = Lifecycle.PER_CLASS)
public class TestCollection {
    @Test
    public void TestCollectingWood() {
        DungeonManiaController game = new DungeonManiaController();

        // Initially when creating a game, the player will have no items.
        DungeonResponse initial = game.newGame("collectables", "Standard");
        assertTrue(initial.getInventory().size() == 0);

        // After moving onto a collectable, in this case wood, the player will now have 1 item in their inventory, wood.
        DungeonResponse collected = game.tick(null, Direction.DOWN);
        assertTrue(collected.getInventory().size() == 1);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("wood"))
        );

    }
    @Test
    public void TestCollectingSword() {
        DungeonManiaController game = new DungeonManiaController();

        // Initially when creating a game, the player will have no items.
        DungeonResponse initial = game.newGame("collectables", "Standard");
        assertTrue(initial.getInventory().size() == 0);

        game.tick(null, Direction.LEFT);
        game.tick(null, Direction.LEFT);
        game.tick(null, Direction.LEFT);
        game.tick(null, Direction.LEFT);

        // After moving onto a collectable, in this case wood, the player will now have 1 item in their inventory, wood.
        DungeonResponse collected = game.tick(null, Direction.DOWN);
        assertTrue(collected.getInventory().size() == 1);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("sword"))
        );
    }
    @Test
    public void TestCollectingKey() {
        DungeonManiaController game = new DungeonManiaController();

        // Initially when creating a game, the player will have no items.
        DungeonResponse initial = game.newGame("collectables", "Standard");
        assertTrue(initial.getInventory().size() == 0);

        game.tick(null, Direction.LEFT);
        game.tick(null, Direction.LEFT);
        game.tick(null, Direction.LEFT);
        game.tick(null, Direction.LEFT);
        game.tick(null, Direction.LEFT);

        // After moving onto a collectable, in this case wood, the player will now have 1 item in their inventory, wood.
        DungeonResponse collected = game.tick(null, Direction.DOWN);
        assertTrue(collected.getInventory().size() == 1);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("key"))
        );
    }
    @Test
    public void TestCollectingArmour() {
        DungeonManiaController game = new DungeonManiaController();

        // Initially when creating a game, the player will have no items.
        DungeonResponse initial = game.newGame("collectables", "Standard");
        assertTrue(initial.getInventory().size() == 0);

        game.tick(null, Direction.LEFT);
        game.tick(null, Direction.LEFT);
        game.tick(null, Direction.LEFT);

        // After moving onto a collectable, in this case wood, the player will now have 1 item in their inventory, wood.
        DungeonResponse collected = game.tick(null, Direction.DOWN);
        assertTrue(collected.getInventory().size() == 1);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("armour"))
        );
    }
    @Test
    public void TestCollectingBomb() {
        DungeonManiaController game = new DungeonManiaController();

        // Initially when creating a game, the player will have no items.
        DungeonResponse initial = game.newGame("collectables", "Standard");
        assertTrue(initial.getInventory().size() == 0);

        game.tick(null, Direction.LEFT);
        game.tick(null, Direction.LEFT);

        // After moving onto a collectable, in this case wood, the player will now have 1 item in their inventory, wood.
        DungeonResponse collected = game.tick(null, Direction.DOWN);
        assertTrue(collected.getInventory().size() == 1);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("bomb"))
        );
    }
    @Test
    public void TestCollectingTreasure() {
        DungeonManiaController game = new DungeonManiaController();

        // Initially when creating a game, the player will have no items.
        DungeonResponse initial = game.newGame("collectables", "Standard");
        assertTrue(initial.getInventory().size() == 0);

        game.tick(null, Direction.LEFT);

        // After moving onto a collectable, in this case wood, the player will now have 1 item in their inventory, wood.
        DungeonResponse collected = game.tick(null, Direction.DOWN);
        assertTrue(collected.getInventory().size() == 1);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("treasure"))
        );
    }
    @Test
    public void TestCollectingArrow() {
        DungeonManiaController game = new DungeonManiaController();

        // Initially when creating a game, the player will have no items.
        DungeonResponse initial = game.newGame("collectables", "Standard");
        assertTrue(initial.getInventory().size() == 0);

        game.tick(null, Direction.RIGHT);

        // After moving onto a collectable, in this case wood, the player will now have 1 item in their inventory, wood.
        DungeonResponse collected = game.tick(null, Direction.DOWN);
        assertTrue(collected.getInventory().size() == 1);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("arrow"))
        );
    }
    @Test
    public void TestCollectingInvisPotion() {
        DungeonManiaController game = new DungeonManiaController();

        // Initially when creating a game, the player will have no items.
        DungeonResponse initial = game.newGame("collectables", "Standard");
        assertTrue(initial.getInventory().size() == 0);

        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);

        // After moving onto a collectable, in this case wood, the player will now have 1 item in their inventory, wood.
        DungeonResponse collected = game.tick(null, Direction.DOWN);
        assertTrue(collected.getInventory().size() == 1);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("invisibility_potion"))
        );
    }
    @Test
    public void TestCollectingInvincePotion() {
        DungeonManiaController game = new DungeonManiaController();

        // Initially when creating a game, the player will have no items.
        DungeonResponse initial = game.newGame("collectables", "Standard");
        assertTrue(initial.getInventory().size() == 0);

        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);

        // After moving onto a collectable, in this case wood, the player will now have 1 item in their inventory, wood.
        DungeonResponse collected = game.tick(null, Direction.DOWN);
        assertTrue(collected.getInventory().size() == 1);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("invincibility_potion"))
        );
    }
    @Test
    public void TestCollectingHealthPotion() {
        DungeonManiaController game = new DungeonManiaController();

        // Initially when creating a game, the player will have no items.
        DungeonResponse initial = game.newGame("collectables", "Standard");
        assertTrue(initial.getInventory().size() == 0);

        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);

        // After moving onto a collectable, in this case wood, the player will now have 1 item in their inventory, wood.
        DungeonResponse collected = game.tick(null, Direction.DOWN);
        assertTrue(collected.getInventory().size() == 1);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("health_potion"))
        );
    }

    @Test
    // Testing collecting a non-collectable item, a boulder in this case.
    public void TestUncollectable() {
        DungeonManiaController game = new DungeonManiaController();

        // Initially when creating a game, the player will have no items.
        DungeonResponse initial = game.newGame("boulders", "Peaceful");
        assertTrue(initial.getInventory().size() == 0);
        
        // After moving onto a boulder, since it is not a collectable item, it will not be in the player's inventory.
        DungeonResponse moveToBoulder = game.tick(null, Direction.RIGHT);
        assertTrue(moveToBoulder.getInventory().size() == 0);
    }

    @Test
    public void TestCollectingManyItems() {
        DungeonManiaController game = new DungeonManiaController();

        // Initially when creating a game, the player will have no items.
        DungeonResponse initial = game.newGame("collectables", "Standard");
        assertTrue(initial.getInventory().size() == 0);

        // Moves the player just before a collectable key.
        for (int i = 0; i < 5; i++) {
            game.tick(null, Direction.LEFT);
        }

        DungeonResponse collected = game.tick(null, Direction.DOWN); // pick up key
        assertTrue(collected.getInventory().size() == 1); 
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("key"))
        );
        collected = game.tick(null, Direction.RIGHT); // pick up sword
        assertTrue(collected.getInventory().size() == 2);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("sword"))
        );
        collected = game.tick(null, Direction.RIGHT); // pick up armour
        assertTrue(collected.getInventory().size() == 3);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("armour"))
        );
        collected = game.tick(null, Direction.RIGHT); // pick up bomb
        assertTrue(collected.getInventory().size() == 4);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("bomb"))
        );
        collected = game.tick(null, Direction.RIGHT); // pick up treasure
        assertTrue(collected.getInventory().size() == 5);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("treasure"))
        );
        collected = game.tick(null, Direction.RIGHT); // pick up wood
        assertTrue(collected.getInventory().size() == 6);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("wood"))
        );
        collected = game.tick(null, Direction.RIGHT); // pick up arrow
        assertTrue(collected.getInventory().size() == 7);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("arrow"))
        );
        collected = game.tick(null, Direction.RIGHT); // pick up invisibility_potion
        assertTrue(collected.getInventory().size() == 8);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("invisibility_potion"))
        );
        collected = game.tick(null, Direction.RIGHT); // pick up invincibility_potion
        assertTrue(collected.getInventory().size() == 9);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("invincibility_potion"))
        );
        collected = game.tick(null, Direction.RIGHT); // pick up health_potion
        assertTrue(collected.getInventory().size() == 10);
        assertTrue(
            collected.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("health_potion"))
        );
    }

    @Test
    // Tests that a player can collect multiple collectible entities of the same type.
    public void TestCollectingMultiples() {
        DungeonManiaController game = new DungeonManiaController();

        // Initially when creating a game, the player will have no items.
        DungeonResponse initial = game.newGame("buildables", "Standard");
        assertTrue(initial.getInventory().size() == 0);

        // Collects a treasure
        game.tick(null, Direction.RIGHT);

        DungeonResponse twoItems = game.tick(null, Direction.RIGHT);
        DungeonResponse threeItems = game.tick(null, Direction.RIGHT);

        assertEquals(2, twoItems.getInventory().size());
        assertEquals(3, threeItems.getInventory().size());

        assertEquals(1, twoItems.getInventory().stream().filter(e -> e.getType().equals("arrow")).count());
        assertEquals(2, threeItems.getInventory().stream().filter(e -> e.getType().equals("arrow")).count());

    }
    // For these, will need to create a new map.
    // Testing collecting multiple items of the same type.

    // Testing collecting a key, that will disappear after being used in a door.
}
