package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import dungeonmania.entities.Inventory;
import dungeonmania.entities.Player;
import dungeonmania.entities.enemy.Mercenary;
import dungeonmania.entities.itemEntity.collectableEntity.potion.Potion;
import dungeonmania.exceptions.InvalidActionException;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Direction;
import dungeonmania.util.Position;

public class TestPotions {

    @Test 
    public void TestPotionExceptions() {
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();

        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("healthpotion", "Standard"));
        Player player = (Player) GameFile.getPlayer(GameFile.getEntities());

        // use invalid item - throw exception
        
        assertThrows(IllegalArgumentException.class, () -> dungeon.tick("invalid", Direction.NONE));
        //get potion
        dungeon.tick(null, Direction.RIGHT);
        dungeon.tick(null, Direction.RIGHT);
        dungeon.tick(null, Direction.RIGHT);

        //see health potion
        Inventory inventory = player.getInventory(); 
        assertTrue(inventory.numberOf("health_potion") > 0 ); 

        //use health potion
        Potion healthPotion = (Potion) inventory.getItem("health_potion");
        dungeon.tick(healthPotion.getId(), Direction.NONE);
        assertThrows(InvalidActionException.class, () -> dungeon.tick(healthPotion.getId(), Direction.NONE));
        
    }

    @Test 
    public void TestHealthPotion() {
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();

        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("healthpotion", "Standard"));
        Player player = (Player) GameFile.getPlayer(GameFile.getEntities());

        // move into battle and fight mercenary 
        dungeon.tick(null, Direction.RIGHT);
        int postBattleHealth = (int) player.getHealth();

        //get potion
        dungeon.tick(null, Direction.RIGHT);
        dungeon.tick(null, Direction.RIGHT);

        //see health potion
        Inventory inventory = player.getInventory(); 
        assertTrue(inventory.numberOf("health_potion") > 0 ); 

        //use health potion
        Potion healthPotion = (Potion) inventory.getItem("health_potion");
        dungeon.tick(healthPotion.getId(), Direction.NONE);
        int postPotionHealth = (int) player.getHealth();
        assertTrue(postPotionHealth > postBattleHealth);
    }

    @Test //assumes merc battle radisu doesnt work
    public void TestInvincibilityPotion() {
        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();

        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("invinciblepotion", "Standard"));
        Player player = (Player) GameFile.getPlayer(GameFile.getEntities());

        // move into potion  
        dungeon.tick(null, Direction.RIGHT);

        //check inventory 
        Inventory inventory = player.getInventory(); 
        assertTrue(inventory.numberOf("invincibility_potion") > 0 ); 

        //use potion - mercenary should also move back
        Potion invincibilityPotion = (Potion) inventory.getItem("invincibility_potion");
        dungeon.tick(invincibilityPotion.getId(), Direction.NONE);
        dungeon.tick(null, Direction.RIGHT);
        dungeon.tick(null, Direction.RIGHT);
        

        //see health post battle is full
        assertTrue(player.getHealth() >= 10);
    }


    @Test //assumes merc battle radisu doesnt workd
    public void TestInvisibilityPotion() {

        // create new dungeon controller
        DungeonManiaController dungeon = new DungeonManiaController();

        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("invisibilitypotion", "Standard"));
        Player player = (Player) GameFile.getPlayer(GameFile.getEntities());

        // move into potion  
        dungeon.tick(null, Direction.RIGHT);

        //check inventory 
        Inventory inventory = player.getInventory(); 
        assertTrue(inventory.numberOf("invisibility_potion") > 0 ); 

        //use potion 
        Potion invisibilityPotion = (Potion) inventory.getItem("invisibility_potion");
        dungeon.tick(invisibilityPotion.getId(), Direction.NONE);

        //check mercenary postion 
        Mercenary merc = (Mercenary) dungeon.getCurrentGame().SingleEntityFromPos(new Position(player.getPosition().getX() + 1, player.getPosition().getY()));
        Position mercPos1 = merc.getPosition();

        //move while invisible
        dungeon.tick(null, Direction.DOWN);
        dungeon.tick(null, Direction.UP);

        //check mercenary postion post movement
        Position mercPos2 = merc.getPosition();
        assert(mercPos1.equals(mercPos2));

    }
    
}

