package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import dungeonmania.exceptions.InvalidActionException;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.*;

public class TestZombieSpawner {
    
    @Test
    //Testing one zombie spawns at the 20th tick
    public void TestZombieSpawnOne() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("zombieSpawner", "Standard"));

        // move the player and spawn a single zombie
        for (int i = 0; i < 20; i++) {
            dungeon.tick(null, Direction.DOWN);
        }
        //Checks that one zombie has spawned
        assertTrue(GameFile.getEntities().stream().anyMatch(e->e.getType().equals("zombie_toast")));

    }

    @Test
    //Testing no zombies spawn at 19th tick
    public void TestNoSpawn() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("zombieSpawner", "Standard"));

        // move the player and 
        for (int i = 0; i < 19; i++) {
            dungeon.tick(null, Direction.DOWN);
        }
        //Checks that no zombie has spawned
        assertFalse(GameFile.getEntities().stream().anyMatch(e->e.getType().equals("zombie_toast")));
    }

    @Test
    //Tests that two zombies can be spawned and zombies stay alive if player is not attacking
    public void TestSpawnPersists() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("zombieSpawner", "Standard"));

        // move the player and spawns zombie
        for (int i = 0; i < 20; i++) {
            dungeon.tick(null, Direction.DOWN);
        }
        //Checks one zombie is spawned
        long counterFirst = GameFile.getEntities().stream().filter(t -> t.getType().equals("zombie_toast")).collect(Collectors.counting());
        assertTrue(counterFirst == 1);

        //Moves the player to spawn another zombie
        for (int i = 0; i < 20; i++) {
            dungeon.tick(null, Direction.DOWN);
        }

        //Checks two zombies are spawned on the map
        long counterSecond = GameFile.getEntities().stream().filter(t -> t.getType().equals("zombie_toast")).collect(Collectors.counting());
        assertTrue(counterSecond == 2);
    }

    @Test
    //Tests that multiple spawners will all spawn zombies
    public void TestMultipleSpawners() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("zombieSpawnersThree", "Standard"));

        // move the player and spawn zombies
        for (int i = 0; i < 20; i++) {
            dungeon.tick(null, Direction.DOWN);
        }

        // move the player and Spawn zombies from all spawners
        long counterFirst = GameFile.getEntities().stream().filter(t -> t.getType().equals("zombie_toast")).collect(Collectors.counting());
        assertTrue(counterFirst == 3);
        for (int i = 0; i < 20; i++) {
            dungeon.tick(null, Direction.DOWN);
        }
        // move the player and spawn zombies from all spawners
        long counterSecond = GameFile.getEntities().stream().filter(t -> t.getType().equals("zombie_toast")).collect(Collectors.counting());
        assertTrue(counterSecond == 6);
    }


    @Test
    //Testing that the spawner can be interacted and destroyed
    public void testDestroySpawnerSuccess() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("zombieSpawnerInteract", "Standard"));
        System.out.println(GameFile.getEntities().stream().filter(e -> e.getType().equals("zombie_toast_spawner")).findFirst().get().getId());

        //Move the player to the right to get sword and finish next to spawner
        dungeon.tick(null, Direction.LEFT);
        dungeon.tick(null, Direction.LEFT);

        //Kill Spawner
        dungeon.interact("2");
        //Checks Spawner no longer exists
        assertFalse(GameFile.getEntities().stream().anyMatch(e->e.getType().equals("zombie_toast_spawner")));
    }

    @Test
    //Testing that the spawner throws exception when player does not have sword
    public void testNoSwordInteract() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("zombieSpawnerInteract", "Standard"));
        //Move up to avoid the sword
        dungeon.tick(null, Direction.UP);
        //Move right to reach the spawner
        dungeon.tick(null, Direction.RIGHT);
        dungeon.tick(null, Direction.RIGHT);
        dungeon.tick(null, Direction.RIGHT);

        assertThrows(InvalidActionException.class, () -> dungeon.interact("2"));
    }

    @Test
    //Testing that the spawner throws exception when player is not in range
    public void testNoInRangeInteract() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("zombieSpawnerInteract", "Standard"));
        //Move up to avoid the sword
        dungeon.tick(null, Direction.UP);
        //Call interact when not in range
        assertThrows(InvalidActionException.class, () -> dungeon.interact("2"));
    }

}