package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import dungeonmania.exceptions.InvalidActionException;
import dungeonmania.response.models.DungeonResponse;
import dungeonmania.util.Direction;

@TestInstance(value = Lifecycle.PER_CLASS)
public class TestBuilding {
    @Test
    // Test for building a bow.
    public void TestBuildBow() {
        DungeonManiaController game = new DungeonManiaController();

        // Shows that the player's inventory is initially empty.
        DungeonResponse initialState = game.newGame("buildables", "Peaceful");
        assertTrue(initialState.getInventory().isEmpty());

        // Collecting all the items for building a bow.
        for (int i = 0; i < 7; i++) {
            game.tick(null, Direction.RIGHT);
        }

        // Gets the DungeonResponse after getting all items and shows that a player can build a bow.
        DungeonResponse gottenAllItems = game.tick(null, Direction.RIGHT);
        assertTrue(gottenAllItems.getInventory().size() == 7);
        assertTrue(gottenAllItems.getBuildables().contains("bow"));

        // Builds the bow and shows that the bow is now in the player's inventory.
        assertDoesNotThrow(() -> {
            DungeonResponse afterBuilding = game.build("bow");
            assertTrue(afterBuilding.getInventory()
            .stream()
            .anyMatch(i -> i.getType().equals("bow")));
        });

    }

    @Test
    // Test for building a shield.
    public void TestBuildShield() {
        DungeonManiaController game = new DungeonManiaController();

        // Shows that the player's inventory is initially empty.
        DungeonResponse initialState = game.newGame("buildables", "Peaceful");
        assertTrue(initialState.getInventory().isEmpty());

        // Collecting all the items for building a shield.
        for (int i = 0; i < 7; i++) {
            game.tick(null, Direction.RIGHT);
        }

        // Gets the DungeonResponse after getting all items and shows that a player can build a shield.
        DungeonResponse gottenAllItems = game.tick(null, Direction.RIGHT);
        assertTrue(gottenAllItems.getInventory().size() == 7);
        assertTrue(gottenAllItems.getBuildables().contains("shield"));

        // Builds the shield and shows that the shield is now in the player's inventory.
        assertDoesNotThrow(() -> {
            DungeonResponse afterBuilding = game.build("shield");
            assertTrue(afterBuilding.getInventory()
            .stream()
            .anyMatch(i -> i.getType().equals("shield")));
        });
    }

    @Test
    // Test for attempting to build but not having enough items.
    public void TestNotEnoughItems() {
        DungeonManiaController game = new DungeonManiaController();
        game.newGame("buildables", "Peaceful");
        
        // Collecting some, but not all the prerequisite items for building a bow.
        for (int i = 0; i < 3; i++) {
            game.tick(null, Direction.RIGHT);
        }

        // Shows that at this point, the player does not have enough materials to build a bow.
        DungeonResponse notGottenAllItems = game.tick(null, Direction.DOWN);
        assertTrue(notGottenAllItems.getInventory().size() == 3);
        assertTrue(!notGottenAllItems.getBuildables().contains("bow"));

        // Attempting to build a bow fails.
        assertThrows(InvalidActionException.class, () -> game.build("bow"));

        // Shows that there is no match to an item in inventory of type bow.
        DungeonResponse inventoryNoBow = game.tick(null, Direction.RIGHT);
        assertTrue(inventoryNoBow.getInventory()
            .stream()
            .noneMatch(i -> i.getType().equals("bow"))
        );
    }

    @Test
    // Test for attempting to build but not having the required item types.
    public void TestNoItems() {
        DungeonManiaController game = new DungeonManiaController();
        DungeonResponse initialState = game.newGame("buildables", "Peaceful");
        assertTrue(initialState.getInventory().isEmpty());

        // Shows that at this point, the player does not have enough materials to build a shield.
        assertTrue(!initialState.getBuildables().contains("shield"));

        // Attempting to build a bow fails.
        assertThrows(InvalidActionException.class, () -> game.build("shield"));

        // Shows that the player's inventory is still empty.
        DungeonResponse stillNoItems = game.tick(null, Direction.DOWN);
        assertTrue(stillNoItems.getInventory().isEmpty());
    }

    @Test
    // Test for attempting to build with the wrong item type provided.
    public void TestWrongItemType() {
        DungeonManiaController game = new DungeonManiaController();
        
        game.newGame("buildables", "Peaceful");

        // Attempting to build invalid item types or items that are not buildable.
        assertThrows(IllegalArgumentException.class, () -> game.build("beau"));
        assertThrows(IllegalArgumentException.class, () -> game.build("sheeld"));
        assertThrows(IllegalArgumentException.class, () -> game.build("sword"));

        // Collecting all the items for building a shield or bow.
        for (int i = 0; i < 7; i++) {
            game.tick(null, Direction.RIGHT);
        }

        // Shows the Player can now build a bow or shield.
        DungeonResponse allItemsCollected = game.tick(null, Direction.DOWN);
        assertTrue(allItemsCollected.getInventory().size() == 7);
        assertTrue(allItemsCollected.getBuildables().contains("bow"));
        assertTrue(allItemsCollected.getBuildables().contains("shield"));

        // Still unable to build any invalid items.
        assertThrows(IllegalArgumentException.class, () -> game.build("beau"));
        assertThrows(IllegalArgumentException.class, () -> game.build("sheeld"));
        assertThrows(IllegalArgumentException.class, () -> game.build("sword"));


        // Shows that there are no bows or shields in the player's inventory.
        DungeonResponse afterFailedBuilding = game.tick(null, Direction.DOWN);
        assertTrue(afterFailedBuilding.getInventory()
            .stream()
            .noneMatch(i -> i.getType().equals("bow"))
        );
        assertTrue(afterFailedBuilding.getInventory()
            .stream()
            .noneMatch(i -> i.getType().equals("shield"))
        );
        
    }

    @Test
    // Testing building a shield with keys or sun stones instead of treasure.
    public void testBuildShieldAlternate() {
        // Building a shield with a key.
        DungeonManiaController game = new DungeonManiaController();
        game.newGame("buildables_alternate", "Peaceful");

        // Getting all 2 wood and a key for a shield.
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        DungeonResponse gottenKeyAndOtherItems = game.tick(null, Direction.RIGHT);

        assertTrue(gottenKeyAndOtherItems.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("key")));
        
        // Building a shield should work and the key should be gone.
        assertDoesNotThrow(() -> {
            DungeonResponse builtShield = game.build("shield");
            assertTrue(builtShield.getInventory()
                .stream()
                .anyMatch(e -> e.getType().equals("shield")));
                
            assertFalse(builtShield.getInventory()
                .stream()
                .anyMatch(e -> e.getType().equals("key")));
        });

        // Building a shield with a sun stone (interchangeable with treasure)
        DungeonManiaController gameSun = new DungeonManiaController();
        gameSun.newGame("buildables_alternate", "Peaceful");

        // Getting all 2 wood and a sun stone for a shield.
        gameSun.tick(null, Direction.RIGHT);
        gameSun.tick(null, Direction.RIGHT);
        gameSun.tick(null, Direction.RIGHT);
        DungeonResponse gottenSunAndOtherItems = gameSun.tick(null, Direction.RIGHT);

        assertTrue(gottenSunAndOtherItems.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("sun_stone")));
        
        // Building a shield should work and the sun stone should be gone.
        assertDoesNotThrow(() -> {
            DungeonResponse builtShield = gameSun.build("shield");
            assertTrue(builtShield.getInventory()
                .stream()
                .anyMatch(e -> e.getType().equals("shield")));
            
            assertFalse(builtShield.getInventory()
                .stream()
                .anyMatch(e -> e.getType().equals("sun_stone")));
        });
    }
}
