package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import dungeonmania.util.Direction;
import dungeonmania.util.Position;

public class TestPlayerMovement {
    
    @Test 
    public void TestMoveUp() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("playerMovement", "Standard"));

        // check that the game loaded correctly
        assertTrue(dungeon.getCurrentGame().getPositionEntity("16").equals(new Position(2, 2)) ); // player is in pos 2,2

        // move the character up one position 
        dungeon.tick("none", Direction.UP);
        // check the position of the player moved up
        assertTrue(dungeon.getCurrentGame().getPositionEntity("16").equals(new Position(2, 1)));
    }

    @Test 
    public void TestMoveDown() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("playerMovement", "Standard"));

        // check that the game loaded correctly
        assertTrue(dungeon.getCurrentGame().getPositionEntity("16").equals(new Position(2, 2))); // player is in pos 3,3

        // move the character up one position 
        dungeon.tick("none", Direction.DOWN);
        // check the position of the player moved up
        assertTrue(dungeon.getCurrentGame().getPositionEntity("16").equals(new Position(2, 3)));
    }

    @Test 
    public void TestMoveLeft() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("playerMovement", "Standard"));

        // check that the game loaded correctly
        assertTrue(dungeon.getCurrentGame().getPositionEntity("16").equals(new Position(2, 2))); // player is in pos 3,3

        // move the character up one position 
        dungeon.tick("none", Direction.LEFT);
        // check the position of the player moved up
        assertTrue(dungeon.getCurrentGame().getPositionEntity("16").equals(new Position(1, 2)));
    }

    @Test 
    public void TestMoveRight() {
        // create new dungeon 
        
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("playerMovement", "Standard"));
 
        // check that the game loaded correctly
        assertTrue(dungeon.getCurrentGame().getPositionEntity("16").equals(new Position(2, 2))); // player is in pos 3,3

        // move the character up one position 
        dungeon.tick("none", Direction.RIGHT);
        // check the position of the player moved up
        assertTrue(dungeon.getCurrentGame().getPositionEntity("16").equals(new Position(3, 2)));
    }

    @Test 
    public void TestMoveWall() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("playerMovement", "Standard"));

        // check that the game loaded correctly
        assertTrue(dungeon.getCurrentGame().getPositionEntity("16").equals(new Position(2, 2))); // player is in pos 3,3

        // move the character up one position 
        dungeon.tick("none", Direction.DOWN);
        dungeon.tick("none", Direction.DOWN);
        // check the position of the player moved up
        assertTrue(dungeon.getCurrentGame().getPositionEntity("16").equals(new Position(2, 3)));
    }
}
