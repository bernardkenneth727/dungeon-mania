package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import dungeonmania.exceptions.InvalidActionException;
import dungeonmania.gameFile.GameFile;
import dungeonmania.response.models.DungeonResponse;
import dungeonmania.util.Direction;

@TestInstance(value = Lifecycle.PER_CLASS)
public class TestBomb {
    @Test
    // Testing picking up a bomb.
    public void TestPickupBomb() {
        DungeonManiaController game = new DungeonManiaController();
        
        DungeonResponse startGame = game.newGame("bombsAndBoulders", "Standard");
        assertFalse(startGame.getInventory().stream().anyMatch(i -> i.getType().equals("bomb")));
        
        DungeonResponse pickedUpBomb = game.tick(null, Direction.RIGHT);
        assertTrue(pickedUpBomb.getInventory().stream().anyMatch(i -> i.getType().equals("bomb")));

    }

    @Test
    // Testing placing/using a bomb with the tick method.
    public void TestPlacingBomb() {
        DungeonManiaController game = new DungeonManiaController();
        
        game.newGame("bombsAndBoulders", "Standard");
        
        // Getting the id of the bomb picked up.
        DungeonResponse pickedBomb = game.tick(null, Direction.RIGHT);
        String bombId = pickedBomb.getInventory()
            .stream()
            .filter(i -> i.getType().equals("bomb"))
            .findFirst()
            .get()
            .getId();
        
        game.tick(null, Direction.LEFT);

        
        // Placing down the bomb.
        DungeonResponse placingBomb = game.tick(bombId, Direction.NONE);
        // Showing that the bomb is no longer in the player's inventory.
        assertFalse(placingBomb.getInventory()
            .stream()
            .anyMatch(i -> i.getType().equals("bomb")));
        assertFalse(placingBomb.getInventory()
            .stream()
            .anyMatch(i -> i.getId().equals(bombId)));

        // Showing that the bomb is now on the ground.
        assertTrue(placingBomb
            .getEntities()
            .stream()
            .anyMatch(i -> i.getId().equals(bombId)));
    }

    @Test
    // Testing detonating a bomb using the boulder + floor switch.
    public void TestDetonateBomb() {
        DungeonManiaController game = new DungeonManiaController();
        game.newGame("bombsAndBoulders", "Standard");

        // Getting the id of the bomb picked up.
        DungeonResponse pickedBomb = game.tick(null, Direction.RIGHT);
        String bombId = pickedBomb.getInventory()
            .stream()
            .filter(i -> i.getType().equals("bomb"))
            .findFirst()
            .get()
            .getId();

        // Showing that walls, arrows and wood are on the ground.
        assertTrue(pickedBomb.getEntities().stream().anyMatch(e -> e.getType().equals("wood")));
        assertTrue(pickedBomb.getEntities().stream().anyMatch(e -> e.getType().equals("arrow")));
        assertTrue(pickedBomb.getEntities().stream().anyMatch(e -> e.getType().equals("wall")));
        
        DungeonResponse detonatingBomb = game.tick(bombId, Direction.NONE);
        // Showing that the wood and arrows are no longer on the ground.
        assertFalse(detonatingBomb.getEntities().stream().anyMatch(e -> e.getType().equals("wood")));
        assertFalse(detonatingBomb.getEntities().stream().anyMatch(e -> e.getType().equals("arrow")));
        assertFalse(detonatingBomb.getEntities().stream().anyMatch(e -> e.getType().equals("bomb")));
        // Showing that all the walls have been removed from the ground.
        assertFalse(detonatingBomb.getEntities().stream().anyMatch(e -> e.getType().equals("wall")));


    }

    @Test
    // Testing trying to use a bomb not in the player's inventory.
    public void TestBombNotInInventory() {
        DungeonManiaController game = new DungeonManiaController();
        DungeonResponse initialState = game.newGame("bombsAndBoulders", "Standard");

        String bombId = initialState.getEntities().stream().filter(e -> e.getType().equals("bomb")).findFirst().get().getId();

        assertThrows(InvalidActionException.class, () -> game.tick(bombId, Direction.NONE));
    }

    
}
