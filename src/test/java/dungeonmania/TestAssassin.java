package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Direction;
import dungeonmania.util.Position;

public class TestAssassin {
    
    @Test 
    public void testAssassinSpawn() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();

        assertDoesNotThrow(() -> dungeon.newGame("assassinSpawner", "Standard"));

        for (int i = 0; i < 30; i++) {
            dungeon.tick(null, Direction.DOWN);
            dungeon.tick(null, Direction.RIGHT);
        }
        // check that assassin has spawned at least once 
        long counterFirst = GameFile.getEntities().stream().filter(t -> t.getType().equals("assassin")).collect(Collectors.counting());
        counterFirst += GameFile.getEntities().stream().filter(t -> t.getType().equals("mercenary")).collect(Collectors.counting());
        assertTrue(counterFirst > 0);
    }

    @Test
    public void testAssassinMovement() {
        // should be the same as Mercenary 
         // create new dungeon 
         DungeonManiaController dungeon = new DungeonManiaController();
        
         // create new game with gamemode standard
         assertDoesNotThrow(() -> dungeon.newGame("assassinMovement", "Standard"));
         GameFile game = dungeon.getCurrentGame();
 
         // Mercenary should move one position towards player
         game.tick(Direction.RIGHT);
         assertTrue(game.getPositionEntity("2").equals(new Position(2, 1)));
    }
    @Test
    //Testing Assassin Movement for all directions without wall blockage
    public void TestAssassinFollows() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("assassinFollow", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // Mercenary should move one position towards player
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("2").equals(new Position(2, 1)));
        game.tick(Direction.UP);
        assertTrue(game.getPositionEntity("2").equals(new Position(2, 0)));
        game.tick(Direction.LEFT);
        assertTrue(game.getPositionEntity("2").equals(new Position(3, 0)));
        game.tick(Direction.DOWN);
        //assertTrue(game.getPositionEntity("2").equals(new Position(3, 1)));
    }
    @Test
    public void testAssassinBattleLose() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
                
        // create new game with gamemode standard
        dungeon.newGame("battleAssassin", "Standard");

        dungeon.tick(null, Direction.RIGHT);
        System.out.println("in here");
        assertTrue(GameFile.getPlayer(GameFile.getEntities()) == null);
    }
    @Test
    public void TestBattleMercenarySword() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
                
        // create new game with gamemode standard
        dungeon.newGame("battleAssassinSword", "Standard");
        GameFile game = dungeon.getCurrentGame();
        if (game.getEnt("2").getHasArmour()) {
            dungeon.tick(null, Direction.RIGHT);
            dungeon.tick(null, Direction.RIGHT);
            System.out.println("in here");
            assertTrue(GameFile.getPlayer(GameFile.getEntities()) == null);
        }
        else {

            dungeon.tick(null, Direction.RIGHT);
            dungeon.tick(null, Direction.RIGHT);
            System.out.println("player supposed to live?");
            assertTrue(GameFile.getPlayer(GameFile.getEntities()) != null);
            assertFalse(GameFile.getEntities().stream().anyMatch(e->e.getType().equals("assassin")));
        }
    }
    @Test
    public void testAssassinBribe() {
        DungeonManiaController dungeon = new DungeonManiaController();
        
        assertDoesNotThrow(() -> dungeon.newGame("assassinBribe", "Standard"));
        GameFile game = dungeon.getCurrentGame();
        // Player has collected 2 treasures in inventory. 
        game.tick(Direction.RIGHT);
        game.tick(Direction.RIGHT);
        dungeon.interact("4");
        //Checks inventory no longer has coins
        assertTrue(game.getInventory().numberOf("treasure") == 0);
        assertTrue(game.getInventory().numberOf("one_ring") == 0);
    }
}

