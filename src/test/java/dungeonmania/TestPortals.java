package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import dungeonmania.entities.Portal;
import dungeonmania.util.Direction;
import dungeonmania.util.Position;

public class TestPortals {
    @Test
    public void testPlayerPortalPair1() {
         // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
            
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("portalComplex", "Standard"));
        //GameFile game = dungeon.getCurrentGame();

        assertTrue(dungeon.getCurrentGame().getPositionEntity("2").equals(new Position(1,0)));
        Portal portal = (Portal) dungeon.getCurrentGame().getEnt("2");
        assertTrue(portal.getColour().equals("BLUE"));
        
        // pass through the portal and assert right position
        dungeon.tick(null, Direction.UP);
        dungeon.tick(null, Direction.RIGHT);
        assertTrue(dungeon.getCurrentGame().getPositionEntity("1").equals(new Position(5,2)));
    }


    @Test
    public void testPlayerPortalPair2() {
        // create new dungeon 
       DungeonManiaController dungeon = new DungeonManiaController();
           
       // create new game with gamemode standard
       assertDoesNotThrow(() -> dungeon.newGame("portalComplex", "Standard"));
       assertTrue(dungeon.getCurrentGame().getPositionEntity("3").equals(new Position(1,2)));
       Portal portal = (Portal) dungeon.getCurrentGame().getEnt("3");
       assertTrue((portal).getColour().equals("RED"));
       
       // pass through the portal and assert right position
       dungeon.tick(null, Direction.DOWN);
       dungeon.tick(null, Direction.RIGHT);
       assertTrue(dungeon.getCurrentGame().getPositionEntity("1").equals(new Position(5,0)));
   }



}
