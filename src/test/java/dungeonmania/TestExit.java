package dungeonmania;

import dungeonmania.entities.Entity;
import dungeonmania.entities.Player;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Position;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;

public class TestExit {

    @Test //fix
    // Testing status of exit upon completion 
    public void testExit() {
        DungeonManiaController game = new DungeonManiaController();

        // newgame 
        assertDoesNotThrow(() -> game.newGame("exit", "Standard"));

        //assert exit is in right position 
        Player player = (Player) GameFile.getPlayer(GameFile.getEntities()); 
        Position playerPos = player.getPosition();
        Entity entity = game.getCurrentGame().SingleEntityFromPos(new Position((playerPos.getX() +1), playerPos.getY())); 
        assert(entity.getType().equals("exit"));
    }
}
