package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import dungeonmania.entities.Player;
import dungeonmania.entities.itemEntity.collectableEntity.rare.Rare;
import dungeonmania.gameFile.GameFile;
import dungeonmania.response.models.DungeonResponse;
import dungeonmania.util.Direction;

@TestInstance(value = Lifecycle.PER_CLASS)
public class TestOneRing {
    @Test
    // When getting a seed that gets the item, the player has won the item.
    public void testGettingRing() {
        DungeonManiaController game = new DungeonManiaController();
        game.newGame("maze", "Standard");
        GameFile myGame = game.getCurrentGame();

        // In this case, a seed of 2 gives the next int to be 8 which is less than 10, where anything less wins the item.
        assertTrue(Rare.battleWinnings(myGame.getInventory(), 2) != null);
        assertEquals(1, myGame.getInventory().numberOf("one_ring"));
    }

    @Test
    // When getting a seed that does not get the item, the player has not won the item.
    public void testNotGettingRing() {
        DungeonManiaController game = new DungeonManiaController();
        game.newGame("maze", "Standard");
        GameFile myGame = game.getCurrentGame();

        // A seed of 1 does not win since it gives 85, greater than 10.
        assertEquals(null, Rare.battleWinnings(myGame.getInventory(), 1));
        assertEquals(0, myGame.getInventory().numberOf("one_ring"));
    }

    @Test
    // Attempting multiple times, and winning multiple TheOneRings.
    public void testMultipleWins() {
        DungeonManiaController game = new DungeonManiaController();
        game.newGame("maze", "Standard");
        GameFile myGame = game.getCurrentGame();
        
        // When the seed goes up from 0 to 24, there should be 3 cases where the number generated is less than 15.
        for (int i = 0; i < 25; i++) {
            Rare.battleWinnings(myGame.getInventory(), i);
        }
        assertEquals(3, myGame.getInventory().numberOf("one_ring"));
    }

    @Test
    // Collecting The One Ring.
    public void testCollectOneRing() {
        DungeonManiaController game = new DungeonManiaController();
        DungeonResponse initialState = game.newGame("oneRing", "Peaceful");
        
        assertTrue(initialState.getInventory().isEmpty());

        // The player has moved onto a spot with The One Ring on it, so they should be able to collect it.
        DungeonResponse collectingOneRing = game.tick(null, Direction.RIGHT);

        assertTrue(collectingOneRing.getInventory().stream().anyMatch(e -> e.getType().equals("one_ring")));

    }

    @Test
    // Using The One Ring in Battle.
    public void testBattleWithOneRing() {
        DungeonManiaController game = new DungeonManiaController();
        DungeonResponse initialState = game.newGame("oneRing", "Standard");
        
        // The player's inventory should be initially empty.
        assertTrue(initialState.getInventory().isEmpty());

        // The player should now have a The One Ring in their inventory.
        DungeonResponse collectingOneRing = game.tick(null, Direction.RIGHT);
        assertTrue(collectingOneRing.getInventory().stream().anyMatch(e -> e.getType().equals("one_ring")));
        // Gets the id of The One Ring that the player has collected.
        String OneRingId = collectingOneRing.getInventory().stream().filter(e -> e.getType().equals("one_ring")).findFirst().get().getId();

        // Getting the current game file, to set the player's health to 0.5, so they would normally die to a Spider.
        GameFile currentGame = game.getCurrentGame();
        Player player = (Player) GameFile.getPlayer(GameFile.getEntities());
        player.setHealth(0.5);

        // The player's current game inventory should also contain the one ring id that was obtained.
        assertTrue(currentGame.getInventory().contains(OneRingId));

        // Moving to battle the spider.
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        
        // After fighting the spider, the player will no longer have this instance of The One Ring in their inventory since they used it in order to beat the spider.
        assertFalse(currentGame.getInventory().contains(OneRingId));

    }

    @Test
    // Testing getting the winning items many times to ensure that the player gets at least one The One Ring and Anduril.
    public void testManyRandom() {
        DungeonManiaController game = new DungeonManiaController();
        game.newGame("maze", "Peaceful");
        GameFile myGame = game.getCurrentGame();

        // Generates the battle winnings 1000 times.
        for (int i = 0; i < 1000; i++) {
            Rare.battleWinnings(myGame.getInventory());
        }

        // There should be at least 1 One Ring and 1 Anduril generated, since there is a 10% chance of One Ring and 5% change of Anduril.
        assertTrue(myGame.getInventory().numberOf("one_ring") > 0);
        assertTrue(myGame.getInventory().numberOf("anduril") > 0);
    }
    
}
