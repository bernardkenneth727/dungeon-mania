package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import dungeonmania.gameFile.GameFile;
import dungeonmania.response.models.DungeonResponse;
import dungeonmania.util.Direction;
import dungeonmania.util.Position;

@TestInstance(value = Lifecycle.PER_CLASS)
public class TestDoor {
    @Test
    // Testing for unlocking a door.
    public void TestUnlock() {
        DungeonManiaController game = new DungeonManiaController();

        game.newGame("doors_and_keys", "Standard");

        // Getting the dungeon responses after getting key and then moving through a door.
        DungeonResponse afterCollectingKey = game.tick(null, Direction.RIGHT);
        DungeonResponse afterMovingThroughDoor = game.tick(null, Direction.RIGHT);

        // Getting the positions of player at these responses.
        Position playerPositionAfterCollectingKey = afterCollectingKey.getEntities()
            .stream()
            .filter(e -> e.getType().equals("player"))
            .findFirst()
            .get()
            .getPosition();
        
        Position playerPositionAfterMovingThroughDoor = afterMovingThroughDoor.getEntities()
        .stream()
        .filter(e -> e.getType().equals("player"))
        .findFirst()
        .get()
        .getPosition();

        // The player has now moved through the door, so the player's positions for those should be false, but the player is adjacent to their position before.
        assertFalse(playerPositionAfterCollectingKey.equals(playerPositionAfterMovingThroughDoor));
        assertTrue(Position.isAdjacent(playerPositionAfterMovingThroughDoor, playerPositionAfterCollectingKey));
    }

    @Test
    // Testing for attempting to go through a door.
    public void TestLockedDoor() {
        DungeonManiaController game = new DungeonManiaController();

        game.newGame("doors_and_keys", "Standard");

        game.tick(null, Direction.DOWN);
        game.tick(null, Direction.RIGHT);

        // Getting the dungeon responses before attempting to move through a door and after attempting to move through a door.
        DungeonResponse beforeMovingThroughDoor = game.tick(null, Direction.RIGHT);
        DungeonResponse afterAttemptingMovingThroughDoor = game.tick(null, Direction.UP);

        // Getting the positions of player at these responses.
        Position playerPositionBeforeMovingThroughDoor = beforeMovingThroughDoor.getEntities()
            .stream()
            .filter(e -> e.getType().equals("player"))
            .findFirst()
            .get()
            .getPosition();
        
        Position playerPositionAfterAttemptingMovingThroughDoor = afterAttemptingMovingThroughDoor.getEntities()
        .stream()
        .filter(e -> e.getType().equals("player"))
        .findFirst()
        .get()
        .getPosition();

        // The player has now unsuccessfully moved through the door, so their position will be the same.
        assertTrue(playerPositionAfterAttemptingMovingThroughDoor.equals(playerPositionBeforeMovingThroughDoor));
    }

    @Test
    // Testing for unlocking two doors, that both keys are used.
    public void TestTwoUnlockingTwoDoors() {
        DungeonManiaController game = new DungeonManiaController();

        game.newGame("doors_and_keys", "Standard");

        // Getting the dungeon responses after getting key and then moving through a door. After collecting a key there should be one in the players inventory.
        DungeonResponse afterCollectingKey = game.tick(null, Direction.RIGHT);
        assertTrue(afterCollectingKey.getInventory().stream().anyMatch(e -> e.getType().equals("key")));
        
        // The player has now used the key so the amount of keys should be 0 in the players inventory.
        DungeonResponse afterMovingThroughDoor = game.tick(null, Direction.RIGHT);
        assertFalse(afterMovingThroughDoor.getInventory().stream().anyMatch(e -> e.getType().equals("key")));

        // Moving back to open another door.
        game.tick(null, Direction.LEFT);
        game.tick(null, Direction.LEFT);

        // Getting the dungeon responses after getting another key. After collecting a key there should be one in the players inventory.
        DungeonResponse afterCollectingAnotherKey = game.tick(null, Direction.LEFT);
        assertTrue(afterCollectingAnotherKey.getInventory().stream().anyMatch(e -> e.getType().equals("key")));        
        
        // Getting the dungeon responses after going through another door.
        DungeonResponse afterMovingThroughAnotherDoor = game.tick(null, Direction.LEFT);
        // The player has now used the key so the amount of keys should be 0 in the players inventory.
        assertFalse(afterMovingThroughAnotherDoor.getInventory().stream().anyMatch(e -> e.getType().equals("key"))); 

        
    }

    @Test
    // Testing walking through a door multiple times should work.
    public void testWalkingThroughDoorMultipleTimes() {
        DungeonManiaController game = new DungeonManiaController();

        game.newGame("doors_and_keys", "Standard");

        // Getting the dungeon responses after getting key and then moving through a door. After collecting a key there should be one in the players inventory.
        game.tick(null, Direction.RIGHT);
        Position playerPositionAfterCollectingKey = GameFile.getPlayer(GameFile.getEntities()).getPosition();

        game.tick(null, Direction.RIGHT);
        Position playerPositionOnDoor = GameFile.getPlayer(GameFile.getEntities()).getPosition();

        game.tick(null, Direction.RIGHT);
        Position playerPositionAfterOnDoor =GameFile.getPlayer(GameFile.getEntities()).getPosition();

        game.tick(null, Direction.LEFT);
        Position playerPositionAgainOnDoor = GameFile.getPlayer(GameFile.getEntities()).getPosition();

        // Since the door is unlocked, the player can go over the door, so they should have the same position walking onto it initially as walking back onto it.
        assertEquals(playerPositionOnDoor, playerPositionAgainOnDoor);

        // The players position should change when a door is unlocked.
        assertNotEquals(playerPositionAfterCollectingKey, playerPositionOnDoor);
        assertNotEquals(playerPositionOnDoor, playerPositionAfterOnDoor);
    }

    @Test
    // Attempting to get 2 keys.
    public void testAttemptingGetTwoKeys() {
        DungeonManiaController game = new DungeonManiaController();
        game.newGame("doors_and_keys", "Peaceful");

        // Getting the first key, showing its in the player's inventory.
        DungeonResponse getOneKey = game.tick(null, Direction.RIGHT);
        assertTrue(getOneKey.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("key"))
        );

        // Moving onto the second key, the player still only has 1 key in their inventory, and there is still a key on the ground under the player.
        game.tick(null, Direction.LEFT);
        DungeonResponse attemptingSecondKey = game.tick(null, Direction.LEFT);
        assertEquals(1, attemptingSecondKey.getInventory().size());
        assertTrue(attemptingSecondKey.getEntities().stream().anyMatch(e -> e.getType().equals("key")));

        // The player is standing on the second key, but cannot pick it up, since the player already has a key.
        Position playerPosition = GameFile.getPlayer(GameFile.getEntities()).getPosition();
        Position keyPosition = attemptingSecondKey.getEntities().stream().filter(e -> e.getType().equals("player")).findFirst().get().getPosition();
        assertEquals(playerPosition, keyPosition);
    }

}