
package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import dungeonmania.gameFile.GameFile;
import dungeonmania.util.*;

public class TestSpider {
    @Test
    //Testing one spider can be created
    public void TestSpiderCreated() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("spiderMovement", "Standard"));

        // Spider should move one position towards player
        dungeon.tick("", Direction.RIGHT);
        assertTrue(GameFile.getEntities().stream().anyMatch(e->e.getType().equals("spider") == true));
    }


    @Test
    //Testing spider spawns after
    public void TestSpiderSpawns() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("spiderSpawn", "Standard"));

        //move 25 ticks and check a spider is now spawned
        for (int i = 0; i < 26; i++) {
            dungeon.tick("",Direction.RIGHT);
        }
            
        assertTrue(GameFile.getEntities().stream().anyMatch(e->e.getType().equals("spider") == true));
    }

    @Test
    //Testing spider movement in empty space
    public void TestSpiderMoves() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("spiderMovement", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // Spider should move one position towards player
        dungeon.tick("",Direction.UP);
        assertTrue(game.getPositionEntity("2").equals(new Position(0, -1)));
        dungeon.tick("",Direction.UP);
        assertTrue(game.getPositionEntity("2").equals(new Position(1, -1)));
        dungeon.tick("",Direction.UP);
        assertTrue(game.getPositionEntity("2").equals(new Position(1, 0)));
        dungeon.tick("",Direction.UP);
        assertTrue(game.getPositionEntity("2").equals(new Position(1, 1)));
        dungeon.tick("",Direction.UP);
        assertTrue(game.getPositionEntity("2").equals(new Position(0, 1)));
        dungeon.tick("",Direction.UP);
        assertTrue(game.getPositionEntity("2").equals(new Position(-1, 1)));
        dungeon.tick("",Direction.UP);
        assertTrue(game.getPositionEntity("2").equals(new Position(-1, 0)));
        dungeon.tick("",Direction.UP);
        assertTrue(game.getPositionEntity("2").equals(new Position(-1, -1)));
    }
    

    @Test
    //Testing one spider can move through walls
    public void TestSpiderWall() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("spiderWall", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // move the player and checks spider has also moved
        dungeon.tick("",Direction.RIGHT);
        assertTrue(game.getPositionEntity("2").equals(new Position(0, -1)));
    }

    @Test
    //Testing spider can go through doors
    public void TestSpiderDoorClosed() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("spiderDoorClosed", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        dungeon.tick("",Direction.RIGHT);
        assertTrue(game.getPositionEntity("2").equals(new Position(0, -1)));
    }

    @Test
    //Testing spider cannot move onto an enemy
    public void TestSpiderOnEnemy() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("spiderEnemy", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        dungeon.tick("",Direction.DOWN);
        assertTrue(game.getPositionEntity("3").equals(new Position(0, 1)));

        dungeon.tick("",Direction.DOWN);

        assertTrue(game.getPositionEntity("3").equals(new Position(0, 1)));
    }
    
    @Test
    //Tests to see if spider reverses its direction
    public void TestSpiderOnBoulder() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("spiderBoulder", "Standard"));
        GameFile game = dungeon.getCurrentGame();
        
        dungeon.tick("",Direction.DOWN);
        assertTrue(game.getPositionEntity("2").equals(new Position(0, -1)));

        dungeon.tick("",Direction.DOWN);
        assertTrue(game.getPositionEntity("2").equals(new Position(1, -1)));

        //Spuder has hit the boulder and now begins to reverse its positions
        dungeon.tick("",Direction.DOWN);
        assertTrue(game.getPositionEntity("2").equals(new Position(1, -1)));

        dungeon.tick("",Direction.DOWN);
        assertTrue(game.getPositionEntity("2").equals(new Position(0, -1)));

        dungeon.tick("",Direction.DOWN);
        assertTrue(game.getPositionEntity("2").equals(new Position(-1, -1)));
    }
}
