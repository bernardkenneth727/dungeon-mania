
package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import dungeonmania.entities.*;
import dungeonmania.entities.enemy.Enemy;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.*;

public class TestMercenary {
    @Test
    //Testing one mercenary can move and follows player
    public void TestMercenaryMoves() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("mercenaryMovement", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // Mercenary should move one position towards player

        game.tick(Direction.RIGHT);
        System.out.println("I have ticking problems");
        assertTrue(game.getPositionEntity("2").equals(new Position(2, 1)));
    }

    @Test 
    public void testMercenarySpawns() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();

        assertDoesNotThrow(() -> dungeon.newGame("assassinSpawner", "Standard"));

        for (int i = 0; i < 40; i++) {
            dungeon.tick(null, Direction.DOWN);
        }
        // check that assassin has spawned at least once 
        long counterFirst = GameFile.getEntities().stream().filter(t -> t.getType().equals("mercenary")).collect(Collectors.counting());
        counterFirst += GameFile.getEntities().stream().filter(t -> t.getType().equals("assassin")).collect(Collectors.counting());
        assertTrue(counterFirst >= 1);
    }
    
    @Test
    public void TestIntoPlayer() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("mercenaryIntoPlayer", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // Mercenary should move one position towards player

        //game.tick(Direction.RIGHT);
        if (game.getEnt("2").getHasArmour()) {
            dungeon.tick(null, Direction.LEFT);
            System.out.println("in here");
            assertTrue(GameFile.getPlayer(game.getEntities()) == null);
        }
        else {
            dungeon.tick(null, Direction.LEFT);
            assertTrue(GameFile.getPlayer(game.getEntities()) != null);
            assertFalse(game.getEntities().stream().anyMatch(e->e.getType().equals("mercenary")));
        }
    }

    @Test
    public void TestPlayerIntoMerc() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("playerIntoMercenary", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // Mercenary should move one position towards player

        //game.tick(Direction.RIGHT);
        if (game.getEnt("2").getHasArmour()) {
            dungeon.tick(null, Direction.LEFT);
            System.out.println("in here");
            assertTrue(GameFile.getPlayer(game.getEntities()) == null);
        }
        else {
            dungeon.tick(null, Direction.LEFT);
            assertTrue(GameFile.getPlayer(game.getEntities()) != null);
            assertFalse(game.getEntities().stream().anyMatch(e->e.getType().equals("mercenary")));
        }
    }
    

    @Test
    //Testing Mercenary Movement for all directions without wall blockage
    public void TestMercenaryFollows() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("mercenaryFollow", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // Mercenary should move one position towards player

        game.tick(Direction.UP);
        assertTrue(game.getPositionEntity("2").equals(new Position(1, 0)));
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("2").equals(new Position(2, 0)));
        game.tick(Direction.DOWN);
        assertTrue(game.getPositionEntity("2").equals(new Position(2, 1)));
    }

    @Test
    //Testing one mercenary cannot move through walls
    public void TestMercenaryWall() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("mercenaryWallBoulder", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // move the player and checks mercenary is in spwn position
        game.tick(Direction.UP);
        assertTrue(game.getPositionEntity("2").equals(new Position(0, 0)));
    }

    @Test
    //Testing mercenary cannot move through door after opened
    public void TestMercenaryDoor() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("mercenaryDoor", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        //Checks mercenary can now move onto the door
        game.tick(Direction.UP);
        game.tick(Direction.LEFT);
        game.tick(Direction.RIGHT);
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("2").equals(new Position(3, 0)));
    }

    @Test
    //Testing one mercenary cannot move through locked doors
    public void TestMercenaryDoorClosed() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("mercenaryDoor", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // move the player and spawn a single merc
        game.tick(Direction.UP);
        game.tick(Direction.UP);
        game.tick(Direction.RIGHT);

        assertTrue(game.getPositionEntity("2").equals(new Position(0, 0)));
    }

    @Test
    //Testing one mercenary cannot move on top of another enemy
    public void TestMercenaryOnEnemy() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("enemyEnclosed", "Standard"));
        GameFile game = dungeon.getCurrentGame();
        
        game.tick(Direction.DOWN);


        assertTrue(game.getPositionEntity("3").equals(new Position(1, 0)));
    }

    @Test
    public void TestMercenaryOnBoulder() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("mercenaryWallBoulder", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        
        game.tick(Direction.UP);
        game.tick(Direction.UP);

        assertTrue(game.getPositionEntity("2").equals(new Position(0, 0)));
    }

    @Test
    //Checks if armour is spwned on Mercenaries
    public void TestRandomMercenaryArmour() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard and a map with multiple mercenaries
        assertDoesNotThrow(() -> dungeon.newGame("mercenaryMultiple", "Standard"));

        //Checks that at least one merc spawned out has armour
        int counter = 0;
        List<Entity> mercenaries = GameFile.getEntities().stream().filter(p -> p.getType().equals("mercenary")).collect(Collectors.toList());
        for (Entity merc : mercenaries) {
            Enemy mercEnemy = (Enemy) merc;
            if (mercEnemy.isHasArmour()) {
                counter++;
            }
        }
        
        assertTrue(counter > 0);
    }
}
