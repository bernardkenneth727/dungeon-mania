package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import dungeonmania.gameFile.GameFile;
import dungeonmania.response.models.DungeonResponse;
import dungeonmania.util.Direction;
import dungeonmania.entities.itemEntity.collectableEntity.Wood;
import dungeonmania.entities.itemEntity.ItemEntity;
public class TestInventory {
    @Test 
    public void TestCreateInventory() {
        DungeonManiaController game = new DungeonManiaController();

        game.newGame("inventory", "Standard");
        DungeonResponse emptyInventory = game.tick(null, Direction.UP);
        assertTrue(emptyInventory.getInventory() != null);
    }
    
    @Test 
    //Test Inventory can add item
    public void TestAddItem() {
        DungeonManiaController game = new DungeonManiaController();

        game.newGame("inventory", "Standard");
        DungeonResponse emptyInventory = game.tick(null, Direction.UP);
        assertTrue(emptyInventory.getInventory().size() == 0);
        game.tick(null, Direction.DOWN);
        DungeonResponse collectedOne = game.tick(null, Direction.RIGHT);

        assertTrue(collectedOne.getInventory().size() == 1);
        assertTrue(
            collectedOne.getInventory()
            .stream()
            .anyMatch(e -> e.getType().equals("invincibility_potion"))
        );
    }


    @Test 
    //Tests removing from inventory
    public void TestUseItem() {
        DungeonManiaController game = new DungeonManiaController();
        game.newGame("inventory", "Standard");
        GameFile dungeon = game.getCurrentGame();

        DungeonResponse emptyInventory = game.tick(null, Direction.UP);
        assertTrue(emptyInventory.getInventory().size() == 0);
        game.tick(null, Direction.DOWN);

        //checking one item is in inventory
        DungeonResponse collectedOne = game.tick(null, Direction.RIGHT);
        assertTrue(collectedOne.getInventory().size() == 1);
        //Using item
        dungeon.getInventory().useItem("invincibility_potion");
        assertTrue(dungeon.getInventory().getAllItems().size() == 0);
    }

    @Test 
    //Test Searching for item from item ID
    public void TestFindItem() {
        DungeonManiaController game = new DungeonManiaController();
        game.newGame("inventory", "Standard");
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        GameFile dungeon = game.getCurrentGame();
        ItemEntity inventoryWood = dungeon.getInventory().findItem("3");
        assertTrue(inventoryWood instanceof Wood);

    }


    @Test 
    //Test function that returns number of items of that type
    public void TestNumberOf() {
        DungeonManiaController game = new DungeonManiaController();
        game.newGame("inventory", "Standard");
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        GameFile dungeon = game.getCurrentGame();
        assertTrue(dungeon.getInventory().numberOf("wood") == 2);
        assertTrue(dungeon.getInventory().numberOf("invincibility_potion") == 1);
    }
}

