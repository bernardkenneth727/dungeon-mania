package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import dungeonmania.entities.Inventory;
import dungeonmania.entities.Player;
import dungeonmania.gameFile.*;
import dungeonmania.util.Direction;

public class TestAnduril {
    
    @Test
    public void TestAndurilInventory() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
                
        // create new game with gamemode standard
        dungeon.newGame("anduril", "Standard");
        
        //move onto anduril
        dungeon.tick(null, Direction.RIGHT);

        //check inventory 
        Player player = (Player) GameFile.getPlayer(GameFile.getEntities());
        Inventory inventory = player.getInventory(); 
        assertTrue(inventory.numberOf("anduril") > 0 ); 
    }


}
