package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import dungeonmania.gameFile.GameFile;
import dungeonmania.response.models.*;
import dungeonmania.util.*;

public class TestSaveLoadGame {

    @Test 
    public void testSaveSuccessful() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        dungeon.newGame("playerMovement", "Standard");

        // save the game
        DungeonResponse saved = dungeon.saveGame("playerMovement1");

        // move up
        dungeon.tick(null, Direction.UP);

        // check that the player has changed postions to where it started off
        assertTrue(saved.getEntities().get(15).getPosition().equals(new Position(2, 2)));
    }
    @Test 
    public void testTest() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        dungeon.newGame("playerMovement", "Standard");

        // save the game
        DungeonResponse saved = dungeon.saveGame("test");

        // move up
        dungeon.tick(null, Direction.UP);

        // check that the player has changed postions to where it started off
        assertTrue(saved.getEntities().get(15).getPosition().equals(new Position(2, 2)));
    }
    @Test 
    public void testSaveSuccessful1() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        dungeon.newGame("playerMovement", "Standard");
        dungeon.tick(null, Direction.UP);
        
        // save the game
        DungeonResponse saved = dungeon.saveGame("playerMovement1");
        
        // check that the player has changed postions to where it started off
        assertTrue(saved.getEntities().get(15).getPosition().equals(new Position(2, 1)));
    }

    @Test 
    public void testSaveNotSuccessfulDoesNotExist() {
        DungeonManiaController dungeon = new DungeonManiaController();
        assertThrows(IllegalArgumentException.class ,  () -> dungeon.saveGame("playerMovement1"));
    }
    @Test
    public void testSaveNotSuccessfulAlreadyComplete() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("exitGoal", "Standard");

        DungeonResponse saved = dungeon.saveGame("exitGoal1");

        dungeon.tick(null, Direction.DOWN);

        assertTrue(saved.getEntities().get(8).getPosition().equals(new Position(2, 2)));

        assertThrows(IllegalArgumentException.class, () -> dungeon.saveGame("exitGoal2"));
    }

    @Test
    public void testLoadSuccessful(){
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("exitGoal", "Standard");

        DungeonResponse saved = dungeon.saveGame("exitGoal1");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        dungeon.tick(null, Direction.UP);

        assertTrue(saved.getEntities().get(8).getPosition().equals(new Position(2, 2)));

        //assertDoesNotThrow(() -> dungeon.loadGame(saved.getDungeonId()));
        dungeon.loadGame(saved.getDungeonId());
        
        assertTrue(GameFile.getEntities().get(8).getPosition().equals(new Position(2, 2)));
    }

    @Test
    public void testLoadSuccessfulAfterMoved(){
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("exitGoal", "Standard");
        dungeon.tick(null, Direction.UP);

        DungeonResponse saved = dungeon.saveGame("exitGoal1");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertTrue(saved.getEntities().get(8).getPosition().equals(new Position(2, 1)));
        dungeon.newGame("exitGoal", "Standard");

        assertDoesNotThrow(() -> dungeon.loadGame(saved.getDungeonId()));
        
        assertTrue(GameFile.getEntities().get(8).getPosition().equals(new Position(2, 1)));
    }   

    @Test
    public void testLoadSuccessfulThenMove(){
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("exitGoal", "Standard");

        DungeonResponse saved = dungeon.saveGame("exitGoal1");

        dungeon.tick(null, Direction.UP);

        assertTrue(saved.getEntities().get(8).getPosition().equals(new Position(2, 2)));

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertDoesNotThrow(() -> dungeon.loadGame(saved.getDungeonId()));
        assertTrue(GameFile.getEntities().get(8).getPosition().equals(new Position(2, 2)));
        
        System.out.println(GameFile.getEntities());

        dungeon.tick(null, Direction.UP);
        assertTrue(GameFile.getEntities().get(8).getPosition().equals(new Position(2, 1)));
        
        dungeon.tick(null, Direction.DOWN);
        dungeon.tick(null, Direction.DOWN);

        assertThrows(IllegalArgumentException.class, () -> dungeon.saveGame("exitGoal2"));
    }
    @Test
    public void testLoadSuccessfulInventory(){
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("buildables", "Standard");

        dungeon.tick(null, Direction.RIGHT);
        dungeon.tick(null, Direction.RIGHT);
        dungeon.tick(null, Direction.RIGHT);
        dungeon.tick(null, Direction.RIGHT);
        dungeon.tick(null, Direction.RIGHT);
        dungeon.tick(null, Direction.RIGHT);

        DungeonResponse saved = dungeon.saveGame("inventory1");
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        //assertDoesNotThrow(() -> dungeon.loadGame(saved.getDungeonId()));
        dungeon.loadGame(saved.getDungeonId());
        DungeonResponse current = dungeon.getCurrentGame().getDungeon();
        assertTrue(current.getBuildables().equals(saved.getBuildables()));
        assertTrue(current.getDungeonName().equals(saved.getDungeonName()));
        /* asserts commented out will not pass since the Enitity and Item Responses are considered to be different
        even if the contents inside each response are exactly the same 
        
        assertTrue(current.getEntities().equals(saved.getEntities()));
        assertTrue(current.getInventory().equals(saved.getInventory()));


        */

    } 
    
    @Test 
    public void testLoadGameSwitchAndFinish() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("switchGoal", "Standard");

        DungeonResponse saved = dungeon.saveGame("goal");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        dungeon.loadGame(saved.getDungeonId());
        GameFile game = dungeon.getCurrentGame();
        System.out.println(saved.getGoals());
        System.out.println(dungeon.getCurrentGame().getGoals());
        dungeon.tick(null, Direction.DOWN);
        assertTrue(game.checkGoalComplete() == true);
    }

    @Test
    public void testLoadGameAdvancedGoal() {
        DungeonManiaController dungeon = new DungeonManiaController();

        dungeon.newGame("advanced", "Standard");

        DungeonResponse saved = dungeon.saveGame("advanced");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        dungeon.loadGame(saved.getDungeonId());

        System.out.println(saved.getGoals());
        System.out.println(dungeon.getCurrentGame().getGoals());
    }
}
