package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import dungeonmania.gameFile.*;
import dungeonmania.util.*;

public class TestNewGame {
    @Test
    public void testCreateNewGameFileDoesNotExist(){
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        assertThrows(IllegalArgumentException.class, () -> dungeon.newGame("doesNotExist", "Standard"));
    }
    
    @Test
    public void testCreateNewGameModeDoesNotExist(){
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        assertThrows(IllegalArgumentException.class, () -> dungeon.newGame("playerMovement", "doesNotExist"));
    }

    @Test 
    public void testCreateNewGameSimple() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("playerMovement", "Standard"));

        GameFile game = dungeon.getCurrentGame();

        // check dimensions are correct
        assertTrue(game != null);

        Position playerPos = new Position(2,2);
        assertTrue(game.getPositionEntity("16").equals(playerPos)); // player

        assertTrue(game.getGoals() == "there is no goal");
    }

    @Test 
    public void testCreateNewGameMaze() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
            
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("maze", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // check dimensions are correct
        assertTrue(game != null);

        assertTrue(game.getPositionEntity("22").equals(new Position(1,1))); //player
        assertTrue(game.getPositionEntity("185").equals(new Position(18,16))); //exit

        // check goals are correct
        assertTrue(game.getGoals().equals(":exit"));
    }
   
    @Test
    public void testCreateNewGameAdvanced() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
            
        dungeon.newGame("advanced", "Standard");
        // create new game with gamemode standard
        //assertDoesNotThrow(() -> dungeon.newGame("advanced", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        // check dimensions are correct
        assertTrue(game != null);

        // check that entities have been placed in correct space
        assertTrue(game.getPositionEntity("20").equals(new Position(1,1))); //player
        assertTrue(game.getPositionEntity("21").equals(new Position(6,1))); //sword
        assertTrue(game.getPositionEntity("41").equals(new Position(13,4))); //bomb
        assertTrue(game.getPositionEntity("44").equals(new Position(3,5))); //mercenary
        assertTrue(game.getPositionEntity("78").equals(new Position(7,10))); // treasure
        assertTrue(game.getPositionEntity("79").equals(new Position(11,10))); // invincibility potion

        // check goals are correct
        assertTrue(game.getGoals().equals(":enemies AND :treasure"));
    }
}
