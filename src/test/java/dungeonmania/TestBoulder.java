package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import dungeonmania.entities.Boulder;
import dungeonmania.entities.Player;
import dungeonmania.gameFile.GameFile;
import dungeonmania.response.models.DungeonResponse;
import dungeonmania.response.models.EntityResponse;
import dungeonmania.util.Direction;
import dungeonmania.util.Position;

@TestInstance(value = Lifecycle.PER_CLASS)
public class TestBoulder {
    @Test
    // Testing that moving to the right in the Boulder Map actually moves the boulder 
    public void testBoulderPushing() {
        DungeonManiaController game = new DungeonManiaController();
        // Gets the DungeonResponse before an initial boulder movement.
        DungeonResponse initialState = game.newGame("boulders", "Standard");
        // Gets the DungeonResponse after an initial boulder movement.
        DungeonResponse moveToRightState = game.tick(null, Direction.RIGHT);

        // Shows that the entities have moved, including the player and boulder.
        assertNotEquals(initialState.getEntities(), moveToRightState.getEntities());

    }

    @Test
    // Testing that 2 boulders in a row cannot be pushed.
    public void testTwoBouldersBlocking() {
        DungeonManiaController game = new DungeonManiaController();

        game.newGame("boulders", "Standard");

        // Pushes the initial right boulder out of line of the other 
        // Now has 2 boulders below player
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);
        Position pos1 = GameFile.getPlayer(GameFile.getEntities()).getPosition(); 

        // After attempting to move down with 2 boulders in a row will not work, all boulders and player stay in same location.
        game.tick(null, Direction.DOWN);
        Position pos2 = GameFile.getPlayer(GameFile.getEntities()).getPosition(); 
        assertEquals(pos1, pos2);
    }

    @Test
    // Testing that a boulder cannot be pushed into a wall.
    public void testPushBoulderWall() {
        DungeonManiaController game = new DungeonManiaController();

        game.newGame("boulders", "Standard");

        // Pushes boulder against a wall after 2 movements.
        game.tick(null, Direction.RIGHT);
        game.tick(null, Direction.RIGHT);

        //check positions of player and boulder 
        Player player = (Player) GameFile.getPlayer(GameFile.getEntities());
        Position playerPos1 = player.getPosition(); 
        Boulder boulder = (Boulder) game.getCurrentGame().SingleEntityFromPos(new Position((playerPos1.getX()+1), playerPos1.getY()));
        Position boulderPos1 = boulder.getPosition();

        // Attempts to push the boulder further into the wall and the state should remain the same as before in terms of entity positions.
        game.tick(null, Direction.RIGHT);
        assertEquals(playerPos1, player.getPosition());
        assertEquals(boulderPos1, boulder.getPosition());
    }

    @Test
    // Testing pushing a boulder onto a floor switch.
    public void testBoulderOnSwitches() {
        DungeonManiaController game = new DungeonManiaController();

        game.newGame("boulders", "Standard");

        // Moves to the spot before moving a boulder onto a switch.
        game.tick(null, Direction.RIGHT);
        DungeonResponse beforeSwitch = game.tick(null, Direction.DOWN);
        List<EntityResponse> beforeSwitchEntities = beforeSwitch.getEntities();

        // Shows that at the position desired to trigger the switch, there is currently a switch.
        assertTrue(
            beforeSwitchEntities.stream().anyMatch(e -> e.getPosition().equals(new Position(5, 3)) && e.getType().equals("switch"))
        );
        
        // Moves the boulder onto the switch to be triggered.
        DungeonResponse afterSwitch1 = game.tick(null, Direction.RIGHT);
        List<EntityResponse> afterSwitchEntities1 = afterSwitch1.getEntities(); 
        

        // Shows that at that position the switch is now triggered by showing there is a boulder there.
        assertTrue(
            afterSwitchEntities1.stream().anyMatch(e -> e.getPosition().equals(new Position(5, 3)) && e.getType().equals("boulder"))
        );

        //move boulder off switch and see postion 
        game.tick(null, Direction.UP);  
        game.tick(null, Direction.RIGHT);
        DungeonResponse afterSwitch2 = game.tick(null, Direction.DOWN);      
        List<EntityResponse> afterSwitchEntities2 = afterSwitch2.getEntities(); 
        
        assertFalse(
            afterSwitchEntities2.stream().anyMatch(e -> e.getPosition().equals(new Position(5, 3)) && e.getType().equals("boulder"))
        );

    }


}
