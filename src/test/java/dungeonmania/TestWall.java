package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import dungeonmania.entities.Entity;
import dungeonmania.entities.Player;
import dungeonmania.util.Direction;
import dungeonmania.util.Position;
import dungeonmania.gameFile.*;

public class TestWall {

    @Test 
    //test player movement around wall 
    public void TestWallMovement() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("advanced", "Standard"));

        // check that the game loaded correctly
        //assertTrue(dungeon.getCurrentGame().getPositionEntity("16").equals(new Position(2, 2)) ); // player is in pos 2,2

        //check position of character at start
        Player player = (Player) GameFile.getPlayer(GameFile.getEntities()); 
        Position pos1 = player.getPosition(); 

        // try to move the charracter left  - wall should block movement 
        dungeon.tick("none", Direction.LEFT);

        // check the position of the player after movement 
        Position pos2 = player.getPosition(); 
        assertEquals(pos1, pos2); 
    }


    //test position of wall
    @Test 
    //test player movement around wall 
    public void TestWallBorder() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("advanced", "Standard"));

        // check that the game loaded correctly
        //assertTrue(dungeon.getCurrentGame().getPositionEntity("16").equals(new Position(2, 2)) ); // player is in pos 2,2

        //Find entity next to player (border)
        Player player = (Player) GameFile.getPlayer(GameFile.getEntities()); 
        Position pos1 = player.getPosition();
        Entity entity = dungeon.getCurrentGame().SingleEntityFromPos(new Position((pos1.getX() -1), pos1.getY())); 

        // check the entity is a wall
        assert(entity.getType().equals("wall"));
    }
    
}
