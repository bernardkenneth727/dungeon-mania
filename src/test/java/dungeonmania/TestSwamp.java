package dungeonmania;


import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


import org.junit.jupiter.api.Test;

import dungeonmania.gameFile.GameFile;
import dungeonmania.util.*;

public class TestSwamp {

    @Test
    //Testing players are not affected by swamp tile
    public void TestPlayerInSwamp() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("swampPlayerMerc", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("1").equals(new Position(1, 0)));
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("1").equals(new Position(2, 0)));
        
    }

    @Test
    //Mercenary is stalled in the swamp
    public void TestMercInSwamp() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("swampPlayerMerc", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("3").equals(new Position(0, 0)));
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("3").equals(new Position(1, 0)));
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("3").equals(new Position(1, 0)));
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("3").equals(new Position(2, 0)));

    }

    @Test
    //Spider is stalled in swamp
    public void TestSpiderinSwamp() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("swampSpider", "Standard"));
        GameFile game = dungeon.getCurrentGame();
        
        game.tick(Direction.DOWN);
        assertTrue(game.getPositionEntity("3").equals(new Position(0, -1)));

        game.tick(Direction.DOWN);
        assertTrue(game.getPositionEntity("3").equals(new Position(0, -1)));

        game.tick(Direction.DOWN);
        assertTrue(game.getPositionEntity("3").equals(new Position(1, -1)));
    }

    //Test that zombies are stalled in swamptile
    @Test
    public void TestZombieinSwamp() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("swampZombie", "Standard"));
        GameFile game = dungeon.getCurrentGame();
        
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("1").equals(new Position(2, 1)));

        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("1").equals(new Position(2, 1)));

        game.tick(Direction.RIGHT);
        if (game.getPositionEntity("1").equals(new Position(3, 1))) {
            assertTrue(game.getPositionEntity("1").equals(new Position(3, 1)));
        } else {
            assertTrue(game.getPositionEntity("1").equals(new Position(1, 1)));
        }
    }

    //Test that boulders move in swamptile
    @Test
    public void TestBoulderinSwamp() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("swampBoulder", "Standard"));
        GameFile game = dungeon.getCurrentGame();
        
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("2").equals(new Position(2, 0)));
        

        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("2").equals(new Position(3, 0)));
        assertTrue(game.getPositionEntity("1").equals(new Position(2, 0)));

        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("2").equals(new Position(3, 0)));
        assertTrue(game.getPositionEntity("1").equals(new Position(2, 0)));

        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("2").equals(new Position(4, 0)));
        assertTrue(game.getPositionEntity("1").equals(new Position(3, 0)));
    }

    //Test that variation of the swamp factor occurs
    @Test
    public void TestSwampFactor() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("swampFactor", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("3").equals(new Position(0, 0)));
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("3").equals(new Position(0, -1)));
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("3").equals(new Position(1, -1)));
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("3").equals(new Position(2, -1)));
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("3").equals(new Position(2, 0)));
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("3").equals(new Position(3, 0)));

    }
    @Test
    public void TestSwampFactor2() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("swampFactor2", "Standard"));
        GameFile game = dungeon.getCurrentGame();
        
        game.tick(Direction.DOWN);
        assertTrue(game.getPositionEntity("3").equals(new Position(0, -1)));

        game.tick(Direction.DOWN);
        assertTrue(game.getPositionEntity("3").equals(new Position(0, -1)));
        game.tick(Direction.DOWN);
        assertTrue(game.getPositionEntity("3").equals(new Position(0, -1)));
        game.tick(Direction.DOWN);
        assertTrue(game.getPositionEntity("3").equals(new Position(0, -1)));


        game.tick(Direction.DOWN);
        assertTrue(game.getPositionEntity("3").equals(new Position(1, -1)));
    }

    //Dijkstra's test for mercenary where the swamp tile has factor of 2. There are walls above and below it
    //so the quickest way to player is through the swamp.
    @Test
    public void TestMercMustWalkInSwamp() {
        // create new dungeon 
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("swampMercWalk", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("3").equals(new Position(0, 0)));
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("3").equals(new Position(1, 0)));
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("3").equals(new Position(1, 0)));
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("3").equals(new Position(2, 0)));

    }

    @Test
    //Checks that the time delay on swamp persist even if they are originally spawned on a swamp tile.
    public void TestSwampSpawn() {
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("swampSpawn", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("4").equals(new Position(0, -1)));
        assertTrue(game.getPositionEntity("5").equals(new Position(3, 4)));
        game.tick(Direction.RIGHT);
        assertTrue(game.getPositionEntity("4").equals(new Position(0, -2)));
        assertFalse(game.getPositionEntity("5").equals(new Position(3, 4)));
    
    }

    @Test
    public void TestSwampMultipleSpawn() {
        DungeonManiaController dungeon = new DungeonManiaController();
        
        // create new game with gamemode standard
        assertDoesNotThrow(() -> dungeon.newGame("swampMultiple", "Standard"));
        GameFile game = dungeon.getCurrentGame();

        game.tick(Direction.DOWN);
        assertTrue(game.getPositionEntity("4").equals(new Position(0, -1)));

        game.tick(Direction.DOWN);
        assertTrue(game.getPositionEntity("4").equals(new Position(0, -1)));

        game.tick(Direction.DOWN);
        assertTrue(game.getPositionEntity("4").equals(new Position(1, -1)));

        game.tick(Direction.DOWN);
        assertTrue(game.getPositionEntity("4").equals(new Position(1, -1)));

        game.tick(Direction.DOWN);
        assertTrue(game.getPositionEntity("4").equals(new Position(1, 0)));
    
    
    }   
}
