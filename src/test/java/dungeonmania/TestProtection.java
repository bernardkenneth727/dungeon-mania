package dungeonmania;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import dungeonmania.entities.itemEntity.ItemEntity;
import dungeonmania.entities.itemEntity.collectableEntity.rare.Rare;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Armour;
import dungeonmania.gameFile.GameFile;
import dungeonmania.response.models.DungeonResponse;
import dungeonmania.util.Direction;

@TestInstance(value = Lifecycle.PER_CLASS)
public class TestProtection {
    @Test
    // Testing whether having armour protects the player more than without having armour.
    public void testArmourProtection() {
        // Creating a new game instance where the player has armour.
        DungeonManiaController game1 = new DungeonManiaController();
        game1.newGame("midnight_armour_enemies", "Standard");

        // Showing the player has armour in the inventory and that there is a spider on the ground.
        DungeonResponse hasArmour = game1.tick(null, Direction.RIGHT);
        assertTrue(hasArmour.getInventory().stream().anyMatch(e -> e.getType().equals("armour")));
        assertTrue(hasArmour.getEntities().stream().anyMatch(e -> e.getType().equals("spider")));

        game1.tick(null, Direction.RIGHT);
        game1.tick(null, Direction.RIGHT);

        // The player has now battled a spider and there should no longer be a spider on the ground.
        DungeonResponse afterSpider = game1.tick(null, Direction.RIGHT);
        assertFalse(afterSpider.getEntities().stream().anyMatch(e -> e.getType().equals("spider")));

        // Getting the health of the player after battling a spider with armour.
        double playerHealth1 = GameFile.getPlayer(GameFile.getEntities()).getHealth();

        // Creating another new game instance where the player does not have armour but the same map.
        DungeonManiaController game2 = new DungeonManiaController();
        game2.newGame("midnight_armour_enemies", "Standard");

        // The player gets armour, and there is a spider on the ground.
        DungeonResponse hasArmour2 = game2.tick(null, Direction.RIGHT);
        assertTrue(hasArmour2.getInventory().stream().anyMatch(e -> e.getType().equals("armour")));
        assertTrue(hasArmour2.getEntities().stream().anyMatch(e -> e.getType().equals("spider")));

        // We now get rid of the armour from the player's inventory, to prove the test.
        GameFile gameFile2 = game2.getCurrentGame();
        gameFile2.getInventory().useItem("armour");

        // We now show that there is no armour in the player's inventory.
        DungeonResponse removedArmour2 = game2.tick(null, Direction.RIGHT);
        assertFalse(removedArmour2.getInventory().stream().anyMatch(e -> e.getType().equals("armour")));
        
        game2.tick(null, Direction.RIGHT);

        // There should now no longer be a spider in the game as the player has defeated it.
        DungeonResponse afterSpider2 = game2.tick(null, Direction.RIGHT);
        assertFalse(afterSpider2.getEntities().stream().anyMatch(e -> e.getType().equals("spider")));

        // Getting the health of the player after defeating the spider without armour.
        double playerHealth2 = GameFile.getPlayer(GameFile.getEntities()).getHealth();

        // The health of the player in the first instance with armour should have been greater than the 
        // second instance without armour, after fighting the spider.
        assertNotEquals(playerHealth1, playerHealth2);
        assertTrue(playerHealth1 > playerHealth2);
        
    }

    @Test
    // A test to show the protective quality of a shield by comparing with shield to without shield.
    public void testShieldProtection() {
        // This instance of a new game has a shield.
        DungeonManiaController game1 = new DungeonManiaController();

        // Shows that the player's inventory is initially empty.
        DungeonResponse initialState = game1.newGame("buildables_enemies", "Standard");
        assertTrue(initialState.getInventory().isEmpty());
        assertTrue(initialState.getEntities().stream().anyMatch(e -> e.getType().equals("spider")));

        // Collecting all the items for building a shield.
        for (int i = 0; i < 6; i++) {
            game1.tick(null, Direction.RIGHT);
        }

        // Builds the shield and shows that the shield is now in the player's inventory.
        assertDoesNotThrow(() -> {
            DungeonResponse afterBuilding = game1.build("shield");
            assertTrue(afterBuilding.getInventory()
            .stream()
            .anyMatch(i -> i.getType().equals("shield")));
        });

        // The spider is no longer in the game, so it has been battled.
        DungeonResponse afterBattlingSpider = game1.tick(null, Direction.RIGHT);
        assertFalse(afterBattlingSpider.getEntities().stream().anyMatch(e -> e.getType().equals("spider")));

        // Getting the player's current health in game.
        double playerHealth1 = GameFile.getPlayer(GameFile.getEntities()).getHealth();

        // Second instance of a game where the player does not build a shield.
        DungeonManiaController game2 = new DungeonManiaController();

        game2.newGame("buildables_enemies", "Standard");

        // Walking directly to the spider.
        for (int i = 0; i < 7; i++) {
            game2.tick(null, Direction.RIGHT);
        }

        // Getting the player's health after battling a spider without a shield.
        double playerHealth2 = GameFile.getPlayer(GameFile.getEntities()).getHealth();

        // The instance where the player has a shield should have greater health than the instance of the player not having a shield after battling a spider.
        assertNotEquals(playerHealth1, playerHealth2);
        assertTrue(playerHealth1 > playerHealth2);

    }

    @Test
    // Showing that armour gets depleted when fighting enemies.
    public void testArmourDurability() {
        DungeonManiaController game = new DungeonManiaController();

        game.newGame("zombieMultiple", "Standard");
        GameFile myGame = game.getCurrentGame();
        
        // Getting one hundred one rings, so that the player stays alive when battling all the mercenaries.
        for (int i = 0; i < 100; i++) {
            Rare.battleWinnings(myGame.getInventory(), 2);
        }

        // Giving the player Armour.
        ItemEntity myArmour = new Armour(null, "-100");
        myGame.getInventory().addItem(myArmour);

        // Showing that the player has armour.
        DungeonResponse beforeDepletingArmour = game.tick(null, Direction.DOWN);
        assertTrue(beforeDepletingArmour.getInventory().stream().anyMatch(e -> e.getType().equals("armour")));
        
        // Getting the armour id so we can check that the player will no longer have this specific armour afterwards.
        String armourId = beforeDepletingArmour.getInventory()
            .stream()
            .filter(e -> e.getType().equals("armour"))
            .findFirst()
            .get()
            .getId();

        // The player fights mercenaries for a while.
        for (int i = 0; i < 20; i++) {
            game.tick(null, Direction.UP);
            game.tick(null, Direction.DOWN);
        }

        // After fighting for a while, the player's original armour has deteriorated and is no longer in the players inventory.
        DungeonResponse afterDepletingArmour = game.tick(null, Direction.RIGHT);
        assertFalse(afterDepletingArmour.getInventory().stream().anyMatch(e -> e.getId().equals(armourId)));

    }
    

    @Test
    // Testing getting armour from fighting enemies.
    public void testGettingArmourFromEnemies() {
        DungeonManiaController game = new DungeonManiaController();

        game.newGame("zombieMultiple", "Standard");
        GameFile myGame = game.getCurrentGame();
        
        // Getting one hundred one rings, so that the player stays alive when battling all the mercenaries.
        for (int i = 0; i < 100; i++) {
            Rare.battleWinnings(myGame.getInventory(), 2);
        }

        // The player fights mercenaries for a while.
        for (int i = 0; i < 20; i++) {
            game.tick(null, Direction.UP);
            game.tick(null, Direction.DOWN);
        }

        // After fighting for a while, the player should have armour in their inventory from fighting mercenaries.
        DungeonResponse winningManyBattles = game.tick(null, Direction.RIGHT);
        assertTrue(winningManyBattles.getInventory().stream().anyMatch(e -> e.getType().equals("armour")));

    }
    
}
