package dungeonmania;

import dungeonmania.entities.Entity;
import dungeonmania.entities.Interactable;
import dungeonmania.entities.itemEntity.buildableEntity.Buildable;
import dungeonmania.entities.itemEntity.buildableEntity.BuildableFactory;
import dungeonmania.exceptions.InvalidActionException;
import dungeonmania.response.models.DungeonResponse;
import dungeonmania.util.Direction;
import dungeonmania.util.FileLoader;
import dungeonmania.util.Data;
import dungeonmania.gameFile.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

public class DungeonManiaController {

    private GameFile currentGame;

    public DungeonManiaController() {
    }

    public String getSkin() {
        return "default";
    }

    public String getLocalisation() {
        return "en_US";
    }

    public List<String> getGameModes() {
        return Arrays.asList("standard", "peaceful", "hard");
    }

    /**
     * /dungeons
     * 
     * Done for you.
     */
    public static List<String> dungeons() {
        try {
            return FileLoader.listFileNamesInResourceDirectory("/dungeons");
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    /**
     * 
     * @param dungeonName // name of dungeon map
     * @param gameMode
     * @return
     * @throws IllegalArgumentException
     * @throws IOException
     */
    public DungeonResponse newGame(String dungeonName, String gameMode) throws IllegalArgumentException {
        // throws exception when dungeonName does not exist 
        String path = dungeonName;
        List<String> listFiles = dungeons();
        if (!listFiles.contains(path)) {
            throw new IllegalArgumentException("Dungeon Does Not Exist");
        }
        gameMode = gameMode.toLowerCase();
        // throws exception when gameMode does not exist
        if (!getGameModes().contains(gameMode)) {
            throw new IllegalArgumentException("Gamemode is Invalid");
        }

        //load the json file as a data class 
        path = "/dungeons/" + dungeonName + ".json";

        Data data = null;
        try {
            //File file = new File(dungeonName + ".json");
            data = FileLoader.getFileData(FileLoader.loadResourceFile(path));
        } catch (IOException e) {
            data = null;
        }

        // construct gameFile 
        String dungeonId = dungeonName;
        currentGame = new GameFile(dungeonName, dungeonId, gameMode, data);
        
        // get DungeonResponse from gameFile
        return currentGame.getDungeon();
    }
    
    /**
     * when a game is saved, it is added to a list of saved GameFiles, gameFile is updated
     * @param dungeonId unique ID given to the game 
     * @return the Dungeon Game that has been saved
     * @throws IllegalArgumentException when game is already completed 
     */
    public DungeonResponse saveGame(String dungeonId) throws IllegalArgumentException { // save it as a json file and then reload it 
        // save the games as a json into savedGames in resources  
        // check if a game exists
        if (currentGame == null) {
            throw new IllegalArgumentException("game does not exist");
        }
        // throw exception if the game is not the current game 
        if (dungeonId.equals(null)) {
            throw new IllegalArgumentException("Invalid argument");
        }
        // throw exception if the game has been completed 
        if (currentGame.checkGoalComplete()) {
            throw new IllegalArgumentException("game already complete");
        }

        // create a new jsonFile
        JSONObject jsonSaveFile = new JSONObject();

        // add in the dungeonName 
        jsonSaveFile.put("dungeonName", currentGame.getDungeonName());
        // add in the gameMode 
        jsonSaveFile.put("gameMode", currentGame.getGameMode());
        // add list of entities 
        jsonSaveFile.put("entities", currentGame.getEntitiesAsJSON());
        // add list of inventory 
        jsonSaveFile.put("inventory", currentGame.getInventoryAsJSON());
        // add goal 
        jsonSaveFile.put("goal-condition", currentGame.getGoal().getGoalJSON());
        // save starting position
        jsonSaveFile.put("start_x", GameFile.getStartingPos().getX());
        jsonSaveFile.put("start_y", GameFile.getStartingPos().getY());

        Path path = Paths.get("src/main/resources/savedGames");
        String paths = path.toAbsolutePath().toString() + "/" + dungeonId + ".json";

        File file = new File(paths);
        file.getParentFile().mkdirs();

        // add the jsonFile as a new file to the savedGames file in src/main/java/dungeonmania
        try {     
            FileWriter newFile = new FileWriter(file);
            newFile.write(jsonSaveFile.toString());

            newFile.flush();
            newFile.close();

        } catch (IOException e) {
            System.out.println("file cannot be made");
        } 
        // return the version of the saved dungeonresponse
        return new DungeonResponse(dungeonId, currentGame.getDungeonName(), currentGame.getListEntityResponse(), 
        currentGame.getListItemResponse(), currentGame.fixListBuildables(), currentGame.getGoals());
       // return currentGame.getDungeon();
        
    }

    /**
     * 
     * @param dungeonId
     * @return 
     * @throws IllegalArgumentException
     * @throws IOException
     */
    public DungeonResponse loadGame(String dungeonId) throws IllegalArgumentException{
        // load the game from the games that have been saved but not completed
        // game will not save when completed so there can not be a file

        // check if the file exists 
        if (!allGames().contains(dungeonId)) {
            throw new IllegalArgumentException("game does not exist");
        }

        // load in the dungeon id and set the current game.
        String path = "/savedGames/" + dungeonId + ".json";
        Data data = null;
        try {
            data = FileLoader.getFileData(FileLoader.loadResourceFile(path));
        } catch (IOException e){
            data = null;
        }

        String dungeonName = data.getDungeonName();
        //String gamemode = data.getGameMode();

        // use constructor for loading games 
        this.currentGame = new GameFile(dungeonName, data);

        return currentGame.getDungeon();
    }

    /**
     * @return a list of all the games that can be reaccessed (saved)
     */
    public List<String> allGames() {
        List<String> listSavedGames = new ArrayList<String>();
        try {
            listSavedGames = FileLoader.listFileNamesInResourceDirectory("savedGames");
        } catch(IOException e) {
            System.out.println("cannot get list of savedfiles");
        }
        return listSavedGames;
    }

    /**
     * moves the player or uses an item 
     * @param itemUsed
     * @param movementDirection
     * @return
     * @throws IllegalArgumentException if item is wrong
     * @throws InvalidActionException if item does not exist in inventory
     */
    public DungeonResponse tick(String itemUsed, Direction movementDirection) throws IllegalArgumentException, InvalidActionException {
        // move in a direction OR use an item (type of entity rather than the id)
        // battles will only last 1 tick (considers all the damage taken and who will win)

        if (movementDirection.equals(Direction.NONE)) {
            currentGame.tick(itemUsed);
        }
        else {
            currentGame.tick(movementDirection);
        }

        DungeonResponse finalTick = currentGame.getDungeon();

        if (GameFile.getPlayer(GameFile.getEntities()) == null) {
            currentGame = null;
        } 
        else if (currentGame.checkGoalComplete()) {
            currentGame = null;
        }
        
        return finalTick;
    }

    /**
     * interacts with an entity in the game. either mercenary or zombie spawner
     * @param entityId
     * @return
     * @throws IllegalArgumentException
     * @throws InvalidActionException
     */
    public DungeonResponse interact(String entityId) throws IllegalArgumentException, InvalidActionException {
        List<Entity> entitiesList = GameFile.getEntities();

        List<String> interactablesList = new ArrayList<>(Arrays.asList("zombie_toast_spawner", "mercenary", "assassin"));
        // If the entityId provided does not match a valid entityId, throws an IllegalArgumentException.
        if (!entitiesList
            .stream()
            .anyMatch(e -> e.getId().equals(entityId))
        ) {
            throw new IllegalArgumentException("entityId is not a valid entity ID.");
        } 
        // If the entityId provided does match an entity, but it is not a mercenary or zombie_toast_spawner, then return at this point.
        else if (entitiesList
            .stream()
            .anyMatch(e -> e.getId().equals(entityId) && !interactablesList.contains(e.getType()))
        ) {
            return currentGame.getDungeon();
        }
        Interactable interactableEntity = (Interactable) entitiesList
            .stream()
            .filter(e -> e.getId().equals(entityId) && interactablesList.contains(e.getType()))
            .findFirst()
            .get();
        // gives mercenary treasure and destroy zombie toast spawner
        interactableEntity.interact(currentGame.getInventory(), GameFile.getEntities());
        
        return currentGame.getDungeon();
    }

    /**
     * Builds the item that is given.
     * @param buildable (String) - The type of item that is passed through.
     * @return DungeonResponse - The current state of the Dungeon.
     * @throws IllegalArgumentException Throws if buildable is not one of bow, shield.
     * @throws InvalidActionException Throws if the player does not have sufficient items to craft the buildable.
     */
    public DungeonResponse build(String buildable) throws IllegalArgumentException, InvalidActionException {
        // HashMap of the potential buildables, with String for buildable type name and the Buildable for the new buildable object.
        Map<String, Buildable> potentialBuildables = BuildableFactory.getAllBuildablesMap();

        // If the buildable parameter does not match the type of any potential 
        // buildables then throws an illegal argument exception. Does this by 
        // checking if the buildable is one of the keys of potentialBuildables.
        if (!potentialBuildables.containsKey(buildable)) {
            throw new IllegalArgumentException("Buildable is not one of bow, shield, midnight armour.");
        }

        // If the player's current buildables includes the one to build, then builds it.
        if (currentGame.fixListBuildables().contains(buildable)) {
            // Gets the buildable from the potential buildables map and then builds it.
            Buildable matchingBuildable = potentialBuildables.get(buildable);
            matchingBuildable.buildEntity(currentGame.getInventory());
        // Otherwise throws an invalid action exception, since the player does not have sufficient resources.
        } else {
            throw new InvalidActionException("The player does not have sufficient items to craft the buildable.");
        }
        return currentGame.getDungeon();
    }

    public GameFile getCurrentGame() {
        return currentGame;
    }

    /**
     * Generates a randomised dungeon, created using a randomised version of Prim's Algorithm.
     * @param xStart (int) - The x coordinate of the starting point.
     * @param yStart (int) - The y coordinate of the starting point.
     * @param xEnd (int) - The x coordinate of the ending point.
     * @param yEnd (int) - The y coordinate of the ending point.
     * @param gameMode (String) - The game mode of the dungeon that is generated i.e. peaceful, standard, hard.
     * @return DungeonResponse - The current state of the Dungeon.
     * @throws IllegalArgumentException Throws if the game mode given is not peaceful, standard or hard.
     */
    public DungeonResponse generateDungeon(int xStart, int yStart, int xEnd, int yEnd, String gameMode) throws IllegalArgumentException {
        gameMode = gameMode.toLowerCase();
        if (!getGameModes().contains(gameMode)) {
            throw new IllegalArgumentException("gameMode is not a valid game mode.");
        }

        DungeonGeneration dungeonGenerator = new DungeonGeneration(xStart, yStart, xEnd, yEnd, gameMode);
        currentGame = dungeonGenerator.generateGameFile();

        return currentGame.getDungeon();

    }

}