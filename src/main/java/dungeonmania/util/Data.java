package dungeonmania.util;

import dungeonmania.gameFile.goals.*;
import java.util.List;

// import javax.naming.Context;

import com.google.gson.annotations.SerializedName;

/**
 * class to help read the JSON files easier and translate using GSON
 */
public class Data {
    private List<EntityData> entities;
    private List<EntityData> inventory;
    private String dungeonName;
    private String gameMode;
    @SerializedName("goal-condition")
    private GoalController goal;
    private int start_x;
    private int start_y;

    
    
    
    /**
     * @return list of entities as Entities
     */
    public List<EntityData> getEntities() {
        return entities;
    }
    /**
     * @return dungeonName
     */
    public String getDungeonName() {
        return dungeonName;
    }
    /**
     * @return gameMode
     */
    public String getGameMode() {
        return gameMode;
    }
    /**
     * @return goal as goalController
     */
    public GoalController getGoal() {
        return goal;
    }
    /**
     * @return the inventory
     */
    public List<EntityData> getInventory() {
        return inventory;
    }
    public int getStart_x() {
        return start_x;
    }
    public int getStart_y() {
        return start_y;
    }
}
