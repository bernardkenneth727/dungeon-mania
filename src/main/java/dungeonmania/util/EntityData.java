package dungeonmania.util;

/**
 * This class is used to read in all the information required for the different entities. 
 * It is the middle man from reading JSON 
 */
public class EntityData {
    private int x;
    private int y;
    private String type;
    private String id;
    private int key;
    private String colour;
    private double health;
    private double attackdmg;
    private boolean hasArmour;
    private boolean hasShield;
    private boolean isAlly;
    private String logic;
    private int movement_factor;
    private boolean onSwampTile;
    
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public String getType() {
        return type;
    }
    public String getId() {
        return id;
    }
    public int getKey() {
        return key;
    }
    public String getColour() {
        return colour;
    }
    public double getAttackdmg() {
        return attackdmg;
    }
    public double getHealth() {
        return health;
    }
    public boolean getHasArmour() {
        return hasArmour;
    }
    public boolean getHasShield() {
        return hasShield;
    }
    
    public boolean getIsAlly() {
        return isAlly;
    }
    public String getLogic() {
        return logic;
    }
    public int getMovementFactor() {
        return movement_factor;
    }
    public boolean isOnSwampTile() {
        return onSwampTile;
    }


}