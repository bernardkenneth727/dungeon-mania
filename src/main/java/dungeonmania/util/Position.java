package dungeonmania.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;


import java.lang.Math;
public final class Position {
    private final int x, y, layer;

    public Position(int x, int y, int layer) {
        this.x = x;
        this.y = y;
        this.layer = layer;
    }

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
        this.layer = 0;
    }

    @Override
    public final int hashCode() {
        return Objects.hash(x, y, layer);
    }

    @Override
    public final boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Position other = (Position) obj;

        // z doesn't matter
        return x == other.x && y == other.y;
    }

    public final int getX() {
        return x;
    }

    public final int getY() {
        return y;
    }

    public final int getLayer() {
        return layer;
    }

    public final Position asLayer(int layer) {
        return new Position(x, y, layer);
    }

    public final Position translateBy(int x, int y) {
        return this.translateBy(new Position(x, y));
    }

    public final Position translateBy(Direction direction) {
        return this.translateBy(direction.getOffset());
    }

    public final Position translateBy(Position position) {
        return new Position(this.x + position.x, this.y + position.y, this.layer + position.layer);
    }

    // (Note: doesn't include z)

    /**
     * Calculates the position vector of b relative to a (ie. the direction from a
     * to b)
     * @return The relative position vector
     */
    public static final Position calculatePositionBetween(Position a, Position b) {
        return new Position(b.x - a.x, b.y - a.y);
    }

    public  static final boolean isAdjacent(Position a, Position b) {
        int x = a.x - b.x;
        int y = a.y - b.y;
        x = Math.abs(x);
        y = Math.abs(y);
        return Math.abs(x + y) == 1;
    }

    // (Note: doesn't include z)
    public final Position scale(int factor) {
        return new Position(x * factor, y * factor, layer);
    }

    @Override
    public final String toString() {
        return "Position [x=" + x + ", y=" + y + ", z=" + layer + "]";
    }

    public int distanceBetween(Position a, Position b) {
        int x = Math.abs(a.getX() - b.getX());
        int y = Math.abs(a.getY() - b.getY());
        int distance = x + y;
        return distance;
    }

    // Return Adjacent positions in an array list with the following element positions:
    // 0 1 2
    // 7 p 3
    // 6 5 4
    public List<Position> getAdjacentPositions() {
        List<Position> adjacentPositions = new ArrayList<>();
        adjacentPositions.add(new Position(x-1, y-1));
        adjacentPositions.add(new Position(x  , y-1));
        adjacentPositions.add(new Position(x+1, y-1));
        adjacentPositions.add(new Position(x+1, y));
        adjacentPositions.add(new Position(x+1, y+1));
        adjacentPositions.add(new Position(x  , y+1));
        adjacentPositions.add(new Position(x-1, y+1));
        adjacentPositions.add(new Position(x-1, y));
        return adjacentPositions;
    }

    public List<Position> getCardinalPositions() {
        List<Position> possiblePos = new ArrayList<>();
        possiblePos.add(new Position(x  , y-1));
        possiblePos.add(new Position(x  , y+1));
        possiblePos.add(new Position(x+1, y));
        possiblePos.add(new Position(x-1, y));
        return possiblePos;
    }

    public Position getRandomPosition() {
        List<Position> possiblePos = this.getCardinalPositions();
        Random rand = new Random();
        Position randomPos = possiblePos.get(rand.nextInt(possiblePos.size()));
        return randomPos;
    }

    /**
     * Creates a list of all positions in a specified range of the current Position.
     * @param range (int) - The range to get positions of relative to the current Position.
     * @return List - Returns the newly created list.
     * @pre range >= 0
     */
    public List<Position> getPositionsInRange(int range) {
        if (range < 0) range = - range;
        List<Position> positionsInRange = new ArrayList<>();
        int minusRange = - range;
        for (int x = minusRange; x <= range; x++) {
            for (int y = minusRange; y <= range; y++) {
                positionsInRange.add(new Position(this.x + x, this.y + y));
            }
        }

        // Removes the current position from this list.
        positionsInRange.remove(this);

        //System.out.println(positionsInRange);

        return positionsInRange;
    }

    /**
     * Returns a random Position in a specified range of the current position.
     * @param range (int) - The range relative to the current Position to get the random Position from.
     * @return Position - Returns a Position.
     */
    public Position getRandomPositionInRange(int range) {
        List<Position> possiblePos = getPositionsInRange(range);
        Random rand = new Random();
        Position randomPos = possiblePos.get(rand.nextInt(possiblePos.size()));
        return randomPos;
    }


}
