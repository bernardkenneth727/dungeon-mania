package dungeonmania.entities;

import dungeonmania.util.*;

import java.util.List;

import dungeonmania.entities.enemy.Spider;
import dungeonmania.gameFile.GameFile;

public class Boulder extends MoveableEntity implements Collidable{
    private boolean isOnSwitch; 

    /**
     * constrcutor for boulder
     * @param x
     * @param y
     */
    public Boulder(Position pos, String id) {
        super(pos, "boulder", false, id);
        this.isOnSwitch = false; 
    }

    /**
     * @param position
     * @param direction
     * @param gamefile
     * @return boolean that determines if entity can move to a new spot
     */
    public boolean canMoveTo(Position position, Direction direction, List<Entity> entities, String gameMode) {
        if (swampChecks(entities)) {
            Position newPos; 
            newPos = super.getFuturePos(direction); 
            boolean isPossible = true;

            List<Entity> entityList = GameFile.findEntityFromPos(newPos, entities);
            //for loop for entity list that checks conditions
            for (Entity entity : entityList) {
                if (entity instanceof Collidable) {
                    Collidable obj = (Collidable) entity;
                    isPossible = obj.canMoveOnto(this); 
                }  
            }
            if (isPossible) {
                //move
                move(direction);
                //check if on switch after moving 
                setStatus(entities);
                return true; 
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @return boolean if boulder is on switch
     */
    public boolean isOnSwitch() {
        return this.isOnSwitch; 
    }

    /**
     * sets the status of the boulder
     * @param gamefile
     */
    public void setStatus(List<Entity> entities) {
        List<Entity> entityList2 = GameFile.findEntityFromPos(getPosition(), entities);
        for (Entity entity : entityList2) {
            if (entity.getType().equals("switch")) {
                this.isOnSwitch = true; 
                activateWireFromEndpoint(entities); 
            } else {
                this.isOnSwitch = false; 
                deactivateWireFromEndpoint(entities);
            }


        }
    }

    public boolean canMoveOnto(Entity ent) {
        if (ent instanceof Spider) {
            Spider spider = (Spider) ent;
            spider.spiderReverseDirection();
            return false;
        } else {
            return false; 
        }
    }

    //activate endpoint wire 
    public void activateWireFromEndpoint(List<Entity> entities) {
        for (Entity e : entities) {
            if (Position.isAdjacent(e.getPosition(), getPosition()) && e.getType().equals("wire")) {
                Wire w = (Wire) e; 
                w.setWireOn(entities); 
            }
        }
    }

    public void deactivateWireFromEndpoint(List<Entity> entities) {
        for (Entity e : entities) {
            if (Position.isAdjacent(e.getPosition(), getPosition()) && e.getType().equals("wire")) {
                Wire w = (Wire) e; 
                w.setWireOff(entities); 
            }
        }
    }


}
