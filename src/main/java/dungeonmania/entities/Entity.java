package dungeonmania.entities;

import java.util.List;

import org.json.JSONObject;

import dungeonmania.util.*;

public class Entity extends EntityData{
    private static String idCounter = "0";
    private Position pos;
    private String type;
    private String id;
    private boolean isAlive;
    private boolean isInteractable;


    /**
     * mostly for reading through GSON
     * @param x
     * @param y
     * @param type
     * @param isInteractable
     * @param colour
     * @param key
     */
    public Entity(Position pos, String type, boolean isInteractable, String id) {
        this.pos = pos;
        this.type = type;
        this.isInteractable = isInteractable;
        this.isAlive = true;
        if (id.equals("-100")) {
            idCounter = Integer.toString(Integer.parseInt(idCounter) + 1);
            this.id = idCounter; 
        } else {
            this.id = id;
        } 
    }
    /**
     * resets the counter
     */
    public static void resetIdCounter() {
        Entity.idCounter = "0";
    }

    /**
     * @return type
     */
    public String getType() {
        return type;
    }
    /**
     * sets the type 
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * @return position
     */
    public Position getPosition() {
        return pos;
    }
    /**
     * sets the position 
     * @param position
     */
    public void setPosition(Position position) {
        this.pos = position;
    }
    /** 
     * @return id
     */
    public String getId() {
        return id;
    }
    /**
     * sets the id
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * @return if it is interactable
     */
    public boolean getIsInteractable(){
        return isInteractable;
    }
    /**
     * @return if it is alive
     */
    public boolean isAlive() {
        return isAlive;
    }
    /**
     * sets if entity is alive
     * @param isAlive
     */
    public void setAlive(boolean isAlive) {
        this.isAlive = isAlive;
    }

    /**
     * not used but is here just incase 
     * @param game
     */
    public void tick(List<Entity> entities) {
        // nothing goes here (mostly for override just incase)
    }
    public void tick(List<Entity> entities, String gameMode) {
        // nothing goes here (mostly for override just incase)
    }

    /**
     * @return entity as a JSON Object
     */
    public JSONObject getEntAsJSON() {
        
        JSONObject entJSON = new JSONObject();
        entJSON.put("x", this.pos.getX()); 
        entJSON.put("y", this.pos.getY());
        entJSON.put("type", this.type);
        entJSON.put("id", this.id);
        return entJSON;
    }
}
