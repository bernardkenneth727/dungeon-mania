package dungeonmania.entities;

import dungeonmania.util.Position;

public class Exit extends Entity {

    private boolean isExited; 

    /**
     * constructor for exit
     * @param x
     * @param y
     */
    public Exit(Position pos, String id) {
        super(pos, "exit", false, id);
        isExited = false; 
    }
    /**
     * @return bool for is exited
     */
    public boolean isExited() {
        return isExited; 
    }
    /**
     * sets if the exit has been pressed
     */
    public void pressExit() {
        this.isExited = true; 
        
    }
}
