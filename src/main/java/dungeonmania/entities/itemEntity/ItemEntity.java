package dungeonmania.entities.itemEntity;

import dungeonmania.entities.Entity;
import dungeonmania.util.Position;

public class ItemEntity extends Entity {
    /**
     * constructor for item Entity
     * @param x
     * @param y
     * @param type
     * @param key
     */
    public ItemEntity(Position pos, String type, String id) {
        super(pos, type, false, id);
    }
    public ItemEntity(String type, String id) {
        super(null, type, false, id);
    }
    
}
