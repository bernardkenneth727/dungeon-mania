package dungeonmania.entities.itemEntity.collectableEntity;

import java.util.ArrayList;
import java.util.List;

import dungeonmania.entities.Entity;
import dungeonmania.entities.Wire;
import dungeonmania.entities.itemEntity.ItemEntity;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Position;

public class LightBulb extends ItemEntity implements ElectricItem{
    private boolean isOn;
    private String logic; // AND, OR, XOR, NOT, CO_AND
    private int previouslyActivated;
    
    /**
     * Constructor for lightbulb 
     * @param pos
     * @param id
     * @param isOn
     * @param logic
     */
    public LightBulb(Position pos, String type, String id, String logic) {
        super(pos, type, id);
        this.logic = logic;
        this.previouslyActivated = 0;

        // sets the type if it is on or off
        if (type.equals("light_bulb_on")) {
            this.setType("light_bulb_on");
            this.isOn = true;
            
        }
        else {
            this.setType("light_bulb_off");
            this.isOn = false;
        }
    }
    /**
     * @return if light is on 
     */
    @Override
    public boolean getIsOn() {
        return isOn;
    }
    /**
     * sets if light is on
     * @param isOn
     */
    public void setIsLightOn(boolean isOn) {
        this.isOn = isOn;
    }
    /**
     * @return the logic (AND, OR, XOR, NOT, CO_AND)
     */
    public String getLogic() {
        return logic;
    }
    
    /**
     * sets the type to be on or off
     * @param listEntities
     */
    @Override
    public void tick(List<Entity> listEntities) {
        if (checkLogicTrue(listEntities)) {
            this.isOn = true;
            this.setType("light_bulb_on");
        } 
        else {
            this.isOn = false;
            this.setType("light_bulb_off");
        }
    }
    
    /**
     * @param listEntities
     * @return list of wires that are adjacent to the lightbulb
     */
    public List<ElectricItem> getWiresAdjacent(List<Entity> listEntities) {

        List<ElectricItem> listAdjacent = new ArrayList<ElectricItem>();
        for (Entity ent : listEntities) {
            if (Position.isAdjacent(this.getPosition(), ent.getPosition()) && ent.getType().equals("wire")) {
                listAdjacent.add((ElectricItem) ent);
            }
            else if (this.getPosition().equals(ent.getPosition()) && ent.getType().equals("wire")){
                listAdjacent.add((ElectricItem)ent);
            }
            else if (Position.isAdjacent(this.getPosition(), ent.getPosition()) && ent.getType().equals("switch")) {
                listAdjacent.add((ElectricItem)ent);
            }
            else if (this.getPosition().equals(ent.getPosition()) && ent.getType().equals("switch")){
                listAdjacent.add((ElectricItem)ent);
            }
        }
        return listAdjacent;
    }

    /**
     * @param wiresAdjacent
     * @return number of wires that are active and adjacent
     */
    public int numActiveAdjacentWires(List<ElectricItem> wiresAdjacent) {
        int active = 0;
        for (ElectricItem ent : wiresAdjacent) {
            if (ent.getIsOn()) {
                active++;
            }
        }
        return active;
    }
    /**
     * @param listEntities
     * @return checks if the logic has passed or not 
     */
    public boolean checkLogicTrue(List<Entity> listEntities) {

        boolean logicPassed = false;
        List<ElectricItem> wiresAdjacent = getWiresAdjacent(listEntities);
        int activeAdjacentWires = numActiveAdjacentWires(wiresAdjacent);

        switch (logic) {
            case "AND" : 
                if (activeAdjacentWires == wiresAdjacent.size()) {
                    logicPassed = true;
                }
                break;
            case "OR" : 
                if (activeAdjacentWires >= 1) {
                    logicPassed = true;
                }
                break;
            case "XOR" : 
                if (activeAdjacentWires == 1) {
                    logicPassed = true;
                }
                break;
            case "NOT" : 
                if (activeAdjacentWires == 0) {
                    logicPassed = true;
                }
                break;
            case "CO_AND" :
                // check if the prev Activated Wires is 2 less than the current active adjacent wires
                if (activeAdjacentWires - previouslyActivated >= 2){
                    logicPassed = true;
                }
                else if (this.isOn && activeAdjacentWires >= 2) {
                    logicPassed = true;
                }
                break;
        }
        previouslyActivated = activeAdjacentWires;
        return logicPassed;
    }
    
    @Override
    public void setState(GameFile game) {
        List<Entity> entities = GameFile.getEntities(); 

        for (Entity e : entities) {
            if (Position.isAdjacent(e.getPosition(), getPosition()) && e.getType().equals("wire")) {
                Wire w = (Wire) e; 
                if (w.getIsOn() == true) {
                    this.isOn = true; 
                    break; 
                }
            }
        } 
        this.isOn = false; 
    }

}
