package dungeonmania.entities.itemEntity.collectableEntity.protection;

public interface Protection {
    public double getProtectionLevel();
}
