package dungeonmania.entities.itemEntity.collectableEntity;

import java.util.List;

import dungeonmania.entities.Entity;
import dungeonmania.entities.Inventory;
import dungeonmania.entities.itemEntity.ItemEntity;
import dungeonmania.util.Position;

public abstract class CollectableEntity extends ItemEntity {

    /**
     * constructor for collectable entity
     * @param x
     * @param y
     * @param type
     * @param key
     */
    public CollectableEntity(Position pos, String type, String id) {
        super(pos, type, id);
    }
    /**
     * picks up the collectable item
     * @param gf
     * @param inv
     */
    public void pickupCollectable(List<Entity> entities, Inventory inv) {
        inv.addItem(this);
        this.setPosition(null);
        entities.remove(this);

    }
}
