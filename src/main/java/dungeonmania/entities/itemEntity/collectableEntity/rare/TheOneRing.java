package dungeonmania.entities.itemEntity.collectableEntity.rare;

import dungeonmania.entities.itemEntity.collectableEntity.CollectableEntity;
import dungeonmania.util.Position;

public class TheOneRing extends CollectableEntity {

    /**
     * Constructor for TheOneRing.
     * @param pos (Position) - Where the item is located (can be null if not on the map).
     * @param id (String) - The id of the Entity.
     */
    public TheOneRing(Position pos, String id) {
        super(pos, "one_ring", id);
    }
    
}
