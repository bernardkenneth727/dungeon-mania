package dungeonmania.entities.itemEntity.collectableEntity.rare;

import dungeonmania.entities.itemEntity.collectableEntity.CollectableEntity;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Sword;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Weapon;
import dungeonmania.util.Position;

public class Anduril extends CollectableEntity implements Weapon{
    
    private Weapon weapon; 

    /**
     * Constrcuctor for loading anduril 
     */
    public Anduril(Position pos, String id) {
        super(pos, "anduril", id);
        Sword sword = new Sword(null, "-100");
        this.weapon = sword; 
    }

    /**
     * Constructor for anduril 
     * @param pos
     * @param weapon
     */
    public Anduril(Position pos, Weapon weapon) {
        super(pos, "anduril", "-100");
        this.weapon = weapon;
    }

    /**
     * Weapon damage is a wrapper based on sword damage (3 times sword dmg)
     */
    @Override
    public double getWeaponDamage() {
        return weapon.getWeaponDamage() * 3; 
    }
    
}
