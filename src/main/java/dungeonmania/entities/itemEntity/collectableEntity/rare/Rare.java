package dungeonmania.entities.itemEntity.collectableEntity.rare;

import java.util.Random;

import dungeonmania.entities.Inventory;
import dungeonmania.entities.itemEntity.collectableEntity.CollectableEntity;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Sword;

public class Rare {
    /**
     * Returns a CollectableEntity with a new TheOneRing.
     * @return RareCollectableEntity
     */
    public static CollectableEntity getTheOneRing() {
        return new TheOneRing(null, "-100");
    }

    /**
     * Returns a CollectableEntity with a new Anduril.
     * @return RareCollectableEntity
     */
    public static CollectableEntity getAnduril() {
        
        Sword sword = new Sword(null, "-100");
        return new Anduril(null, sword);
    }

    /**
     * Determines whether the player has won any RareCollectableItems, without a seed.
     * @param inv (Inventory) - The player's inventory.
     * @return RareCollectableEntity - returns a RareCollectableEntity if the player wins one, and otherwise returns null.
     */
    public static CollectableEntity battleWinnings(Inventory inv) {
        return battleWinnings(inv, System.nanoTime());
    }

    /**
     * Determines whether the player has won any RareCollectableItems, with a seed.
     * @param inv (Inventory) - The player's inventory.
     * @param seed (long) - A seed to determine if the player will win.
     * @return RareCollectableEntity - returns a RareCollectableEntity if the player wins one, and otherwise returns null.
     */
    public static CollectableEntity battleWinnings(Inventory inv, long seed) {
        // Creates a new Random number generator using a seed and generates one between 0 inclusive and 100 exclusive.
        Random rand = new Random(seed);
        int randomInt = rand.nextInt(100);
        
        // If that number is less than 10, then adds TheOneRing to the player's inventory and returns it.
        if (randomInt < 10) {
            CollectableEntity myOneRing = getTheOneRing();
            inv.addItem(myOneRing);
            return myOneRing;
        } 
        // If that number is less than 15, then adds an Anduril to the player's inventory and returns it.
        else if (randomInt < 15) {
            CollectableEntity myAnduril = getAnduril();
            inv.addItem(myAnduril);
            return myAnduril;
        }
        
        // Otherwise, returns null.
        return null;
    }
}
