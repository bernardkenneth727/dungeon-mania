package dungeonmania.entities.itemEntity.collectableEntity;

import dungeonmania.util.Position;

public class Arrow extends CollectableEntity {
    /**
     * constructor for arrow
     * @param x
     * @param y
     */
    public Arrow(Position pos, String id) {
        super(pos, "arrow", id);
    }
    
}
