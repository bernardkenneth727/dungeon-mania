package dungeonmania.entities.itemEntity.collectableEntity;

import java.util.List;

import org.json.JSONObject;

import dungeonmania.entities.Entity;
import dungeonmania.entities.Inventory;
import dungeonmania.util.Position;

public class Key extends CollectableEntity{
    private int key;

    /**
     * constructor for key
     * @param x
     * @param y
     * @param key
     */
    public Key(Position pos, int key, String id) {
        super(pos, "key", id);
        this.key = key;
    }

    /**
     * @return key
     */
    public int getKey() {
        return key;
    }

    /**
     * @return key as a JSON Object
     */
    @Override
    public JSONObject getEntAsJSON() {
        
        JSONObject entJSON = new JSONObject();
        entJSON.put("x", this.getX()); 
        entJSON.put("y", this.getY());
        entJSON.put("type", "key");
        entJSON.put("key", this.key);
        entJSON.put("id", this.getId());
        
        return entJSON;
    }

    /**
     * Picks up a Key
     * @param entities (List) - List of entities on the dungeon floor, where the key is.
     * @param inv (Inventory) - The player's inventory.
     */
    @Override
    public void pickupCollectable(List<Entity> entities, Inventory inv) {
        // A key can only be picked up if the player does not already have one, so if the player has 0 keys, then they can pickup a key.
        if (inv.numberOf("key") == 0) {
            super.pickupCollectable(entities, inv);
        }
        
    }
}
