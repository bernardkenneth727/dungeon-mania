package dungeonmania.entities.itemEntity.collectableEntity.potion;

import dungeonmania.entities.Player;
import dungeonmania.entities.itemEntity.collectableEntity.CollectableEntity;
import dungeonmania.util.Position;

public abstract class Potion extends CollectableEntity {
    /**
     * constructor for potion
     * @param x
     * @param y
     * @param type
     */
    public Potion(Position pos, String type, String id) {
        super(pos, type, id); 
    }
    /**
     * decreases potion life
     * @param player
     */
    public abstract void decreasePotionLife(Player player);
    /**
     * @return if potion is active
     */
    public abstract boolean isActive();
    /**
     * activates the potion
     * @param player
     */
    public abstract void activate(Player player);
    /**
     * @return type of potion used
     */
    public abstract String getPower(); 
}

