package dungeonmania.entities.itemEntity.collectableEntity.potion;

import dungeonmania.entities.Player;
import dungeonmania.util.Position;

public class InvisibilityPotion extends Potion{

    private String power = "invisible";
    private int potionLife = 10; 
    private boolean active; 

    /**
     * constructor for invisibilility potion
     * @param x
     * @param y
     */
    public InvisibilityPotion(Position pos, String id) {
        super(pos, "invisibility_potion", id);
        this.active = false; 
    }   
    /**
     * activates the potion 
     * @param player
     */
    public void activate(Player player) {
        this.active = true; 
    }
    /**
     * @return if active
     */
    public boolean isActive() {
        return active; 
    }
    /**
     * @return power of potion
     */
    public String getPower() {
        return power; 
    }
    /**
     * decreases potion life
     * @param player
     */
    public void decreasePotionLife(Player player) {
        potionLife--; 
        
        if (potionLife == 0) {
            this.active = false; 
        }
    }
}

