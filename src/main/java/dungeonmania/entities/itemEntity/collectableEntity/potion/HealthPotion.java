package dungeonmania.entities.itemEntity.collectableEntity.potion;

import dungeonmania.entities.Player;
import dungeonmania.util.Position;

public class HealthPotion extends Potion{
    private String power = "heal";
    private int potionLife = 1; 
    private boolean active; 

    /**
     * constructor for health potion
     * @param x
     * @param y
     */
    public HealthPotion(Position pos, String id) {
        super(pos, "health_potion", id); 
        this.active = false;
        
    }
    /**
     * @return if potion is active
     */
    public boolean isActive() {
        return active; 
    }
    /**
     * activates the potion and regenerates player health
     */
    public void activate(Player player) {
        this.active = true; 
        player.regenerateHealth();; 
    }
    /**
     * @return the power type
     */
    public String getPower() {
        return power; 
    }
    /**
     * decreases the potion life in player inventory
     */
    public void decreasePotionLife(Player player) {
        potionLife--; 
        
        if (potionLife == 0) {
            this.active = false; 
        }
    }


    
}

