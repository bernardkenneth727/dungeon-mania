package dungeonmania.entities.itemEntity.collectableEntity.potion;

import dungeonmania.entities.Player;
import dungeonmania.util.Position;

public class InvincibilityPotion extends Potion{

    private String power = "invincible";
    private int potionLife = 10; 
    private boolean active; 

    /**
     * constructor for invincibility potion
     * @param x
     * @param y
     */
    public InvincibilityPotion(Position pos, String id) {
        super(pos, "invincibility_potion", id);
        this.active = false;
       
    }
    /**
     * activates the potion 
     */
    public void activate(Player player) {
        this.active = true; 
    }
    /**
     * @return if the potion is active
     */
    public boolean isActive() {
        return active; 
    }
    /**
     * @return power of potion
     */
    public String getPower() {
        return power; 
    }
    /**
     * decreases potion life
     */
    public void decreasePotionLife(Player player) {
        potionLife--; 
        
        if (potionLife == 0) {
            this.active = false; 
        }
    }

    
}

