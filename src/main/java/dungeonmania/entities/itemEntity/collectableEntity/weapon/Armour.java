package dungeonmania.entities.itemEntity.collectableEntity.weapon;

import dungeonmania.entities.Inventory;
import dungeonmania.entities.itemEntity.collectableEntity.CollectableEntity;
import dungeonmania.entities.itemEntity.collectableEntity.protection.Protection;
import dungeonmania.util.Position;

public class Armour extends CollectableEntity implements Durable, Protection {
    private double durability = 15;
    private double protectionLevel = 1;
    /**
     * constructor for armour
     * @param x
     * @param y
     */
    public Armour(Position pos, String id) {
        super(pos, "armour", id);
    }

    @Override
    public double getDurability() {
        return this.durability;
    }

    @Override
    public void decrementDurability() {
        this.durability--;
    }

    @Override
    public void deteriorateWeapon(Inventory inv) {
        this.decrementDurability();
        if (this.durability <= 0) {
            inv.useItem(this);
        }
    }

    @Override
    public double getProtectionLevel() {
        return this.protectionLevel;
    }
}
