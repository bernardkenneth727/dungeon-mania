package dungeonmania.entities.itemEntity.collectableEntity.weapon;

public interface Weapon {
    /**
     * Interface used in the weapon decorator pattern
     * @return weapon damage
     */
    public double getWeaponDamage();
    
}
