package dungeonmania.entities.itemEntity.collectableEntity.weapon;

import java.util.ArrayList;
import java.util.List;

import dungeonmania.entities.Entity;
import dungeonmania.entities.Inventory;
import dungeonmania.entities.itemEntity.collectableEntity.CollectableEntity;
import dungeonmania.entities.itemEntity.collectableEntity.ElectricItem;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Position;

public class Bomb extends CollectableEntity implements ElectricItem {
    private boolean isPlaced = false;
    private boolean isOn = false;

    /**
     * constructor for bomb
     * @param x
     * @param y
     */
    public Bomb(Position pos, String id) {
        super(pos, "bomb", id);
    }

    /**
     * If a bomb is detonated, then all adjacent entities, including the bomb, excluding the player are removed.
     * @param game (GameFile) - The game's file containing all entities in the game.
     */
    public List<Entity> detonate(GameFile game) {

        List<Entity> explodedEntities = new ArrayList<>();
        if (checkTriggered(game)) {
            // Gets all the entities in the current game.
            List<Entity> gameEntities = GameFile.getEntities();
                    
            // For each entity, if it is adjacent to the bomb, and not a player, then adds to exploded list.
            gameEntities.forEach(e -> {
                if (getPosition().getAdjacentPositions().contains(e.getPosition()) && !e.getType().equals("player")) {
                    // Then remove the entity in range by adding it to an exploded list, to be removed after looping through the all lists..
                    explodedEntities.add(e);
                }
            });
            // Adds the bomb to the exploded list and removes all exploded entities.
            explodedEntities.add(this);
        }
        return explodedEntities; 
    }

    /**
     * Places the bomb down on the game's overworld - can explode when triggered by boulder + floor switch.
     * @param game (GameFile) - The current game in progress.
     * @param inv (Inventory) - The inventory of the player.
     */
    public void placeDown(List<Entity> entities, Inventory inv) {
        // Finds the player to get their position.
        Entity player = GameFile.getPlayer(entities);
        
        // Places the bomb down and removes it from the player's inventory.
        this.setPosition(player.getPosition());
        inv.useItem(this);
        this.isPlaced = true;
        entities.add(this);
    }

    /**
     * Checks if the bomb is placed next to a switch with a boulder on it.
     * @param game (GameFile) - The current game in progress.
     * @return boolean - Returns whether or not, the bomb placed down has been triggered/
     */
    public boolean checkTriggered(GameFile game) {
        List<Entity> entities = GameFile.getEntities();

        // Checks if each entity is a switch placed cardinally adjacent to the bomb and if so, then checks if a boulder is at the same position of the switch.
        for (Entity e : entities) {
            if (Position.isAdjacent(e.getPosition(), this.getPosition()) && e.getType().equals("switch")) {
                if (entities.stream().anyMatch(f -> f.getPosition().equals(e.getPosition()) && f.getType().equals("boulder"))) {
                    // Returns true if that condition is met and if the bomb has been placed there by the player.
                    return this.isPlaced;
                }
            }
            
        }

        return false;
    }


    @Override
    public boolean getIsOn() {
        return isOn;
    }

    @Override
    public void setState(GameFile game) {
        List<Entity> entities = GameFile.getEntities(); 
        for (Entity ent : entities) {
            if (checkCanChangeIfOn(ent)) {
                this.isPlaced= true; 
                this.isOn= true; 
                detonate(game);
            }
        }
    }
    
    private boolean checkCanChangeIfOn(Entity entity) {

        if (entity.getType().equals("wire") || entity.getType().equals("switch")) {
            ElectricItem elecItem = (ElectricItem) entity;
            if (elecItem.getIsOn()) {
                if (Position.isAdjacent(entity.getPosition(), getPosition()) || entity.getPosition().equals(this.getPosition())) {
                    return true;
                }
            }
        }
        return false;
    }
}
