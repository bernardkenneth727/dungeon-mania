package dungeonmania.entities.itemEntity.collectableEntity.weapon;

import dungeonmania.entities.Inventory;
import dungeonmania.entities.itemEntity.collectableEntity.CollectableEntity;
import dungeonmania.util.Position;

public class Sword extends CollectableEntity implements Durable, Weapon {
    private double durability = 10;
    private double weaponDamage = 1.8;
    /**
     * constructor for sword
     * @param x
     * @param y
     */
    public Sword(Position pos, String id) {
        super(pos, "sword", id);
    }

    @Override
    public double getDurability() {
        return this.durability;
    }

    @Override
    public void decrementDurability() {
        this.durability--;
    }

    @Override
    public void deteriorateWeapon(Inventory inv) {
        this.decrementDurability();
        if (this.durability <= 0) {
            inv.useItem(this);
        }
    }

    /**
     * Weapon damage for sword
     */
    @Override
    public double getWeaponDamage() {
        return this.weaponDamage;
    }

}
