package dungeonmania.entities.itemEntity.collectableEntity.weapon;

import dungeonmania.entities.Inventory;

public interface Durable {
    /**
     * @return durabuility of weapon
     */
    public double getDurability();
    /** 
     * decrements durability
    */
    public void decrementDurability();
    /**
     * deteriorates the weapon
     * @param inv
     */
    public void deteriorateWeapon(Inventory inv);
    
}
