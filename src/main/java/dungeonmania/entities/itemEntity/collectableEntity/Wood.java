package dungeonmania.entities.itemEntity.collectableEntity;

import dungeonmania.util.Position;

public class Wood extends CollectableEntity {
    /**
     * constructor for wood
     * @param x
     * @param y
     */
    public Wood(Position pos, String id) {
        super(pos, "wood", id);
    }
}
