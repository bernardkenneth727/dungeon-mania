package dungeonmania.entities.itemEntity.collectableEntity;

import dungeonmania.util.Position;

public class Treasure extends CollectableEntity {

    /**
     * constrcutor for treasure
     * @param position
     */
    public Treasure(Position pos, String type, String id) {
        super(pos, type, id);
    }
    
}
