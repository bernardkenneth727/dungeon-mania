package dungeonmania.entities.itemEntity.collectableEntity;

import dungeonmania.util.Position;

public class SunStone extends Treasure {

    public SunStone(Position pos, String id) {
        super(pos, "sun_stone", id);
    }
    
}
