package dungeonmania.entities.itemEntity.collectableEntity;

import dungeonmania.gameFile.GameFile;

public interface ElectricItem {

    public boolean getIsOn();

    public void setState(GameFile game); 
    
}
