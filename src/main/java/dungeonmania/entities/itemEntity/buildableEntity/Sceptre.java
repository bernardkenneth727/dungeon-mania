package dungeonmania.entities.itemEntity.buildableEntity;

import java.util.List;

import dungeonmania.entities.Entity;
import dungeonmania.entities.Inventory;
import dungeonmania.entities.itemEntity.ItemEntity;

public class Sceptre extends ItemEntity implements Buildable {
    /**
     * Constructor for Sceptre.
     * @param id (String) - Takes in the id of the Sceptre.
     */
    public Sceptre(String id) {
        super("sceptre", id);
    }

    /**
     * Builds the sceptre for the player and places it in the player's inventory.
     * @param inv (Inventory) - The player's inventory to add the sceptre to after building with a sun stone, either wood or arrows, and either treasure or a key.
     */
    @Override
    public void buildEntity(Inventory inv) {
        // Uses the items requires and in scenarios where there are options, removes one or the other (using the useItem method from Inventory).
        inv.useItem("sun_stone");
        
        if (inv.numberOf("wood") >= 1) {
            inv.useItem("wood");
        } else {
            inv.useItem("arrow");
            inv.useItem("arrow");
        }

        // Building a sceptre will prioritise using a sun stone, then treasure, then a key.
        if (inv.numberOf("sun_stone") >= 1) {
            inv.useItem("sun_stone");
        } else if (inv.numberOf("treasure") >= 1) {
            inv.useItem("treasure");
        } else {
            inv.useItem("key");
        }
        inv.addItem(this);
        
    }

    /**
     * Determines whether or not the player can build a sceptre. The player must have at least 1 wood/2 arrows, 1 key/treasure, and a sun stone
     * @param inv (Inventory) - The player's inventory to check whether they can have the items to build a sceptre.
     * @param entities (List) - A list of the entities in the dungeon.
     * @return boolean - Returns true if the player can build the sceptre.
     */
    @Override
    public boolean canBuild(Inventory inv, List<Entity> entities) {
        // Boolean statements for the having either wood or 2 arrows, then key or treasure (or 2 sun stones since treasure is interchangeable with a sun stone) and having a sun stone.
        boolean hasWoodOrArrows = inv.numberOf("wood") >= 1 || inv.numberOf("arrow") >= 2;
        boolean hasKeyOrTreasure = inv.numberOf("key") >= 1 || inv.numberOf("treasure") >= 1 || inv.numberOf("sun_stone") >= 2;
        boolean hasSunStone = inv.numberOf("sun_stone") >= 1;
        if (hasWoodOrArrows && hasKeyOrTreasure && hasSunStone) {
            return true;
        }
        return false;
    }
    
}
