package dungeonmania.entities.itemEntity.buildableEntity;

import java.util.List;

import dungeonmania.entities.Entity;
import dungeonmania.entities.Inventory;
import dungeonmania.entities.itemEntity.ItemEntity;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Durable;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Sword;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Weapon;

public class Bow extends ItemEntity implements Durable, Buildable, Weapon {
    private double durability = 15;
    private Weapon weapon; 

    /**
     * constructor for bow
     */
    public Bow(Weapon weapon) {
        super("bow", "-100");
        this.weapon = weapon; 
    }

    /**
     * constructor for loading bow
     */
    public Bow(String id) {
        super("bow", id);
        Sword sword = new Sword(null, "-100");
        this.weapon = sword;  
    }
    
    /**
     * Builds the bow for the player and places it in the player's inventory.
     * @param inv (Inventory) - The player's inventory to add the new bow to after building with wood and arrows.
     */
    @Override
    public void buildEntity(Inventory inv) {
        // Since this should only be called if the player can build the bow, it 
        // builds the bow by removing 3 arrows and 1 wood from the player's inventory and then adding a bow.
        inv.useItem("wood");
        for (int i = 0; i < 3; i++) {
            inv.useItem("arrow");
        }
        inv.addItem(this);
    }
    /**
     * Determines whether or not the player can build a bow. The player must have at least 1 wood and 3 arrows.
     * @param inv (Inventory) - The player's inventory to add the new bow to after building with wood and arrows.
     */
    @Override
    public boolean canBuild(Inventory inv, List<Entity> entities) {
        if (inv.numberOf("wood") >= 1 && inv.numberOf("arrow") >= 3) {
            return true;
        }
        return false;
    }
    /**
     * @return durability
     */
    public double getDurability() {
        return this.durability;
    }
    /**
     * decrements the durability
     */
    public void decrementDurability() {
        this.durability--;
    }
    /**
     * deteriorates the weapon 
     * @param inv
     */
    public void deteriorateWeapon(Inventory inv) {
        this.decrementDurability();
        if (this.durability <= 0) {
            inv.useItem(this);
        }
    }

    /**
     * Weapon damage is a wrapper based on sword damage (2 times sword dmg)
     */
    @Override
    public double getWeaponDamage() {
        return weapon.getWeaponDamage()*2; 
    }  
}
