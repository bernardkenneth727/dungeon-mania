package dungeonmania.entities.itemEntity.buildableEntity;

import java.util.List;

import dungeonmania.entities.Entity;
import dungeonmania.entities.Inventory;

public interface Buildable {
    /**
     * builds entity
     * @param inv
     */
    public void buildEntity(Inventory inv);
    /**
     * @param inv
     * @return if entyity can be built
     */
    public boolean canBuild(Inventory inv, List<Entity> entities);
}
