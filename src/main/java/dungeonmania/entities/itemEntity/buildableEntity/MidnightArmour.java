package dungeonmania.entities.itemEntity.buildableEntity;

import java.util.List;

import dungeonmania.entities.Entity;
import dungeonmania.entities.Inventory;
import dungeonmania.entities.itemEntity.ItemEntity;
import dungeonmania.entities.itemEntity.collectableEntity.protection.Protection;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Sword;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Weapon;

public class MidnightArmour extends ItemEntity implements Buildable, Weapon, Protection {
    private double protectionLevel = 1.5;
    private Weapon weapon;

    /**
     * Constructor for loading Midnight Armour.
     */
    public MidnightArmour(String id) {
        super("midnight_armour", id);
        Sword sword = new Sword(null, "-100");
        this.weapon = sword;
    }

    /**
     * Constructor for Midnight Armour.
     */
    public MidnightArmour(Weapon weapon) {
        super("midnight_armour", "-100");
        this.weapon = weapon; 
    } 

    /**
     * Gets the midnight armour's level of protection.
     * @return double - Returns the protection's level as a double.
     */
    @Override
    public double getProtectionLevel() {
        return this.protectionLevel;
    }

    /**
     * Builds the midnight armour for the player and places it in the player's inventory.
     * @param inv (Inventory) - The player's inventory to add the new bow to after building with wood and arrows.
     */
    @Override
    public void buildEntity(Inventory inv) {
        // Since this should only be called if the player can build the midnight armour, it 
        // builds the midnight armour by removing a sun stone and an armour from the player's 
        // inventory and then adding a bow.
        inv.useItem("sun_stone");
        inv.useItem("armour");
        inv.addItem(this);
    }

    /**
     * Determines whether or not the player can build a midnight armour. The player must have at least 1 sun stone and 1 armour and there must be no zombies in the dungeon.
     * @param inv (Inventory) - The player's inventory to check whether or not the player has the resources to build a midnight armour.
     * @return boolean - Returns true if the player can build a midnight armour.
     */
    @Override
    public boolean canBuild(Inventory inv, List<Entity> entities) {
        // Checks to ensure there are no zombies in the map.
        boolean zombiesInDungeon = entities.stream()
            .anyMatch(e -> e.getType().equals("zombie_toast"));
        
        // If the player has at least 1 armour, 1 sun stone and there are no zombies in the dungeon, 
        // then the player can build a midnight armour.
        if (inv.numberOf("armour") >= 1 && inv.numberOf("sun_stone") >= 1 && !zombiesInDungeon) {
            return true;
        }
        return false;
    }

    /**
     * Weapon damage is a wrapper based on sword damage (0.5 times sword dmg)
     */
    @Override
    public double getWeaponDamage() {
        return weapon.getWeaponDamage()*0.5; 
    }
    
}
