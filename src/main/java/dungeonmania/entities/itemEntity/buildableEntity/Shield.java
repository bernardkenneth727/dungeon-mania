package dungeonmania.entities.itemEntity.buildableEntity;

import java.util.List;

import dungeonmania.entities.Entity;
import dungeonmania.entities.Inventory;
import dungeonmania.entities.itemEntity.ItemEntity;
import dungeonmania.entities.itemEntity.collectableEntity.protection.Protection;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Durable;

public class Shield extends ItemEntity implements Durable, Buildable, Protection {
    private double durability = 20;
    private double protectionLevel = 2;
    /**
     * constructs shield
     */
    public Shield(String id) {
        super("shield", id);
    }

    /**
     * Builds the shield for the player and places it in the player's inventory.
     * @param inv (Inventory) - The player's inventory to add the new shield to after building with wood and either treasure (or sun stone) or key.
     */
    @Override
    public void buildEntity(Inventory inv) {
        // Uses the wood and either treasure or key to make the shield.
        inv.useItem("wood");
        inv.useItem("wood");
        inv.addItem(this);
        // If the player has treasure, then will craft shield using that instead of key since it is prioritised. 
        // A sun stone is prioritised over treasure and will be used before a treasure to build the shield.
        if (inv.numberOf("sun_stone") >= 1) {
            inv.useItem("sun_stone");
        } else if (inv.numberOf("treasure") >= 1) {
            inv.useItem("treasure");
        }
        // If not, then craft using key.
        else {
            inv.useItem("key");
        }
    }

    
    /**
     * Determines whether or not the player can build a shield. The player must have at least 2 wood and 1 treasure (or sun stone)/key.
     * @param inv (Inventory) - The player's inventory to add the new bow to after building with wood and treasure/key.
     */
    @Override
    public boolean canBuild(Inventory inv, List<Entity> entities) {
        boolean hasTreasureOrKey = inv.numberOf("treasure") >= 1 || inv.numberOf("key") >= 1 || inv.numberOf("sun_stone") >= 1;
        if (hasTreasureOrKey && inv.numberOf("wood") >= 2) {
            return true;
        }
        return false;
    }
    /**
     * @return durability
     */
    public double getDurability() {
        return this.durability;
    }
    /**
     * decremets the durability
     */
    public void decrementDurability() {
        this.durability--;
    }
    /**
     * deteriorates the weapon 
     * @param inv
     */
    public void deteriorateWeapon(Inventory inv) {
        this.decrementDurability();
        if (this.durability <= 0) {
            inv.useItem(this);
        }
    }

    @Override
    public double getProtectionLevel() {
        return this.protectionLevel;
    }

    

    
}
