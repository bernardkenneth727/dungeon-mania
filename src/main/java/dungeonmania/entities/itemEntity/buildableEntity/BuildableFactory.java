package dungeonmania.entities.itemEntity.buildableEntity;

import java.util.HashMap;
import java.util.Map;

import dungeonmania.entities.itemEntity.collectableEntity.weapon.Sword;

public class BuildableFactory {
    /**
     * Gives back a new Bow.
     * @return Buildable - Returns the new Bow.
     */
    static public Buildable getBow() {
        Sword sword = new Sword(null, "-100");
        return new Bow(sword);
    }

    /**
     * Gives back a new Shield.
     * @return Buildable - Returns the new Shield.
     */
    static public Buildable getShield() {
        return new Shield("-100");
    }

    /**
     * Gives back a new Midnight Armour.
     * @return Buildable - Returns the new Midnight Armour.
     */
    static public Buildable getMidnightArmour() {
        Sword sword = new Sword(null, "-100");
        return new MidnightArmour(sword);
    }

    /**
     * Gives back a new Sceptre.
     * @return Buildable - Returns the new Sceptre.
     */
    static public Buildable getSceptre() {
        return new Sceptre("-100");
    }

    /**
     * Gives back all the Buildables as a Map with their type name and new Buildable corresponding to that name.
     * @return Map - Returns a Map with key of String (type name) and value of Buildable.
     */
    static public Map<String, Buildable> getAllBuildablesMap() {
        Map<String, Buildable> possibleBuildablesMapped = new HashMap<>();
        possibleBuildablesMapped.put("bow", getBow());
        possibleBuildablesMapped.put("shield", getShield());
        possibleBuildablesMapped.put("midnight_armour", getMidnightArmour());
        possibleBuildablesMapped.put("sceptre", getSceptre());

        return possibleBuildablesMapped;
    }
}
