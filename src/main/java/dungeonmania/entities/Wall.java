package dungeonmania.entities;

import dungeonmania.entities.enemy.Spider;
import dungeonmania.util.Position;

public class Wall extends Entity implements Collidable{

    /**
     * constructor for wall
     * @param x
     * @param y
     */
    public Wall(Position pos, String id) {
        super(pos, "wall", false, id);
    } 

    public boolean canMoveOnto(Entity ent) {
        if (ent instanceof Spider) {
            return true;
        } else {
            return false;
        }
    }
}
