package dungeonmania.entities.enemy;

import java.util.List;

import org.json.JSONObject;

import dungeonmania.entities.Entity;
import dungeonmania.entities.Inventory;
import dungeonmania.entities.Player;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Armour;
import dungeonmania.exceptions.InvalidActionException;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Position;


/**
 * Super powerful Mercenaries 
 * less than 30% chance that assassins will spawn instead of a mercenary
 * Movements are the same as mercenary 
 * Can only be bribed with The One Ring PLUS 1 treasure
 */
public class Assassin extends Mercenary {

    /**
     * Constructor for assassin
     * @param pos
     * @param id
     * @param health
     */
    public Assassin(Position pos, String id, long health) {
        super(pos, "assassin", id, health);

        if (health <= 0) {
            this.setHealth(8);
        }
        this.setAttackdmg(6);
        if (super.isHasArmour()) {
            setArmour(new Armour(pos, "-100"));
        }
    }
    /**
     * Constructor for Assassin
     * @param pos
     * @param id
     * @param health
     * @param hasArmour
     */
    public Assassin(Position pos, String id, double health, boolean hasArmour) {
        super(pos, "assassin", id, health, hasArmour);

        if (health <= 0) {
            this.setHealth(8);
        }
        this.setAttackdmg(6);
    }

    @Override
    public void interact (Inventory inventory, List<Entity> entities) throws InvalidActionException {
        List<Position> cardinals = this.getPosition().getPositionsInRange(2);
        // If the player has a sceptre, then they can mind control the assassin from anywhere on the dungeon.
        if (inventory.numberOf("sceptre") >= 1) {
            super.setIsally(true);
            Player player = (Player) GameFile.getPlayer(entities);
            player.addAllies(this.getId());
            setMindControlTick(0);
        }
        else if (inventory.numberOf("treasure") >= 1 && cardinals.contains(GameFile.getPlayer(entities).getPosition())) {
            if (inventory.numberOf("one_ring") >= 1) {
                super.setIsally(true);
                inventory.useItem("treasure");
                inventory.useItem("one_ring");
                Player player = (Player) GameFile.getPlayer(entities);
                player.addAllies(this.getId());
                setMindControlTick(-1);
            }
        } else {
            throw new InvalidActionException("You are not next to the assassin/do not have one ring or treasure");
        }
    } 

    @Override
    public JSONObject getEntAsJSON() {
        
        JSONObject entJSON = new JSONObject();
        entJSON.put("x", this.getPosition().getX()); 
        entJSON.put("y", this.getPosition().getY());
        entJSON.put("type", this.getType());
        entJSON.put("health", this.getHealth());
        entJSON.put("attackdmg", (int) this.getAttackdmg());
        entJSON.put("hasArmour", this.getHasArmour());
        if (this.getHasArmour()) {
            entJSON.put("armour", this.getArmour().getId());
        }
        entJSON.put("id", this.getId());
        
        return entJSON;
    }
}
