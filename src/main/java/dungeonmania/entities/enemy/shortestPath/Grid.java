package dungeonmania.entities.enemy.shortestPath;
import java.util.List;
import java.util.ArrayList;
import dungeonmania.entities.Entity;
import dungeonmania.util.Position;


public class Grid {

    public ArrayList<Position> grid;

    
    public Grid() {
        grid = new ArrayList<Position>();
        initiate();
    }


    public ArrayList<Position> getGrid() {
        return grid;
    }

    /**
     * Initiates all the positions of the map to a position between -3 and 50. 
     * This represents a 53 x 53 dungeon map
     */
    public void initiate() {
        for (int i = -3; i <= 50; i++) {
            for (int j = -3; j <= 50; j++) {
                grid.add(new Position(i, j));
                
            }
        }

    }

}
