package dungeonmania.entities.enemy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import dungeonmania.entities.Collidable;
import dungeonmania.entities.Entity;
import dungeonmania.entities.Player;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Armour;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Direction;
import dungeonmania.util.Position;

public class Zombie extends Enemy{
    
    /**
     * constructor for zombie
     * @param pos
     */
    public Zombie(Position pos, String id, double health, boolean hasArmour) {
        super(pos, "zombie_toast", false, health, hasArmour, id);
        if (health <= 0) {
            this.setHealth(5);
        }
        this.setAttackdmg(4);
    }
    /**
     * constructor for zombie
     * @param pos
     */
    public Zombie(Position pos, String id, double health) {
        super(pos, "zombie_toast", false, health, id);
        if (health <= 0) {
            this.setHealth(5);
        }
        this.setAttackdmg(4);

        super.setHasArmour(getRandomBoolean());
        if (super.isHasArmour()) {
            setArmour(new Armour(pos, "-100"));
        }
    }

    /** 
     * ticks for zombie 
     * @param game
    */
    public void tick(List<Entity> entities) {

        if (swampChecks(entities)) {
            
            List <Position> visited = new ArrayList<Position>();

            while (visited.size() < 4) {
                Position futurePos = getPosition().getRandomPosition();
                if (!visited.contains(futurePos)) {
                    visited.add(futurePos);
                } 
                if (canMoveTo(futurePos, null, entities, "")) {
                    move(futurePos);
                    break;
                }
            }
        }
    }


    /**
     * This function checks if the zombie can move to the area
     * @param pos
     * @param direction 
     * @param game
     */
    public boolean canMoveTo(Position pos, Direction direction, List<Entity> entities, String gameMode) {
        List<Entity> ents = GameFile.findEntityFromPos(pos, entities);
        boolean isPossible = true;
        // If list is empty then it means the space is empty.
        if (ents.isEmpty()) {
            isPossible = true;
            return isPossible;
        }
        // Now we loop through the list to see what entities are in that position
        for (Entity ent : ents) {
            if (ent instanceof Collidable) {
                Collidable obj = (Collidable) ent;
                return obj.canMoveOnto(this); 
            } else if (ent instanceof Player) {
                Player playerEnt = (Player) ent;
                 isPossible = true; 
                //if player is invisible and/or invincible
                if (playerEnt.isInvisible()) {
                    isPossible = false; 
                } else if (playerEnt.isInvincible()) {
                    isPossible = false; 
                } else {
                    isPossible = true; 
                }
            } else if (ent instanceof Enemy) {
                isPossible = false;
            }
        }
        return isPossible;
    }
    /**
     * @return a random boolean
     */
    public boolean getRandomBoolean() {
        Random random = new Random();
        return random.nextBoolean();
    }


}
