package dungeonmania.entities.enemy;

import java.util.List;

import dungeonmania.entities.Collidable;
import dungeonmania.entities.Entity;
import dungeonmania.entities.Player;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Direction;
import dungeonmania.util.Position;

public class Hydra extends Enemy{

    public Hydra(Position pos, String id, double health) {
        super(pos, "hydra", false, health, id);

        if (health <= 0) {
            this.setHealth(10);
        }
        this.setAttackdmg(5);
    }

    public Hydra(Position pos, String id, double health, boolean hasArmour) {
        super(pos, "hydra", false, health, id);
        if (health <= 0) {
            this.setHealth(10);
        }
        this.setAttackdmg(5);
    }

    /** 
     * ticks for zombie 
     * @param game
    */
    public void tick(List<Entity> entities) {
        if (!this.getPosition().equals(GameFile.getPlayer(entities).getPosition())) {
            Position futurePos = getPosition().getRandomPosition();
            int moveCounts = 0;
            while (moveCounts < 100) {
                if (canMoveTo(futurePos, null, entities, "")) {
                    move(futurePos);
                    break;
                } else {
                    futurePos = getPosition().getRandomPosition();
                }
                moveCounts++;
            }
        }
    }
    /**
     * This function checks if the zombie can move to the area
     * @param pos
     * @param direction 
     * @param game
     */
    public boolean canMoveTo(Position pos, Direction direction, List<Entity> entities, String gameMode) {
        List<Entity> ents = GameFile.findEntityFromPos(pos, entities);
        boolean isPossible = true;
        // If list is empty then it means the space is empty.
        if (ents.isEmpty()) {
            isPossible = true;
            return isPossible;
        }
        // Now we loop through the list to see what entities are in that position
        for (Entity ent : ents) {
            if (ent instanceof Collidable) {
                Collidable obj = (Collidable) ent;
                isPossible = obj.canMoveOnto(this); 
                
            } else if (ent instanceof Player) {
                Player playerEnt = (Player) ent;
                 isPossible = true; 
                //if player is invisible and/or invincible
                if (playerEnt.isInvisible()) {
                    isPossible = false; 
                } else if (playerEnt.isInvincible()) {
                    isPossible = false; 
                    //moveAwaymethod(); 
                } else {
                    isPossible = true; 
                    // battle(playerEnt, game);
                }
            }
        }
        return isPossible;
    }
}
