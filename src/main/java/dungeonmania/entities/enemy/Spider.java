package dungeonmania.entities.enemy;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

import dungeonmania.entities.Collidable;
import dungeonmania.entities.Entity;
import dungeonmania.entities.Player;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Direction;
import dungeonmania.util.Position;

public class Spider extends Enemy{
    private Deque<Position> spiderMovement = new ArrayDeque<>();

    /**
     * constructor for spider
     * @param pos
     */
    public Spider(Position pos, String id, double health) {
        super(pos, "spider", true, health, false, id);
        if (health <= 0) {
            this.setHealth(3);
        }
        this.setAttackdmg(1);
        spiderMovementSetup();
    }

    /**
     * Sets up the spiderMovement attribute by getting adjacent positions from the spider's spawn position and popping off the first one (and moving it to the end) since that is top left where it should be top middle which is the second.
     */
    private void spiderMovementSetup() {
        this.spiderMovement.addAll(getPosition().getAdjacentPositions());
        this.spiderMovement.add(this.spiderMovement.pop());
    }

    /**
     * Reverses the order of the queue for spiderMovement positions.
     */
    public void spiderReverseDirection() {
        // Creates a new ArrayDeque and inputs each of the spider movement at the front of the queue so it is in reverse order.
        Deque<Position> reverseSpiderMovement = new ArrayDeque<>();
        this.spiderMovement.stream().forEach(pos -> reverseSpiderMovement.addFirst(pos));

        // Clears the current spiderMovement queue and adds all of the elements of reverseSpiderMovement.
        this.spiderMovement.clear();
        this.spiderMovement.addAll(reverseSpiderMovement);

        // The reversed now will have the current position as the next one, so adds that to the end of the queue.
        this.spiderMovement.add(this.spiderMovement.pop());
    }

    /**
     * Determines whether the Spider can move to the next spot.
     * @param direction (Direction) - The direction of the next position to move to (does not affect Spider as it has its own movements).
     * @param game (GameFile) - The current file of the game containing this Spider.
     */
    @Override
    public boolean canMoveTo(Position pos, Direction direction, List<Entity> entities, String gameMode) {
        Position potentialPosition = this.spiderMovement.peek();
        List<Entity> entityList = GameFile.findEntityFromPos(potentialPosition, entities);
        boolean isPossible = true;
        for (Entity ent : entityList) {
            if (ent instanceof Collidable) {
                Collidable obj = (Collidable) ent;
                return obj.canMoveOnto(this); 
            } else if (ent instanceof Player) {
                Player player = (Player) ent;
                //if player is invisible and/or invincible
                if (player.isInvisible()) {
                    return false; 
                } else if (player.isInvincible()) {
                    //moveAwaymethod(); 
                    return false; 
                } else {
                    // battle(player, game);
                }
            } else if (ent instanceof Enemy) {
                isPossible = false;
            }
        }
        return isPossible;
    }
    
    /**
     * Attempts to move the Spider.
     * @param direction (Direction) - The direction of the next position to move to (does not affect Spider as it has its own movements).
     * @param game (GameFile) - The current file of the game containing this Spider.
     */
    public void tick(List<Entity> entities) {
        if (swampChecks(entities)) {
            if (canMoveTo(null, null, entities, "")) {
                // Moves the spider to the popped position in the spiderMovement queue, 
                // and then adds that element to the end of the queue (since the spider 
                // goes in circles).
                setPosition(this.spiderMovement.pop());
                spiderMovement.add(getPosition());
            }
        }

    }


    
}
