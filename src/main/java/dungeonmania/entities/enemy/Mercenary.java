package dungeonmania.entities.enemy;

import java.util.*;

import org.json.JSONObject;

import dungeonmania.entities.Collidable;
import dungeonmania.entities.Entity;
import dungeonmania.entities.Interactable;
import dungeonmania.entities.Inventory;
import dungeonmania.entities.Player;
import dungeonmania.entities.SwampTile;
import dungeonmania.entities.enemy.shortestPath.Grid;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Armour;
import dungeonmania.exceptions.InvalidActionException;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Direction;
import dungeonmania.util.Position;

public class Mercenary extends Enemy implements Interactable {
    // private Armour armour;
    private int battleRadius;
    private int mindControlTick = -1;
    private boolean pathToPlayer;

    /**
     * @return battle radius of mercenary
     */
    public int getBattleRadius() {
        return battleRadius;
    }

    public Mercenary(Position pos, String type, String id, double health, boolean hasArmour) {
        super(pos, type, true, health, hasArmour, id);
    }
    /**
     * constructor to pass assassin
     * @param pos
     * @param type
     * @param id
     * @param health
     */
    public Mercenary(Position pos, String type, String id, double health) {
        super(pos, type, true, health, id);
        if (super.isHasArmour()) {
            setArmour(new Armour(pos, "-100"));
        }
    }

    /**
     * constructor for mercenary
     * @param pos
     */
    public Mercenary(Position pos, String id, double health, boolean hasArmour) {
        super(pos,"mercenary", true, health, hasArmour, id);
        if (health <= 0) {
            this.setHealth(5);
        }
        this.setAttackdmg(4);
    }
    
    /**
     * constructor for mercenary
     * @param pos
     */
    public Mercenary(Position pos, String id, double health) {
        super(pos,"mercenary", true, health, id);
        if (health <= 0) {
            this.setHealth(5);
        }
        this.setAttackdmg(4);
        super.setHasArmour(getRandomBoolean());
        if (super.isHasArmour()) {
            setArmour(new Armour(pos, "-100"));
        }
    }

    /**
     * Sets the current mind control tick level (when it reaches 10, the mercenary/assassin is no longer under mind control).
     * @param tick
     */
    public void setMindControlTick(int tick) {
        this.mindControlTick = tick;
    }
    /**
     * ticks the mercenary
     * @param game
     */
    public void tick(List<Entity> entities) {
        Player player = (Player) GameFile.getPlayer(entities);
        if (swampChecks(entities)) {
            Grid grid = new Grid();

            if (!player.isInvisible()) {
                if (player.isInvincible() && !isIsally()) {
                    moveToFurthest(entities);
                } else if (isIsally()) {
                    moveToClosest(entities);
                
                } else {
                    Map <Position, Position> map = dijkstras(grid, this.getPosition(), entities);
                    Position playerPos = player.getPosition();

                    Position prev = map.get(playerPos);
                    Position next = map.get(playerPos);
                    if (!this.pathToPlayer) {
                        move(this.getPosition());
                
                    } else if (next == null) {
                        move(player.getPosition());
                    } else {
                        next = playerPos;
                        while(!next.equals(this.getPosition())) {
                            prev = next;
                            next = map.get(next);
        
                        }
                        move(prev);
                    }
                }
            }
        }
        // If the mercenary is under mind control and the mind control has ended, then 
        // removes it from being an ally and sets is ally to false.
        if (this.mindControlTick >= 10) {
            this.mindControlTick = -1;
            super.setIsally(false);
            player.removeAllies(this.getId());
        // If their mind control tick is greater than or equal to 0, just increases the tick through mind control.
        } else if (this.mindControlTick >= 0) {
            this.mindControlTick += 1;
        }
    }
    

    public Map <Position, Position> dijkstras(Grid grid, Position source, List<Entity> entities) {
        Map <Position, Double>  dist = new HashMap <Position, Double>();
        Map <Position, Position> prev = new HashMap <Position, Position>();
        Map <Position, Double> cost = new HashMap <Position, Double>();
        
        List<Position> q = new ArrayList<Position>();
        
        for(Position pos : grid.getGrid()) {
            dist.put(pos, Double.POSITIVE_INFINITY);
            prev.put(pos, null);
            cost.put(pos, cost(pos, entities));
            q.add(pos);
        }
        
        dist.replace(source, (double) 0);

        while (!q.isEmpty()) { 
            Position u = getLowestTick(q, dist);

            if (u == null) {
                break;
            }
            q.remove(u);
            for (Position v : u.getCardinalPositions()) {

                if (dist.containsKey(v) && dist.containsKey(u)) {
                    
                    if (dist.get(u) + cost.get(v) < dist.get(v)) {
                        dist.replace(v, dist.get(u) + cost.get(v));
                        prev.replace(v, u);
                    }
                }
            }
        }

        this.pathToPlayer = Double.isFinite(dist.get(GameFile.getPlayer(entities).getPosition()));
        return prev;
    }


    public double cost(Position pos, List<Entity> entities) {
        List<Entity> ents = GameFile.findEntityFromPos(pos, entities);
        boolean isPossible = true;
        // If list is empty then it means the space is empty.
        if (ents.isEmpty()) {
            return 1;
        }
        // Now we loop through the list to see what entities are in that position
        for (Entity ent : ents) {
            if (ent instanceof Collidable) {
                Collidable obj = (Collidable) ent;
                isPossible = obj.canMoveOnto(this); 
            } else if (ent instanceof Enemy && !isIsally()) {
                isPossible = false;
            } else if (ent instanceof SwampTile) {
                SwampTile swamp = (SwampTile) ent;
                return swamp.getMovementFactor();
            }
        }
        if (isPossible) {
            return 1;
        } else {
            return Double.POSITIVE_INFINITY;
        }
    }

    public Position getLowestTick(List<Position> q, Map <Position, Double> dist){ 
        Position lowest = null;
        double lowestDistance = Double.POSITIVE_INFINITY;
        for (Position pos : q){
            if (dist.get(pos) < lowestDistance) {
                lowestDistance = dist.get(pos);
                lowest = pos;
            }
            if (lowestDistance == 1) {
                return lowest;
            }
        }


        // If the mercenary's mind control tick is less than 0, then they are either bribed or not an ally of the player determined by whether they are an ally or not.
        return lowest;
    }

    /**
     * moves the emrcenary closer to player
     * @param game
     */
    public void moveToClosest(List<Entity> entities) {
        Position optimalPos = this.getPosition();
        Position playerPos = GameFile.getPlayer(entities).getPosition();

        List<Position> positions = super.getPosition().getCardinalPositions();
        int shortest = this.getPosition().distanceBetween(this.getPosition(), playerPos);
        for (Position pos : positions) {
            if (pos.distanceBetween(pos, playerPos) <= shortest && this.canMoveTo(pos, null, entities, "")) {
                shortest = pos.distanceBetween(pos, playerPos);
                optimalPos = pos;
            }
        }
        move(optimalPos);

    }


    /**
     * moves mercenary away from player
     * @param game
     */
    public void moveToFurthest(List<Entity> entities) {
        Position optimalPos = new Position(getPosition().getX(), getPosition().getY()); 
        Position playerPos = GameFile.getPlayer(entities).getPosition();
        
        List<Position> positions = super.getPosition().getCardinalPositions();

        int currentFurthest = getPosition().distanceBetween(getPosition(), playerPos);
        for (Position pos : positions) {
            if (pos.distanceBetween(pos, playerPos) > currentFurthest && this.canMoveTo(pos, null, entities, "")) {
                currentFurthest = pos.distanceBetween(pos, playerPos);
                optimalPos = pos;
            }
        }
        move(optimalPos);
    }
    /**
     * @param pos
     * @param direction 
     * @param game
     * @return if the mercenary can move to the next spot
     */
    public boolean canMoveTo(Position pos, Direction direction, List<Entity> entities, String gameMode) {
        List<Entity> ents = GameFile.findEntityFromPos(pos, entities);
        boolean isPossible = true;
        // If list is empty then it means the space is empty.
        if (ents.isEmpty()) {
            return isPossible;
        }
        // Now we loop through the list to see what entities are in that position
        for (Entity ent : ents) {
            if (ent instanceof Collidable) {
                Collidable obj = (Collidable) ent;
                return obj.canMoveOnto(this); 
            } else if (ent instanceof Player && isIsally()) {
                isPossible = false;
            } else if (ent instanceof Enemy){
                isPossible = false;
            }
        }
        return isPossible;
    }


    /**
     * @return a random boolean
     */
    public boolean getRandomBoolean() {
        Random random = new Random();
        return random.nextBoolean();
    }


    public void interact (Inventory inventory, List<Entity> entities) throws InvalidActionException {
        List<Position> cardinals = this.getPosition().getPositionsInRange(2);
        // If the player has a sceptre, then they can mind control the mercenary from anywhere on the dungeon.
        if (inventory.numberOf("sceptre") >= 1) {
            super.setIsally(true);
            Player player = (Player) GameFile.getPlayer(entities);
            player.addAllies(this.getId());
            this.mindControlTick = 0;
        } else if (inventory.numberOf("sun_stone") >= 1 && cardinals.contains(GameFile.getPlayer(entities).getPosition())) {
            super.setIsally(true);
            Player player = (Player) GameFile.getPlayer(entities);
            player.addAllies(this.getId());
            this.mindControlTick = -1;
        } else if (inventory.numberOf("treasure") >= 1 && cardinals.contains(GameFile.getPlayer(entities).getPosition())) {
            super.setIsally(true);
            inventory.useItem("treasure");
            Player player = (Player) GameFile.getPlayer(entities);
            player.addAllies(this.getId());
            this.mindControlTick = -1;
        } else {
            throw new InvalidActionException("You are not next to the mercenary/do not have treasure");
        }
    }

    public boolean inBattleRadius(Player player) {
        if (player.getPosition().distanceBetween(this.getPosition(), player.getPosition()) <= battleRadius) {
            return true;
        } else {
            return false;
        }
        
    }


    @Override
    public JSONObject getEntAsJSON() {
        
        JSONObject entJSON = new JSONObject();
        entJSON.put("x", this.getPosition().getX()); 
        entJSON.put("y", this.getPosition().getY());
        entJSON.put("type", this.getType());
        entJSON.put("health", this.getHealth());
        entJSON.put("attackdmg", (int) this.getAttackdmg());
        entJSON.put("hasArmour", this.getHasArmour());
        //entJSON.put("pathToPlayer", this.getHasArmour());
        if (this.getHasArmour()) {
            entJSON.put("armour", this.getArmour().getId());
        }
        entJSON.put("id", this.getId());
        
        return entJSON;
    }

}
