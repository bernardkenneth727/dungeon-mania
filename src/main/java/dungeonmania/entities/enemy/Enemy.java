package dungeonmania.entities.enemy;

import org.json.JSONObject;

import dungeonmania.entities.Entity;
import dungeonmania.entities.MoveableEntity;
import dungeonmania.entities.Player;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Armour;
import dungeonmania.util.Position;

public abstract class Enemy extends MoveableEntity {
    private double attackdmg;
    private double health;
    private boolean hasArmour;
    private Armour armour;
    private boolean isAlly;


    /**
     * constructor for enemy
     * @param pos
     * @param type
     * @param isInteractable
     */
    public Enemy(Position pos, String type, boolean isInteractable, double health, boolean hasArmour, String id) {
        super(pos, type, isInteractable, id);
        this.health = health;
        this.hasArmour = hasArmour;
        this.isAlly = false;
    }
    /**
     * constructor for enemy
     * @param pos
     * @param type
     * @param isInteractable
     */
    public Enemy(Position pos, String type, boolean isInteractable, double health, String id) {
        super(pos, type, isInteractable, id);
        this.health = health;
        this.isAlly = false;
    }

    
    public boolean isIsally() {
        return isAlly;
    }

    public void setIsally(boolean isally) {
        this.isAlly = isally;
    }

    /**
     * @return health of entity
     */
    public double getHealth() {
        return health;
    }
    /**
     * sets health of entity
     * @param health 
     */
    public void setHealth(double health) {
        this.health = health;
    } 
    /**
     * @return attack dmg
     */
    public double getAttackdmg() {
        return attackdmg;
    }
    /**
     * sets attackdmg
     * @param attackdmg 
     */
    public void setAttackdmg(double attackdmg) {
        this.attackdmg = attackdmg;
    }
    /**
     * @return if it has armour
     */
    public boolean isHasArmour() {
        return hasArmour;
    }
    /**
     * set if they have armour
     * @param hasArmour
     */
    public void setHasArmour(boolean hasArmour) {
        this.hasArmour = hasArmour;
    }
    /**
     * @return the armour
     */
    public Armour getArmour() {
        return armour;
    }
    /**
     * set the armour
     * @param armour
     */
    public void setArmour(Armour armour) {
        this.armour = armour;
    }



    @Override
    public JSONObject getEntAsJSON() {
        
        JSONObject entJSON = new JSONObject();
        entJSON.put("x", this.getPosition().getX()); 
        entJSON.put("y", this.getPosition().getY());
        entJSON.put("type", this.getType());
        entJSON.put("health", this.health);
        entJSON.put("isAlly", this.isAlly);
        entJSON.put("attackdmg", (double) this.attackdmg);
        entJSON.put("hasArmour", this.hasArmour);
        entJSON.put("id", this.getId());
        
        return entJSON;
    }

}
