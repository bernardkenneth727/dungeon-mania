package dungeonmania.entities;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import dungeonmania.util.*;

public class SwampTile extends Entity{

    Map <String, Integer> map;
    private int movementFactor;

    /**
     * Constructor for Swamp tiles
     * @param pos
     * @param movementFactor ticking time it takes to move through swamp
     * @param id
     */

    public SwampTile(Position pos, int movementFactor, String id) {
        super(pos, "swamp_tile", false, id); 
        this.movementFactor = movementFactor; 
        this.map = new HashMap <String, Integer>();
    }

    public int getMovementFactor() {
        return movementFactor;
    }
    
    /**
     * Checks the map of entities with same position as swamp tile and keeps track of how long the enemy
     * has been stuck in the tile. 
     * @param id
     * @param ent
     * @return true if entity can move, false if it cannot
     */
    public boolean onSwampTile(String id, MoveableEntity ent) {
        if (map.containsKey(id) && map.get(id) == (getMovementFactor()-1)) {
            map.remove(id);
            ent.setOnSwampTile(false);
            return true;
        } else {
            int counter = map.get(id)+1;
            map.replace(id, counter); 
            ent.setSwamp(this);
            return false;
        }

    }
    /**
     * Adds to the swamp tile map if it is an enemy that spawns on swamp tile/moved onto it
     * Sets the boolean and swamp tile on the Moveable entity
     * @param id id of entity
     * @param ent entity itself
     * @return false when the enemy is added and can no longer move
     */
    public boolean addToSwamp(String id, MoveableEntity ent) {
        System.out.println(this.getMovementFactor());
        map.put(id, 1);
        ent.setSwamp(this);
        ent.setOnSwampTile(true);
        return false;

    }


    @Override
    public JSONObject getEntAsJSON() {
        
        JSONObject entJSON = new JSONObject();
        entJSON.put("x", this.getX()); 
        entJSON.put("y", this.getY());
        entJSON.put("type", "swamp_tile");
        entJSON.put("movement_factor", this.movementFactor);
        
        return entJSON;
    }
}
