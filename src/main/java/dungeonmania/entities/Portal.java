package dungeonmania.entities;

import java.util.List;

import org.json.JSONObject;

import dungeonmania.util.Direction;
import dungeonmania.util.Position;

public class Portal extends Entity {
    
    private String colour; 

    /**
     * constructor for portal
     * @param x
     * @param y
     * @param colour
     */
    Portal(Position pos, String colour, String id) {
        super(pos, "portal", false, id); 
        this.colour = colour; 
    }

    /**
     * @return colour of portal
     */
    public String getColour() {
        return colour; 
    }
    
    /**
     * teleports the player
     * @param entity
     * @param gamefile
     */
    public boolean teleport(Entity entity, List<Entity> entities, Direction direction) { 
        //Position newpos; 
        List<Entity> entList = entities;
        // run through list of entities and find any portal type
        for (Entity ent : entList) {
            if (ent.getType().equals("portal")) {
                // check if the colour is the same and the ids are different
                Portal pair = (Portal) ent;
                if (pair.getColour().equals(getColour()) && (!ent.getId().equals(getId()))) {
                    Position pairPos = pair.getPosition();
                    Position postPortalPos = pairPos.translateBy(direction);
                    for (Entity e : entities) {
                        if (e.getType().equals("wall") && e.getPosition().equals(postPortalPos)) {
                            return false; 
                        }
                    }

                    entity.setPosition(ent.getPosition());
                    return true; 

                }
            }
        }

        return false; 
    }  
    /**
     * @return portal as a JSON Object
     */
    @Override
    public JSONObject getEntAsJSON() {
        
        JSONObject entJSON = new JSONObject();
        entJSON.put("x", this.getX()); 
        entJSON.put("y", this.getY());
        entJSON.put("type", "portal");
        entJSON.put("key", this.colour);
        
        return entJSON;
    }
}
