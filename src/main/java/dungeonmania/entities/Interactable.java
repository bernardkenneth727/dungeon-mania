package dungeonmania.entities;

import java.util.List;

import dungeonmania.exceptions.InvalidActionException;

public interface Interactable {
    /**
     * interacts with an entity
     * @param inventory
     * @param game
     * @throws InvalidActionException
     */
    public void interact(Inventory inventory, List<Entity> entities) throws InvalidActionException;
    
}
