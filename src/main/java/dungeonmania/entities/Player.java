package dungeonmania.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONObject;

import dungeonmania.util.Direction;
import dungeonmania.entities.enemy.Enemy;
import dungeonmania.entities.enemy.Mercenary;
import dungeonmania.entities.itemEntity.ItemEntity;
import dungeonmania.entities.itemEntity.collectableEntity.CollectableEntity;

import dungeonmania.entities.itemEntity.collectableEntity.potion.Potion;
import dungeonmania.entities.itemEntity.collectableEntity.rare.Rare;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Bomb;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Durable;
import dungeonmania.exceptions.InvalidActionException;
import dungeonmania.gameFile.GameFile;

import dungeonmania.util.*;


public class Player extends MoveableEntity {
    private double health;
    private double attackdmg;
    private double defaultdmg = 1;

    private boolean hard; 

    private Inventory inventory;
    private List<Potion> potionList = new ArrayList<Potion>();
    private List<String> allies = new ArrayList<String>();

    /**
     * constructor for player
     * @param x
     * @param y
     */
    public Player(Position pos, String id, double health) {
        super(pos, "player", true, id);
        this.health = health;
        this.attackdmg = 1;
        this.inventory = new Inventory();

        if (this.health == 10.11) {
            this.hard = false; 
        } else {
            this.hard = true; 
        }
    }
    /**
     * constructor for player
     * @param x
     * @param y
     */
    public Player(Position pos, double health, double attackdmg, boolean hasArmour, boolean hasShield, String id) {
        super(pos, "player", true, id);
        this.health = health;
        this.attackdmg = attackdmg;

        if (this.health == 10.11) {
            this.hard = false; 
        } else {
            this.hard = true; 
        }
    }
    /**
     * sets the inventory (used with Data class)
     * @param inventory
     */
    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    /**
     * sets the health to full
     */
    public void regenerateHealth() {
        health += (double) (health * 0.25); 
    }
    /**
     * sets health
     * @param health
     */
    public void setHealth(double health) {
        this.health = health;
    }
    /**
     * @return health
     */
    public double getHealth() {
        return health; 
    }
    /**
     * @return attackdmg
     */
    public double getAttackdmg() {
        return attackdmg;
    }
    /**
     * sets attackdmg
     * @param attackdmg
     */
    public void setAttackdmg(double attackdmg) {
        this.attackdmg = attackdmg;
    }
    /**
     * @return inventory
     */
    public Inventory getInventory() {
        return inventory;
    }

    public void addAllies(String id) {
        this.allies.add(id);
    }

    public void removeAllies(String id) {
        this.allies.remove(id);
    }

    /**
     * Tick Function for using potion
     * Searches inventory for item, finds it and removes form inventory.
     * Potion effects are applied and added to player potion list.
     * @param potionType
     */
    public void tick(String potionId) {
        ItemEntity item = inventory.findItem(potionId);
        Potion p = (Potion) inventory.useItem(item.getType());
        if (p != null) {
            if (this.hard == true && !p.getPower().equals("invincible")) {
                potionList.add(p);
                p.activate(this);
            } else if (this.hard == false) {
                potionList.add(p);
                p.activate(this);
            }
        } else {
            throw new InvalidActionException("Potion out of stock");
        }
    }

    public void tickBomb(String bombId, List<Entity> entities) {
        Bomb myBomb = (Bomb) inventory.findItem(bombId);
        if (myBomb == null) {
            throw new InvalidActionException("Bomb out of stock");
        } else {
            myBomb.placeDown(entities, inventory);
        }
    }

    /**
     * Normal tick for moving location
     * @param direction
     * @param game
     */
    public void tick(Direction direction, List<Entity> entities, String gameMode) {

        //decrease life of all potions in use 
        if (!potionList.isEmpty()) {
            Potion potionRemove = null; 
            for (Potion potion : potionList) {
                potion.decreasePotionLife(this);
                if (!potion.isActive()) {
                    potionRemove = potion; 
                }
            }
            potionList.remove(potionRemove); 
        }

        //move
        if (canMoveTo(null, direction, entities, gameMode)) {
            move(direction);
            //checkEnemyOnPlayer(entities, gameMode);
        }
    }
    /**
     * checks if the player can move to the spot
     * @param pos
     * @param direction
     * @param game
     */
    public boolean canMoveTo(Position pos, Direction direction, List<Entity> entities, String gameMode) {
        Position newPos = getFuturePos(direction);

        List<Entity> ents = GameFile.findEntityFromPos(newPos, entities);
        boolean isPossible = true;
        // If list is empty then it means the space is empty.
        if (ents.isEmpty()) {;
            return isPossible;
        }
        // Now we loop through the list to see what entities are in that position
        for (Entity ent : ents) {
            // We check if entity is a subclass of collectable. If yes then we typecast it and call pickupcollectable.
            if (ent instanceof CollectableEntity){
                CollectableEntity collectEnt = (CollectableEntity) ent;
                collectEnt.pickupCollectable(entities, this.inventory);
            }

            if (ent instanceof Boulder) {
                Boulder boulderEnt = (Boulder) ent;
                isPossible = boulderEnt.canMoveTo(null, direction, entities, gameMode);
                
            //Checks if player can move onto walls, doors, enemies
            } else if (ent instanceof Collidable) {
                Collidable obj = (Collidable) ent;
                isPossible = obj.canMoveOnto(this); 
                return isPossible;

            } else if (ent instanceof Exit) {
                Exit exit = (Exit) ent;
                exit.pressExit();
                isPossible = true; 

            //Portal
            } else if (ent instanceof Portal) {
                Portal portal = (Portal) ent;
                isPossible = portal.teleport(this, entities, direction);
                
            }
        }
        return isPossible;
    }
    /**
     * checks if the enemy is on the player
     * @param game
     */
    public void checkEnemyOnPlayer(List<Entity> entities, String gameMode){
        
        List<Entity> ents = GameFile.findEntityFromPos(this.getPosition(), entities);
        
        if (!ents.isEmpty() ) {
            for (Entity ent : ents) {
                if (ent instanceof Enemy){
                    Enemy enemyEnt = (Enemy) ent;
                    battle(enemyEnt, entities, gameMode);
                }
                
            }
        }
    }
    /**
     * @param enemy
     * @param game
     * @return if battle is won or lost 
     */
    public boolean battle(Enemy enemy, List<Entity> entities, String gameMode) {
        this.setAttackdmg(inventory.findWeaponDamage() + this.defaultdmg); 

        if (enemy.isIsally()) {
            return true;
        }
        // If invisible, no battle should occur.
        if (isInvisible()) {
            return true;
        }
        while (true) {
            //if invincible 
            if (isInvincible() || gameMode.equals("peaceful")) {
                attack(enemy, entities);
                entities.remove(enemy);
                GameFile.setAllEntities(entities);
                return true;
            } else {
                //If false is returned then the enemy is dead. We then remove him from list
                if (attack(enemy, entities) == false) {
                    entities.remove(enemy);
                    GameFile.setAllEntities(entities);

                    // If the enemy has an armour, then it is given to the player.
                    if (enemy.getArmour() != null) {
                        this.inventory.addItem(enemy.getArmour());
                    }

                    Rare.battleWinnings(this.inventory);
                    return true; 
                }
                //If false is returned then the player is dead.
                if (receivedmg(enemy) == false) {
                    entities.remove(this);
                    GameFile.setAllEntities(entities);
                    return false;
                }
            }
            // Reduce the durability of each durable, i.e. shield, sword, armour, bow.
            for (Durable durable : inventory.findDurables()) {
                durable.deteriorateWeapon(this.inventory);
            }
        }

    }
    /**
     * @param enemy
     * @return true if there is an anduril and enemy is a hydra
     */
    public boolean hydraAndurilExist(Enemy enemy) {
        // check inventory for anduril 
        if (inventory.getAllItems().stream().filter(e -> e.getType().equals("anduril")).collect(Collectors.counting()) >= 1) {
            return true;
        }
        return false;
    }

    /**
     * Attack fucntion for player. This can be overridden depending on potion.
     * @param enemy
     * @return
     */
    public boolean attack(Enemy enemy, List<Entity> entities) {
        if (enemy.isHasArmour()) {
            enemy.setHealth(enemy.getHealth()- ((this.health * this.attackdmg/2) / 5));
        } else if (enemy.getType().equals("hydra") && !hydraAndurilExist(enemy) && Math.random() * 2 < 1) {
            // anduril does not exist 
            // wins the 50/50 then health will increase instead
            enemy.setHealth(enemy.getHealth() + ((this.health * this.attackdmg) / 5));
        } 
        else {
            enemy.setHealth(enemy.getHealth()- ((this.health * this.attackdmg) / 5));
        }
        for (String id : allies) {
            Entity ally = entities.stream().filter(e -> e.getId().equals(id)).findFirst().get();
            Mercenary mercAlly = (Mercenary) ally;
            if (mercAlly.inBattleRadius(this) && mercAlly.isHasArmour()) {
                enemy.setHealth(enemy.getHealth()- ((mercAlly.getHealth() * mercAlly.getAttackdmg()/2) / 5));
            } else if (mercAlly.inBattleRadius(this)) {
                enemy.setHealth(enemy.getHealth()- ((mercAlly.getHealth() * mercAlly.getAttackdmg())/ 5));
            }
        }
        return checkAlive(enemy);
    }
    /**
     * Player takes damage function. This can be overridden depending on potion.
     * @param enemy
     * @return
     */
    public boolean receivedmg(Enemy enemy) {
        this.health = this.getHealth() - ((enemy.getHealth() * enemy.getAttackdmg() / this.inventory.findTotalProtectionLevel()) / 10);
        System.out.println("Player health1: " + getHealth()); 
        return checkAlive(this);
    }
    /**
     * check player potion status
     * @return
     */
    public boolean isInvincible() {
        for (Potion potion : potionList) {
            if (potion.getPower().equals("invincible")) {
                return true; 
            }
        }

        return false; 
    }
    /**
     * @return IF PLAYER IS INVISIBLE
     */
    public boolean isInvisible() {
        for (Potion potion : potionList) {
            if (potion.getPower().equals("invisible")) {
                return true; 
            }
        }

        return false; 
    }


    public void setInventoryFromData(Data data) {
        this.inventory = new Inventory();
        for (EntityData ent : data.getInventory()) {
            Entity itemEnt = EntityFactory.loadEntity(ent);
            ItemEntity item = (ItemEntity) itemEnt;
            this.inventory.addItem(item);
        }
    }

    /**
     * @return player as a JSON Object
     */
    @Override
    public JSONObject getEntAsJSON() {
        
        JSONObject entJSON = new JSONObject();
        entJSON.put("x", this.getPosition().getX()); 
        entJSON.put("y", this.getPosition().getY());
        entJSON.put("type", "player");
        entJSON.put("health", this.health);
        entJSON.put("attackdmg", (double) this.attackdmg);
        entJSON.put("id", this.getId());
        
        return entJSON;
    }
}