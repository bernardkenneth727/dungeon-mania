package dungeonmania.entities;
import dungeonmania.entities.itemEntity.ItemEntity;
import dungeonmania.entities.itemEntity.collectableEntity.protection.Protection;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Durable;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Weapon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import java.util.List;

/*From inventory we should be able to;
- get a list of entity ID's from the map
- get a list of itemEntities from the map
- add Items into map when given an itemEntity
- removeItem from map when given the item
- removeItem from map when given ID of item
- return a specific item given an ID
- counts number of items of a type given 
*/
public class Inventory {
    private  Map<String, ItemEntity> itemMap;

    /**
     * constructor for inventory
     */
    public Inventory() {
        this.itemMap = new HashMap<String, ItemEntity>();
    }
    /**
     * 
     * @return a list of itemEntities from the map
     */
    public List<ItemEntity> getAllItems() {
        List<ItemEntity> allItems = itemMap.entrySet()
            .stream()
            .map(Map.Entry::getValue)
            .collect(Collectors.toList());
        return allItems;
    }
    /**
     * addItems into map when given an itemEntity
     * @param item
     */
    public void addItem(ItemEntity item) {
        itemMap.put(item.getId(), item);
    }
    /**
     * Remove the first instance of the item
     * @param item
     */
    public void useItem(ItemEntity item) {
        // String type = item.getType();
        // ItemEntity foundItem = itemMap.values().stream().filter(t -> t.getType().equals(type)).findFirst().get();
        // itemMap.remove(foundItem.getId());
        this.itemMap.remove(item.getId());

    }
    /**
     * Remove first instance of the item when given the type of item
     * @param type
     * @return the item Entity
     */
    public ItemEntity useItem(String type) {
        ItemEntity foundItem = itemMap.values().stream().filter(t -> t.getType().equals(type)).findFirst().get();
        itemMap.remove(foundItem.getId());
        return foundItem;
    }
    /**
     * @param type
     * @return first instance of the item when given the type of item
     */
    public ItemEntity getItem(String type) {
        ItemEntity foundItem = itemMap.values().stream().filter(t -> t.getType().equals(type)).findFirst().get();
        return foundItem;
    }
    /**
     * @param ItemID
     * @return a specific item given an ID
     */
    public ItemEntity findItem(String ItemID) {
        return itemMap.get(ItemID);
    }
    /**
     * @param type
     * @return number of objects of that type
     */
    public int numberOf(String type) {
        long counter = itemMap.values().stream().filter(t -> t.getType().equals(type)).collect(Collectors.counting());
        return (int)counter;
    }
    /**
     * @return double - the total weapon damage from all Weapons in the player's inventory.
     */
    public double findWeaponDamage() {
        double damageCount = 0;
        for (ItemEntity item : itemMap.values()) {
            if (item instanceof Weapon) {
                damageCount += ((Weapon)item).getWeaponDamage();
            }
            
        }
        return damageCount;
    }
    /**
     * @return List of ItemEntity - list of all the player's weapons in their inventory.
     */
    public List<ItemEntity> findWeapons() {
        List<ItemEntity> weapons = new ArrayList<>();
        for (ItemEntity item : itemMap.values()) {
            if (item instanceof Weapon) {
                weapons.add(item);
            }
        }
        return weapons;
    }

    /**
     * Finds all the durables in the player's inventory.
     * @return List of Durables - Returns a list.
     */
    public List<Durable> findDurables() {
        List<Durable> durables = new ArrayList<>();
        for (ItemEntity item : itemMap.values()) {
            if (item instanceof Durable) {
                durables.add((Durable)item);
            }
        }
        return durables;
    }

    public double findTotalProtectionLevel() {
        double totalProtectionLevel = 1;
        for (ItemEntity item : itemMap.values()) {
            if (item instanceof Protection) {
                totalProtectionLevel += ((Protection)item).getProtectionLevel();
            }
        }

        return totalProtectionLevel;
    }

    public boolean contains(String id) {
        boolean has = itemMap.keySet().stream().anyMatch(e -> e.contains(id));
        return has;
    }
}

