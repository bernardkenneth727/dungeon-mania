package dungeonmania.entities;
import java.util.List;

import dungeonmania.entities.enemy.Zombie;
import dungeonmania.entities.itemEntity.ItemEntity;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Durable;
import dungeonmania.exceptions.InvalidActionException;
import dungeonmania.util.Position;
import dungeonmania.gameFile.GameFile;

public class ZombieToastSpawner extends Entity implements Interactable {
    private int tickCount;
    /**
     * constructor for spawner
     * @param x
     * @param y
     */
    public ZombieToastSpawner(Position pos, String id) {
        super(pos, "zombie_toast_spawner", true, id);
        this.tickCount = 1;
    }
    /**
     * @return tick count
     */
    public int getTickCount() {
        return tickCount;
    }
    /**
     * sets the tick count
     * @param tickCount
     */
    public void setTickCount(int tickCount) {
        this.tickCount = tickCount;
    }
    /**
     * ticks the zombie and spawns if necessary
     * @param game
     */
    public void tick(List<Entity> entities, String gameMode){

        if (gameMode.equals("hard") && tickCount == 15) {
            spawnZombie(entities, gameMode);
            tickCount = 1;
        }
        else if (!gameMode.equals("hard") && tickCount == 20) {
            spawnZombie(entities, gameMode);
            tickCount = 1;
        } else {
            tickCount++;
        }
    }
    /**
     * spawns in a zombie into the game
     * @param game
     */
    public void spawnZombie(List<Entity> entities, String gameMode) {
        Zombie zombie = new Zombie(this.getPosition(), "-100", -1);
        Position pos = this.getPosition().getRandomPosition();
        while (true) {
            if (zombie.canMoveTo(pos, null, entities, gameMode)) {
                zombie.move(pos);
                entities.add(zombie);
                GameFile.setAllEntities(entities);
                break;
            } else {
                pos = this.getPosition().getRandomPosition();
            }
        }
    }

    /**
     * If the player contains a sword or bow, the spider spawner is destroyed. 
     * The item durability is also decreased on all weapons.
     * Throws Invalid Action Exception if the player does not have sword.
     * Checks player is cardinally adjacent
     * @param inventory
     * @param game
     * @throws InvalidActionException
     */
    public void interact (Inventory inventory, List<Entity> entities) throws InvalidActionException {
        //get cardinal positions
        //check if the list contains player location
        List<Position> cardinals = this.getPosition().getCardinalPositions();

        if (!inventory.findWeapons().isEmpty() && cardinals.contains(GameFile.getPlayer(entities).getPosition())) {
            entities.remove(this);
            GameFile.setAllEntities(entities);
            for (ItemEntity item : inventory.findWeapons()) {
                Durable weapon = (Durable) item;
                weapon.deteriorateWeapon(inventory);
            }
        } else {
            throw new InvalidActionException("You do not have a sword");
        }
    }
}

