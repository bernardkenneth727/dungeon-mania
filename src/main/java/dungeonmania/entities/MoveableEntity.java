package dungeonmania.entities;

import dungeonmania.gameFile.GameFile;
import dungeonmania.util.*;

import java.util.List;

public abstract class MoveableEntity extends Entity{
    private boolean onSwampTile;
    public SwampTile swamp;
    /**
     * constructor for moveable entity
     * @param pos
     * @param type
     * @param isInteractable
     * @param id
     */
    public MoveableEntity(Position pos, String type, boolean isInteractable, String id) {
        super(pos, type, isInteractable, id);
    }


    /**
     * @param direction
     * @return the future position
     */
    public Position getFuturePos(Direction direction) {
        Position currentPos = super.getPosition();
        Position newPos = currentPos.translateBy(direction);
        return newPos;
    }
    /**
     * General Move Function to move it to the new location
     * @param direction
     */
    public void move(Direction direction) {
        setPosition(getFuturePos(direction));
        
    }
    /**
     * General Move Function to move it to the new location
     * @param position
     */
    public void move(Position position) {
        setPosition(position);
    }

    public boolean isOnSwampTile() {
        return onSwampTile;
    }
    public void setOnSwampTile(boolean onSwampTile) {
        this.onSwampTile = onSwampTile;
    }
    public SwampTile getSwamp() {
        return swamp;
    }
    public void setSwamp(SwampTile swamp) {
        this.swamp = swamp;
    }
    /**
     * @param ent
     * @return if either enemy or player is alive
     */
    public boolean checkAlive(MoveableEntity ent) {
        boolean aliveStat = true;

        if (ent instanceof Player && ent.getHealth() <= 0) {
            Player player = (Player) ent;
            if (player.getInventory().numberOf("one_ring") != 0) {
                player.getInventory().useItem("one_ring");
                player.setHealth(10.11);
            } else {
                aliveStat = false;
            }
        } else if (ent.getHealth() <= 0) {
            aliveStat = false;
        }
        return aliveStat;
    } 



    // swamp check removes true if you can move, return false if you cant
    public boolean swampChecks(List<Entity> entities) {
        //boolean newlyAdded = false;
        boolean canMove = true;
        if (!this.isOnSwampTile()) {
            List<Entity> currentEnts = GameFile.findEntityFromPos(this.getPosition(), entities);
            for (Entity ent: currentEnts) {
                if (ent instanceof SwampTile) {
                    SwampTile swamp = (SwampTile) ent;    
                    canMove = swamp.addToSwamp(this.getId(), this);
                    return canMove;
                }
            }
        } else if (this.isOnSwampTile()) {
            canMove = this.swamp.onSwampTile(this.getId(), this);
        }
        return canMove;
    }


    /**
     * checks if entity can move to a space
     * @param position
     * @param direction
     * @param game
     * @return boolean
     */
    public abstract boolean canMoveTo(Position position, Direction direction, List<Entity> entities, String gameMode);

}
