package dungeonmania.entities;

public interface Collidable {
    public boolean canMoveOnto(Entity ent);
}
