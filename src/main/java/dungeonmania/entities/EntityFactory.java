package dungeonmania.entities;

import dungeonmania.util.EntityData;
import dungeonmania.util.Position;
import dungeonmania.entities.enemy.*;
import dungeonmania.entities.itemEntity.buildableEntity.*;
import dungeonmania.entities.itemEntity.collectableEntity.*;
import dungeonmania.entities.itemEntity.collectableEntity.potion.*;
import dungeonmania.entities.itemEntity.collectableEntity.rare.*;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.*;
import dungeonmania.gameFile.GameFile;

public class EntityFactory {
    
    /**
     * Factory for creating an entity
     * @param entity
     * @return the entity 
     */
    static public Entity getEntity(EntityData entity, GameFile game) {

        if (entity.getType() == null) {
            return null;
        }
        Position newPos = new Position(entity.getX(), entity.getY());
        
        if (entity.getType().equals("player") && (!game.getGameMode().equals("hard"))) {
            return (Entity) new Player(newPos, "-100", 10.11);
        } else if (entity.getType().equals("player") && game.getGameMode().equals("hard")) {
            return (Entity) new Player(newPos, "-100", 8);
        }
        if (entity.getType().equals("wall")) {
            return (Entity) new Wall(newPos, "-100");
        }
        if (entity.getType().equals("sun_stone")) {
            return (Entity) new SunStone(newPos, "-100");
        }
        if (entity.getType().equals("exit")) {
            return (Entity) new Exit(newPos, "-100");
        }
        if (entity.getType().equals("boulder")) {
            return (Entity) new Boulder(newPos, "-100");
        }
        if (entity.getType().equals("switch")) {
            return (Entity) new FloorSwitch(newPos, "-100");
        }
        if (entity.getType().equals("door")) {
            return (Entity) new Door(newPos, entity.getKey(), "-100");
        }
        if (entity.getType().equals("portal")) {
            return (Entity) new Portal(newPos, entity.getColour(), "-100");
        }
        if (entity.getType().equals("zombie_toast_spawner")) {
            return (Entity) new ZombieToastSpawner(newPos, "-100");
        }
        if (entity.getType().equals("spider")) {
            return (Entity) new Spider(newPos, "-100", -1);
        }
        if (entity.getType().equals("zombie_toast")) {
            return (Entity) new Zombie(newPos, "-100", -1);
        }
        if (entity.getType().equals("mercenary")) {
            return (Entity) new Mercenary(newPos, "-100", -1);
        }
        if (entity.getType().equals("assassin")) {
            return (Entity) new Assassin(newPos, "-100", -1);
        }
        if (entity.getType().equals("hydra")) {
            return (Entity) new Hydra(newPos, "-100", -1);
        }
        if (entity.getType().equals("treasure")) {
            return (Entity) new Treasure(newPos, "treasure", "-100");
        }
        if (entity.getType().equals("key")) {
            return (Entity) new Key(newPos, entity.getKey(), "-100");
        }
        if (entity.getType().equals("health_potion")) {
            return (Entity) new HealthPotion(newPos, "-100");
        }
        if (entity.getType().equals("invincibility_potion")) {
            return (Entity) new InvincibilityPotion(newPos, "-100");
        }
        if (entity.getType().equals("invisibility_potion")) {
            return (Entity) new InvisibilityPotion(newPos, "-100");
        }
        if (entity.getType().equals("wood")) {
            return (Entity) new Wood(newPos, "-100");
        }
        if (entity.getType().equals("arrow")) {
            return (Entity) new Arrow(newPos, "-100");
        }
        if (entity.getType().equals("bomb")) {
            return (Entity) new Bomb(newPos, "-100");
        }
        if (entity.getType().equals("sword")) {
            return (Entity) new Sword(newPos, "-100");
        }
        if (entity.getType().equals("armour")) {
            return (Entity) new Armour(newPos, "-100");
        }
        if (entity.getType().equals("one_ring")) {
            return (Entity) new TheOneRing(newPos, "-100");
        }
        if (entity.getType().equals("anduril")) {
            Sword sword = new Sword(newPos, "-100");
            return (Entity) new Anduril(newPos, sword);
        }
        if (entity.getType().equals("wire")) {
            return (Entity) new Wire(newPos, "-100");
        }
        if (entity.getType().equals("light_bulb_off")) {
            return (Entity) new LightBulb(newPos, entity.getType(), "-100", entity.getLogic());
        }
        if (entity.getType().equals("switch_door")) {
            return (Entity) new SwitchDoor(newPos, "-100", entity.getLogic());
        }
        if (entity.getType().equals("swamp_tile")) {
            return (Entity) new SwampTile(newPos, entity.getMovementFactor(), "-100");
        }
        return null;

    }

    /**
     * Factory for creating an entity
     * @param entity
     * @return the entity 
     */
    static public Entity loadEntity(EntityData entity) {

        if (entity.getType() == null) {
            return null;
        }

        Position newPos = new Position(entity.getX(), entity.getY());

        if (entity.getType().equals("player")) {
            // int x, int y,long health, long attackdmg, Inventory inventory, boolean hasArmour, boolean hasShield
            return (Entity) new Player(newPos,entity.getHealth(), entity.getAttackdmg(),
            entity.getHasArmour(), entity.getHasShield(), entity.getId());
        }
        if (entity.getType().equals("wall")) {
            return (Entity) new Wall(newPos, entity.getId());
        }
        if (entity.getType().equals("sun_stone")) {
            return (Entity) new SunStone(newPos, entity.getId());
        }
        if (entity.getType().equals("exit")) {
            return (Entity) new Exit(newPos, entity.getId());
        }
        if (entity.getType().equals("boulder")) {
            return (Entity) new Boulder(newPos, entity.getId());
        }
        if (entity.getType().equals("switch")) {
            return (Entity) new FloorSwitch(newPos, entity.getId());
        }
        if (entity.getType().equals("door")) {
            return (Entity) new Door(newPos, entity.getKey(), entity.getId());
        }
        if (entity.getType().equals("portal")) {
            return (Entity) new Portal(newPos, entity.getColour(), entity.getId());
        }
        if (entity.getType().equals("zombie_toast_spawner")) {
            return (Entity) new ZombieToastSpawner(newPos, entity.getId());
        }
        if (entity.getType().equals("spider")) {
            return (Entity) new Spider(newPos, entity.getId(), entity.getHealth());
        }
        if (entity.getType().equals("zombie_toast")) {
            return (Entity) new Zombie(newPos, entity.getId(), entity.getHealth(), entity.getHasArmour());
        }
        if (entity.getType().equals("mercenary")) {
            return (Entity) new Mercenary(newPos, entity.getId(), entity.getHealth(), entity.getHasArmour());
        }
        if (entity.getType().equals("assassin")) {
            return (Entity) new Assassin(newPos, entity.getId(), entity.getHealth(), entity.getHasArmour());
        }
        if (entity.getType().equals("hydra")) {
            return (Entity) new Hydra(newPos, entity.getId(), entity.getHealth(), entity.getHasArmour());
        }
        if (entity.getType().equals("treasure")) {
            return (Entity) new Treasure(newPos, "treasure", entity.getId());
        }
        if (entity.getType().equals("key")) {
            return (Entity) new Key(newPos, entity.getKey(), entity.getId());
        }
        if (entity.getType().equals("health_potion")) {
            return (Entity) new HealthPotion(newPos, entity.getId());
        }
        if (entity.getType().equals("invincibility_potion")) {
            return (Entity) new InvincibilityPotion(newPos, entity.getId());
        }
        if (entity.getType().equals("invisibility_potion")) {
            return (Entity) new InvisibilityPotion(newPos, entity.getId());
        }
        if (entity.getType().equals("wood")) {
            return (Entity) new Wood(newPos, entity.getId());
        }
        if (entity.getType().equals("arrow")) {
            return (Entity) new Arrow(newPos, entity.getId());
        }
        if (entity.getType().equals("bomb")) {
            return (Entity) new Bomb(newPos, entity.getId());
        }
        if (entity.getType().equals("sword")) {
            return (Entity) new Sword(newPos, entity.getId());
        }
        if (entity.getType().equals("armour")) {
            return (Entity) new Armour(newPos, entity.getId());
        }
        if (entity.getType().equals("one_ring")) {
            return (Entity) new TheOneRing(newPos, entity.getId());
        }
        if (entity.getType().equals("anduril")) {
            return (Entity) new Anduril(newPos, entity.getId());
        }
        if (entity.getType().equals("bow")) {
            return (Entity) new Bow(entity.getId());
        }
        if (entity.getType().equals("shield")) {
            return (Entity) new Shield(entity.getId());
        }
        if (entity.getType().equals("midnight_armour")) {
            return (Entity) new MidnightArmour(entity.getId());
        }
        if (entity.getType().equals("wire")) {
            return (Entity) new Wire(newPos, entity.getId());
        }
        if (entity.getType().contains("light_bulb") ) {
            return (Entity) new LightBulb(newPos, entity.getId(), entity.getType(), entity.getLogic());
        }
        if (entity.getType().equals("switch_door")) {
            return (Entity) new SwitchDoor(newPos, entity.getId(), entity.getLogic());
        }
        if (entity.getType().equals("sceptre")) {
            return (Entity) new Sceptre(entity.getId());
        }
        if (entity.getType().equals("swamp_tile")) {
            return (Entity) new SwampTile(newPos, entity.getMovementFactor(), entity.getId());
        }
        return null;
    }


    /**
     * spawning a spider
     * @param pos
     * @return
     */
    static public Entity getSpider(Position pos) {
        return (Entity) new Spider(pos, "-100", -1);
    }
    /**
     * spawning a zombie
     * @param pos
     * @return
     */
    static public Entity getZombie(Position pos) {
        return (Entity) new Zombie(pos, "-100", -1);
    }
    /**
     * spawns an assassin
     * @return
     */
    static public Entity getAssassin() {
        return (Entity) new Assassin(GameFile.getStartingPos(), "-100", -1);
    }
    /**
     * spawns a mercenary
     * @return
     */
    static public Entity getMercenary() {
        return (Entity) new Mercenary(GameFile.getStartingPos(), "-100", -1);
    }
    /**
     * spawns a mercenary
     * @return
     */
    static public Entity getHydra(Position pos) {
        return (Entity) new Hydra(pos, "-100", -1);
    }
}