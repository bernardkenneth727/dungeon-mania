package dungeonmania.entities;

import java.util.List;

import dungeonmania.entities.itemEntity.collectableEntity.ElectricItem;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Position;

public class FloorSwitch extends Entity implements ElectricItem {

    private boolean isOn;

    /**
     * constructor for floor switch
     * @param pos
     */
    public FloorSwitch(Position pos, String id) {
        super(pos, "switch", false, id);
    }


    @Override
    public boolean getIsOn() {
        // returns if activated 
        List<Entity> ent = GameFile.findEntityFromPos(this.getPosition(), GameFile.getEntities());

        for (Entity boulder : ent) {
            if(boulder instanceof Boulder) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void setState(GameFile game) {
        List<Entity> ent = GameFile.findEntityFromPos(this.getPosition(), GameFile.getEntities());

        for (Entity boulder : ent) {
            if(boulder instanceof Boulder) {
                isOn = true;
            }
        }
        isOn = false;
    }
}