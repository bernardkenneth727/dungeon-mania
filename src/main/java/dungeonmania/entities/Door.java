package dungeonmania.entities;

import dungeonmania.util.*;

import org.json.JSONObject;

import dungeonmania.entities.enemy.Spider;
import dungeonmania.entities.itemEntity.ItemEntity;
import dungeonmania.entities.itemEntity.collectableEntity.Key;

public class Door extends Entity implements Collidable{
    protected boolean isOpen = false;
    private int key;
    /**
     * constructor for door
     * @param x
     * @param y
     * @param key is the pair with a key
     */
    public Door(Position pos, int key, String id) {
        super(pos, "door", false, id);
        this.isOpen = false;
        this.key = key;
    }

    /**
     * @return boolean of if the door is open
     */
    public boolean getIsOpen() {
        return this.isOpen;
    }
    /**
     * @return key
     */
    public int getKey() {
        return key;
    }

    public void setOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }
    /**
     * Checks if the player has the Key to this Door in their inventory and if so, returns the Key.
     * @param inventory (Inventory) - The player's inventory.
     * @return Key - Returns the Key if the player has it, otherwise returns null.
     */
    public Key hasKey(Inventory inventory) {
        // Filters out items that are not keys and do not have the key id that this door has and returns the first match.
        for (ItemEntity item : inventory.getAllItems()) {
            if (item instanceof Key) {
                Key key = (Key) item;
                if (key.getKey() == this.key) {
                    return (Key) item;
                } 
            }
        }
        return null;
    }
    
    /**
     * Sets the isOpen parameter of the door to be true if the player has the correct key, and removes the key from the player's inventory.
     * @param inventory (Inventory) - The player's inventory.
     */
    public boolean tryOpen(Inventory inventory) {
        Key doorKey = hasKey(inventory);
        
        if (this.isOpen) {
            return true;
        } else if (doorKey != null) {
            this.isOpen = true;
            inventory.useItem(doorKey);
            this.setType("door_unlocked");
            return true;
        } else if (this.isOpen) {
            return true;
        // If the player has a sun stone, they can also go through the door without consuming the stone.
        } else if (inventory.numberOf("sun_stone") >= 1) {
            this.isOpen = true; 
            this.setType("door_unlocked");
            return true; 
        }

        return false;
    }

    public boolean canMoveOnto(Entity ent) {
        if (ent instanceof Player) {
            Player player = (Player) ent;
            return this.tryOpen(player.getInventory());
        }else if (ent instanceof Spider){
            return true;
        } else {
            return getIsOpen();
        }
    }

    /**
     * @return door as a JSON Object
     */
    @Override
    public JSONObject getEntAsJSON() {
        
        JSONObject entJSON = new JSONObject();
        entJSON.put("x", this.getX()); 
        entJSON.put("y", this.getY());
        entJSON.put("type", this.getType());
        entJSON.put("key", this.key);
        entJSON.put("id", this.getId());
        
        return entJSON;
    }
    
}
