package dungeonmania.entities;

import java.util.List;

import dungeonmania.entities.itemEntity.collectableEntity.ElectricItem;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Position;

public class Wire extends Entity implements ElectricItem{

    private boolean isOn; 

    /**
     * constructor for Wire
     * @param pos
     * @param id
     */
    public Wire(Position pos, String id) {
        super(pos, "wire", false, id);
        this.isOn = false; 
    }
    
    @Override
    public boolean getIsOn() {
        return this.isOn;
    }

    /**
     * method called by boulder
     * @param entities
     */
    public void setWireOn(List<Entity> entities) {
        this.isOn = true; 
    
        //set wires in adjacent cells on 
        for (Entity e : entities) {
            if (Position.isAdjacent(e.getPosition(), getPosition()) && e.getType().equals("wire")) {
                Wire w = (Wire) e; 
                if (w.getIsOn() == false) {
                    w.setWireOn(entities);
                }
            }
        } 
    }

    /**
     * method called by boulder
     * @param entities
     */
    public void setWireOff(List<Entity> entities) {
        this.isOn = false; 

        //set wires in adjacent cells off
        for (Entity e : entities) {
            if (Position.isAdjacent(e.getPosition(), getPosition()) && e.getType().equals("wire")) {
                Wire w = (Wire) e; 
                if (w.getIsOn() == true) {
                    w.setWireOff(entities);
                }
            }
        } 
    }

    @Override
    public void setState(GameFile game) {
    }
}



    
    
    
    

