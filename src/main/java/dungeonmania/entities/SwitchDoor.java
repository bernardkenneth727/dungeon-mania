package dungeonmania.entities;

import java.util.ArrayList;
import java.util.List;

import dungeonmania.entities.enemy.Spider;
import dungeonmania.entities.itemEntity.collectableEntity.ElectricItem;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Position;

public class SwitchDoor extends Door implements ElectricItem {

    private boolean isOn;
    private String logic;
    private int previouslyActivated = 0;

    /**
     * Constructor for switch door
     * @param pos
     * @param key
     * @param id
     * @param logic
     */
    public SwitchDoor(Position pos, String id, String logic) {
        super(pos, -100, id);
        this.isOn = false;
        this.logic = logic;
        this.setType("switch_door");
    }

    @Override 
    public boolean getIsOn() {
        return isOn;
    }

    /**
     * sets the type to be on or off
     * @param listEntities
     */
    @Override
    public void tick(List<Entity> listEntities) {
        if (checkLogicTrue(listEntities)) {
            this.isOn = true;
            this.setOpen(true);
        } 
        else {
            this.isOn = false;
            this.setOpen(false);
        }
    }
    
    /**
     * @param listEntities
     * @return list of wires that are adjacent to the door switch
     */
    public List<ElectricItem> getWiresAdjacent(List<Entity> listEntities) {

        List<ElectricItem> listAdjacent = new ArrayList<ElectricItem>();
        for (Entity ent : listEntities) {
            if (Position.isAdjacent(this.getPosition(), ent.getPosition()) && ent.getType().equals("wire")) {
                listAdjacent.add((ElectricItem) ent);
            }
            else if (this.getPosition().equals(ent.getPosition()) && ent.getType().equals("wire")){
                listAdjacent.add((ElectricItem)ent);
            }
            else if (Position.isAdjacent(this.getPosition(), ent.getPosition()) && ent.getType().equals("switch")) {
                listAdjacent.add((ElectricItem)ent);
            }
            else if (this.getPosition().equals(ent.getPosition()) && ent.getType().equals("switch")){
                listAdjacent.add((ElectricItem)ent);
            }
        }
        return listAdjacent;
    }

    /**
     * @param wiresAdjacent
     * @return number of wires that are active and adjacent
     */
    public int numActiveAdjacentWires(List<ElectricItem> wiresAdjacent) {
        int active = 0;
        for (ElectricItem ent : wiresAdjacent) {
            if (ent.getIsOn()) {
                active++;
            }
        }
        return active;
    }
    /**
     * @param listEntities
     * @return checks if the logic has passed or not 
     */
    public boolean checkLogicTrue(List<Entity> listEntities) {

        boolean logicPassed = false;
        List<ElectricItem> wiresAdjacent = getWiresAdjacent(listEntities);
        int activeAdjacentWires = numActiveAdjacentWires(wiresAdjacent);

        switch (logic) {
            case "AND" : 
                if (activeAdjacentWires == wiresAdjacent.size()) {
                    logicPassed = true;
                }
                break;
            case "OR" : 
                if (activeAdjacentWires >= 1) {
                    logicPassed = true;
                }
                break;
            case "XOR" : 
                if (activeAdjacentWires == 1) {
                    logicPassed = true;
                }
                break;
            case "NOT" : 
                if (activeAdjacentWires == 0) {
                    logicPassed = true;
                }
                break;
            case "CO_AND" :
                // check if the prev Activated Wires is 2 less than the current active adjacent wires
                if (activeAdjacentWires - previouslyActivated >= 2){
                    logicPassed = true;
                }
                else if (this.isOn && activeAdjacentWires >= 2) {
                    logicPassed = true;
                }
                break;
        }
        previouslyActivated = activeAdjacentWires;
        return logicPassed;
    }

    @Override
    public void setState(GameFile game) {
        List<Entity> entities = GameFile.getEntities(); 

        for (Entity e : entities) {
            if (Position.isAdjacent(e.getPosition(), getPosition()) && e.getType().equals("wire")) {
                Wire w = (Wire) e; 
                if (w.getIsOn() == true) {
                    this.isOpen = true; 
                    break; 
                }
            }
        } 
        
        this.isOpen = false;  
    }


    public boolean canMoveOnto(Entity ent) {
        if (ent instanceof Player) {
            Player player = (Player) ent;
            return this.tryOpen(player.getInventory());
        }else if (ent instanceof Spider){
            return true;
        } else {
            return getIsOpen();
        }
    }
    
}
