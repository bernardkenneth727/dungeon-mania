package dungeonmania.gameFile;

import dungeonmania.response.models.*;
import dungeonmania.EnemySpawner;
import dungeonmania.entities.*;
import dungeonmania.entities.itemEntity.ItemEntity;
import dungeonmania.entities.itemEntity.buildableEntity.Buildable;
import dungeonmania.entities.itemEntity.buildableEntity.BuildableFactory;
import dungeonmania.entities.itemEntity.collectableEntity.Key;
import dungeonmania.entities.itemEntity.collectableEntity.ElectricItem;
import dungeonmania.entities.itemEntity.collectableEntity.weapon.Bomb;
import dungeonmania.exceptions.InvalidActionException;
import dungeonmania.gameFile.goals.GoalController;
import dungeonmania.util.*;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Arrays;

public class GameFile {

    private String dungeonId;
    private String dungeonName;
    private static List<Entity> allEntities;
    private List<String> usedItems = new ArrayList<String>();
    private List<AnimationQueue> animations;
    private String gameMode;
    
    private static Position startingPos;

    private EnemySpawner enemySpawner = new EnemySpawner();

    private GoalController goal;
    private String goalString;

    /**
     * Constructor for the GameFile
     * @param dungeonName
     * @param dungeonId
     */
    public GameFile(String dungeonName, String dungeonId, String gameMode, Data data) {
        
        this.dungeonId = dungeonId;
        this.dungeonName = dungeonName;
        this.gameMode = gameMode;
        // create new instances of entity so that we can have the entityResponse
        allEntities = this.getEntitiesFromData(data);
        // get the goals as a string
        if (data.getGoal() != null) {
            this.goal = new GoalController(data.getGoal().getGoal(), data.getGoal().getSubgoals(), this);
            if (this.goal.isGoalComplete()) {
                this.goalString = "";
            } else {
                this.goalString = goal.toString(); 
            }
        } else {
            this.goal = new GoalController("no goal", null, this);
        }

        this.animations = new ArrayList<AnimationQueue>();

        startingPos = getPlayer(allEntities).getPosition();
        //change electric state
        changeElectricState();
    }

    /**
     * for loading a game that already has a bunch of info 
     * @param dungeonId
     * @param data
     */
    public GameFile(String dungeonId, Data data) {
        this.dungeonId = dungeonId;
        this.dungeonName = data.getDungeonName();
        // create new instances of entity so that we can have the entityResponse
        allEntities = loadEntitiesFromData(data);
        // set the inventory for the player
        Player player = (Player) getPlayer(allEntities);
        player.setInventoryFromData(data);

        // get the goals as a string
        if (data.getGoal() != null) {
            this.goal = new GoalController(data.getGoal().getGoal(), data.getGoal().getSubgoals(), this);
            if (this.goal.isGoalComplete()) {
                this.goalString = "";
            } else {
                this.goalString = goal.toString(); 
            }
        } else {
            this.goal = new GoalController("no goal", null, this);
        }
        this.animations = new ArrayList<AnimationQueue>();
        gameMode = data.getGameMode();
        startingPos = new Position(data.getStart_x(), data.getStart_y());
        //change electric state
        changeElectricState();
    }

    /**
     * For generating a game with the generateDungeon method, given entities made in the DungeonGeneration class and the goal will always be exit.
     * @param entities (List)
     * @param gameMode (String)
     */
    public GameFile(List<Entity> entities, String gameMode) {
        this.dungeonId = "GeneratedDungeon";
        this.dungeonName = "GeneratedDungeon";
        this.gameMode = gameMode;
        
        setAllEntities(entities);

        this.goal = new GoalController("exit", null, this);
        this.goalString = goal.toString();
        
        this.animations = new ArrayList<AnimationQueue>();
        startingPos = getPlayer(allEntities).getPosition();
    }

    /**
     * @return the game as a dungeonResponse
     */
    public DungeonResponse getDungeon() {
        List<EntityResponse> entityResponses = getListEntityResponse();
        List<ItemResponse> itemResponses = getListItemResponse();

        return new DungeonResponse(dungeonId, dungeonName, 
        entityResponses, itemResponses, fixListBuildables(), goalString, animations);
    }

    /**
     * @return list of entities as class Entity
     */
    public static List<Entity> getEntities() {
        return allEntities;
    }
    
    /**
     * @return list of entities after being constructed properly with a response
     * will only be used in GameFile
     */
    private List<Entity> getEntitiesFromData(Data data) { 
        Entity.resetIdCounter();
        List<Entity> newList = new ArrayList<Entity>();
        for (EntityData ent : data.getEntities()) {
            newList.add(EntityFactory.getEntity(ent, this));
        }
        return newList;
    }
    /**
     * @return list of entities after being constructed properly with a response
     * will only be used in GameFile
     */
    private List<Entity> loadEntitiesFromData(Data data) { 
        // set the idCounter to the highest existing id
        //Entity.resetIdCounter();
        List<Entity> newList = new ArrayList<Entity>();
        for (EntityData ent : data.getEntities()) {
            newList.add(EntityFactory.loadEntity(ent));
        }
        return newList;
    }

    /**
     *  THIS METHOD WILL ONLY BE CALLED TO MAKE A DUNGEON RESPONSE
     * @return the inventory list as an itemResponse
     */
    public List<ItemResponse> getListItemResponse() {
        List<ItemResponse> itemResponses = new ArrayList<ItemResponse>();
        if (getInventory() == null) {
            return itemResponses;
        }
        for (ItemEntity ent : getInventory().getAllItems()) {
            ItemResponse response = new ItemResponse(ent.getId(), ent.getType());
            itemResponses.add(response);
        }
        return itemResponses;
    }

    /**
     * THIS METHOD WILL ONLY BE CALLED TO MAKE A DUNGEON RESPONSE 
     * @return list of entity responses 
     */
    public List<EntityResponse> getListEntityResponse() {
        List<EntityResponse> entResponses = new ArrayList<EntityResponse>();
        for (Entity ent : allEntities) {
            EntityResponse response = new EntityResponse(ent.getId(), ent.getType(), ent.getPosition(), ent.getIsInteractable());
            entResponses.add(response);
        }
        return entResponses;
    }

    public static void setAllEntities(List<Entity> allEntities) {
        GameFile.allEntities = allEntities;
    }
    /**
     * sets dungeonId
     * @param dungeonId
     */
    public void setDungeonId(String dungeonId) {
        this.dungeonId = dungeonId;
    }
    /**
     * @return dunegone Name
     */
    public String getDungeonName() {
        return dungeonName;
    }
    /**
     * @return gameMode
     */
    public String getGameMode() {
        return this.gameMode;
    }
    /**
     * @return dunegonId
     */
    public String getDungeonId() {
        return dungeonId;
    }
    /**
     * @return goals as a string
     */
    public String getGoals() {
        return goal.toString();
    }
    /**
     * @return goals as a GoalController
     */
    public GoalController getGoal() {
        return goal;
    }
    /**
     * @param entity
     */
    public void removeEntity(Entity entity) {
        allEntities.remove(entity);
    }
    /**
     * @return player starting Pos
     */
    public static Position getStartingPos() {
        return startingPos;
    }
    /**
     * @param id
     * @return the position of wanted entity
     */
    public Position getPositionEntity(String id) {
        //go into the list of entities and get the positions
        for (Entity entity : allEntities) {
            if (entity.getId().equals(id)) {
                return entity.getPosition();
            }
        }
        return null;
    }

    /**
     * @return true if the goal has been completed
     */
    public boolean checkGoalComplete() {
        if (goal.getGoalDefined() == null) {
            return false;
        }
        return goal.getGoalDefined().isComplete();
    }

    /**
     * @return inventory 
     * mainly used in testing
     */
    public Inventory getInventory() {
        if (getPlayer(allEntities) != null) {
            return ((Player) getPlayer(allEntities)).getInventory();
        }
        return null;
    }
    public static void entityHasInstanceOfEnemy() {

    }

    /**
     * @return entity at the positon. If entity cannot be found, a entity of type none will be returned.
     * Check the entity isnt on there
     */
    public Entity SingleEntityFromPos(Position pos) {
        for (Entity entity: allEntities) {
            if (entity.getPosition().equals(pos) && !entity.getType().equals("player")){
                return entity;
            }
        }  
        return null;
    }
    /**  
     * calls tick and updates everything regarding entities 
     * player uses an item (potion)
     * @param itemUsed
     */
    public void tick(String itemUsed) throws IllegalArgumentException, InvalidActionException {
        List<String> useableItems = new ArrayList<>(Arrays.asList("bomb", "health_potion", "invincibility_potion", "invisibility_potion"));

        // If the item is not in the player's inventory but on the map floor, then raises an invalid action exception.
        if (allEntities.stream().anyMatch(e -> e.getId().equals(itemUsed))) {
            throw new InvalidActionException("The item is not in the player's inventory.");
        }
        // use a potion 
        if (usedItems.size() > 0 && usedItems.contains(itemUsed)) {
            throw new InvalidActionException("Already used");
        }

        Player entPlayer = (Player) getPlayer(allEntities);
        ItemEntity item = getInventory().findItem(itemUsed);
        if (item == null) {
            throw new IllegalArgumentException("Item ID invalid");
        // If the Id is valid, but the item id does not correspond to a potion or bomb, then an IllegalArgumentException is thrown.
        } else if (!useableItems.contains(item.getType())) {
            throw new IllegalArgumentException("Item ID does not correspond to a useable item.");
        }

        if (item.getType().equals("bomb")) {
            entPlayer.tickBomb(itemUsed, allEntities);
        } else {
            entPlayer.tick(itemUsed);
        }

        usedItems.add(itemUsed);

        // only the movable entities should change when calling tick method
        tickEntities();
        // check for any buildable stuff and add to list of buildables 
        fixListBuildables();
        // check if the goal is complete 
        if (goal.isGoalComplete()) {
            goalString = "";
        }
    }

    /**
     * calls tick and updates everything regarding entities
     * moves the player
     * @param movementDirection
     */
    public void tick(Direction movementDirection) {
        Player player = (Player) getPlayer(allEntities);
        // move in a direction 
        player.tick(movementDirection, allEntities, this.gameMode); // this might have to be entPlayer so there is a tick in entity
        //battle after player move on top of enemy
        player.checkEnemyOnPlayer(allEntities, gameMode);
        // only the movable entities should change when calling tick method and only if player is still alive
        Player playerEnt = (Player) getPlayer(allEntities);
        if (playerEnt != null) {
            tickEntities();
        }
        // check for any buildable stuff and add to list of buildables 

        if (getPlayer(allEntities) != null) {
            fixListBuildables();

            // check if the goal is complete 
            if (goal != null && goal.isGoalComplete()) {
                goalString = "";
            }
        }
        //change electric state
        changeElectricState();
    }
    /**
     * @param type
     * @return the entity needed 
     */
    public Entity getEnt(String id) {
    //should be list as multiple items
        for (Entity ent : allEntities){
            if (ent.getId().equals(id)) {
                return ent;
            }
        }
        return null;
    }
    
    /**
     * @return the player
     */
    public static Entity getPlayer(List<Entity> entities) {
        for (Entity ent : entities){
            if (ent.getType().equals("player")) {
                return ent;
            }
        }
        return null;
    }

    /**
     * @return entity at the positon. If entity cannot be found, an empty list is returned 
     * Check the entity isnt on there
     */
    public static List<Entity> findEntityFromPos(Position pos, List<Entity> entities) {
        List<Entity> enstInPos = new ArrayList<Entity>();
        if ((pos == null) || (entities == null)) {
            
        }
        for (Entity entity: entities) {
            if (entity.getType().equals("anduril")) {
                
            }
            if (entity.getPosition().equals(pos)){
                enstInPos.add(entity);
            }
        }   

        return enstInPos;
    }
    
    /**
     * moves the entities when tick is called
     */
    public void tickEntities() {
        // only the movable entities should change when calling tick method
        for (Entity ent : allEntities) {

            if (!ent.getType().equals("player") && !ent.getType().equals("zombie_toast_spawner")){
                ent.tick(allEntities);
            }
        }

        //Ticks for zombie toaster
        List<Entity> toasters = allEntities.stream().filter(p -> p.getType().equals("zombie_toast_spawner")).collect(Collectors.toList());
        for (Entity spawner : toasters) {
            spawner.tick(allEntities, gameMode);
        }


        Player playerEnt = (Player) getPlayer(allEntities);
        if (playerEnt != null) {
            playerEnt.checkEnemyOnPlayer(allEntities, this.gameMode);
            
        }
        
        // Ticks the spider spawner, checking if should spawn spiders and if so, spawns them.
        //spawningSpiders.tick(this);
        enemySpawner.tick(this);
        
        // Checking for bombs, if detonate them if triggered and then add all exploded entities to 
        // a list which will be removed after the for loop to avoid concurrency issues.
        List<Entity> explodedEntities = new ArrayList<>();
        for (Entity ent : allEntities) {
            if (ent instanceof Bomb) {
                explodedEntities.addAll(((Bomb)ent).detonate(this));
            }
        }

        allEntities.removeAll(explodedEntities);
    }

    /**
     * @return a list of buildable items in a string
     */
    public List<String> fixListBuildables() {
        List<String> listBuild = new ArrayList<String>();
        if (getPlayer(allEntities) == null) {
            return listBuild;
        }
        // Gets a map of all the Buildables with their type as well as the string, 
        // goes through each entry and checks if the Buildable can build and if so, 
        // adds the type name into the listBuild list.
        Map<String, Buildable> allBuildables = BuildableFactory.getAllBuildablesMap();
        for (Map.Entry<String, Buildable> entry : allBuildables.entrySet()) {
            if (entry.getValue().canBuild(getInventory(), getEntities())) {
                listBuild.add(entry.getKey());
            }
        }

        return listBuild;
    }

    /**
     * method to change state of electric items each tick
     */
    public void changeElectricState() {
        List<Entity> allEntities = getEntities(); 
        for (Entity entity : allEntities) {
            if (entity instanceof ElectricItem) {
                ElectricItem elec = (ElectricItem) entity; 
                elec.setState(this); 
            }
        }
    }

    /**
     * @return enmtities as a JSONArray 
     */
    public JSONArray getEntitiesAsJSON() {

        JSONArray entitiesJSON = new JSONArray();

        for (Entity ent : allEntities) {

            JSONObject entJSON = ent.getEntAsJSON();

            entitiesJSON.put(entJSON);
        }
        return entitiesJSON;
    }

    /**
     * @return the inventory as a JSONArray
     */
    public JSONArray getInventoryAsJSON() {
        JSONArray inventoryJSON = new JSONArray();

        for (ItemEntity ent : getInventory().getAllItems()) {
            JSONObject itemJSON = new JSONObject();

            itemJSON.put("type", ent.getType());
            itemJSON.put("id", ent.getId());

            if (ent instanceof Key) {
                Key key = (Key) ent;
                itemJSON.put("key", key.getKey());
            }

            inventoryJSON.put(itemJSON);
        }

        return inventoryJSON;
    }

}

