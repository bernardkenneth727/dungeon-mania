package dungeonmania.gameFile.goals;

import java.util.List;

import dungeonmania.entities.Boulder;
import dungeonmania.entities.Entity;

public class BoulderGoal implements Goal{

    private List<Entity> entities;

	/**
	 * constructor for Boulder
	 * @param entities
	 */
    public BoulderGoal(List<Entity> entities) {
        this.entities = entities;
    }

	/**
	 * Check whether switch goal is complete
	 * @return Value of whether goal is complete
	 */
	@Override
    public boolean isComplete() {
		for (Entity entity : entities) {
			if (entity instanceof Boulder) {
				Boulder boulder = (Boulder) entity;
				if (!boulder.isOnSwitch()) {
					return false;
				}
			}
		}
		return true;
	}
    
}
