package dungeonmania.gameFile.goals;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import dungeonmania.gameFile.GameFile;

public class GoalController {
    private String goal; 
    private List<GoalController> subgoals = new ArrayList<GoalController>();
    private Goal goalDefined;

    /**
     * constructor for the goalController
     * @param goal 
     * @param subgoals
     * @param game
     */
    public GoalController(String goal, List<GoalController> subgoals, GameFile game) {
        this.goal = goal;
        this.subgoals = subgoals;
        this.goalDefined = setGoalsAsGoals(game);
    }

    /**
     * returns the goals as interface Goals 
     * Makes it easier to check for goal completion later 
     */
    public Goal setGoalsAsGoals(GameFile game) {
        switch (goal) {
            case "AND" :
                List<Goal> andList = new ArrayList<Goal>();
                for (GoalController subgoal : subgoals) {
                    Goal g = subgoal.setGoalsAsGoals(game);
                    andList.add(g);
                }
                AndComposite andGoal = new AndComposite(andList);
                goalDefined = (Goal) andGoal;
                break;
            case "OR" :
                List<Goal> orList = new ArrayList<Goal>();
                for (GoalController subgoal : subgoals) {
                    Goal g = subgoal.setGoalsAsGoals(game);
                    orList.add(g);
                }
                OrComposite orGoal = new OrComposite(orList);
                goalDefined = (Goal) orGoal;
                break;
            case "boulders" :
                BoulderGoal boulderGoal = new BoulderGoal(GameFile.getEntities());
                goalDefined = (Goal) boulderGoal;
                break;
            case "enemies" :
                DestroyGoal destroyGoal = new DestroyGoal(GameFile.getEntities());
                goalDefined = (Goal) destroyGoal;
                break;
            case "treasure" :
                TreasureGoal treasureGoal = new TreasureGoal(GameFile.getEntities());
                goalDefined = (Goal) treasureGoal;
                break;
            case "exit" :
                ExitGoal exitGoal = new ExitGoal(GameFile.getEntities());
                goalDefined = (Goal) exitGoal;
                break;    
        }
        return goalDefined;
    }

    public boolean isGoalComplete() {
        if (goalDefined != null && goalDefined.isComplete()) {
            return true;
        }
        return false;
    }
    
    /**
     * @return goal as a string
     */
    public String getGoal() {
        return goal;
    }
    /**
     * @return list of subgoals 
     */
    public List<GoalController> getSubgoals() {
        return subgoals;
    }
    /**
     * returns the goal as a Goal
     */
    public Goal getGoalDefined() {
        return goalDefined;
    }

    @Override
    public String toString() {
        String str = "";
        if (this.goal.equals("no goal")) {
            return "there is no goal";
        }
        // if there is an AND or OR composite 
        if (subgoals != null) {
            // there will only be at most 2 subgoals per goal.
            str = str + getSubGoalAsString(str);
        }
        else { // the single goal
            str = str + ":"+ this.goal;
        }
        if (goalDefined.isComplete()) {
            return str;
        }
        return str;
    }

    /**
     * @param str
     * @return SUBGOALS AS A STRINg
     */
    public String getSubGoalAsString(String str) {
        // check if there are more subgoals in index 0
        if (subgoals.get(0).getSubgoals() != null) {
            //add the string for subgoal 0 into the str
            str = subgoals.get(0).getSubGoalAsString(str);
        } else {
            str = str + ":"+ subgoals.get(0).getGoal();
        }
        // add on either the AND or OR composite
        str = str + " "+ this.goal + " ";
        // check if there is more subgoals in index 1
        if (subgoals.get(1).getSubgoals() != null) {
            // repeat the process
            str = subgoals.get(1).getSubGoalAsString(str);
        } else { // if not just add the goal onto the end of the string
            str = str + ":" + subgoals.get(1).getGoal();
        }
        return str;
    }

    /**
     * @return Goal as a JSON Object
     */
    public JSONObject getGoalJSON() {
        JSONObject goalJSON = new JSONObject();

        // add each goal onto the JSON Object 
        goalJSON.put("goal", this.goal);
        
        if (this.subgoals != null) {
            goalJSON.put("subgoals", getSubGoalJSON());
        }
        return goalJSON;
    }

    /**
     * @return the subhoals as a JSONArray
     */
    public JSONArray getSubGoalJSON() {
        
        JSONArray array = new JSONArray();

        array.put(subgoals.get(0).getGoalJSON());
        array.put(subgoals.get(1).getGoalJSON());

        return array;
    }

}
