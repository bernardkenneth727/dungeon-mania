package dungeonmania.gameFile.goals;

import java.util.List;

import dungeonmania.entities.*;
import dungeonmania.entities.enemy.Enemy;

public class DestroyGoal implements Goal{

    private List<Entity> entities;

	/**
	 * Constructor for destroy Goal
	 * @param entities
	 */
    public DestroyGoal(List<Entity> entities) {
        this.entities = entities;
    }

	/**
	 * Check whether Enemy goal is complete
	 * @return Value of whether goal is complete
	 */
	@Override
    public boolean isComplete() {
		for (Entity entity : entities) {
			if (entity instanceof Enemy) {
				return false;
			}
		}
		return true;
	}
    
}
