package dungeonmania.gameFile.goals;

import java.util.List;

import dungeonmania.entities.Entity;
import dungeonmania.entities.itemEntity.collectableEntity.Treasure;

public class TreasureGoal implements Goal{

    private List<Entity> entities;

	/**
	 * Constructor for treasure Goal
	 * @param entities
	 */
    public TreasureGoal(List<Entity> entities) {
        this.entities = entities;
    }

	/**
	 * Check whether Treasure goal is complete
	 * @return Value of whether goal is complete
	 */
	@Override
    public boolean isComplete() {
		for (Entity entity : entities) {
			if (entity instanceof Treasure) {
				return false;
			}
		}
		return true;
	}
    
}
