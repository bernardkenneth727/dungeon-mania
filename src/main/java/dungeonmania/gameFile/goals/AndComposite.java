package dungeonmania.gameFile.goals;

import java.util.ArrayList;
import java.util.List;

public class AndComposite implements Goal{

    private List<Goal> goals = new ArrayList<Goal>();
    
    /**
     * constructor for AND composite
     * @param goals
     */
    public AndComposite(List<Goal> goals) {
        this.goals = goals;
    }

    /**
     * Check whether goals are complete
     * @return Value of whether goals are complete
     */
    public boolean isComplete() {
        for (Goal g: goals) {
            if (!g.isComplete()) {
                return false;
            } 
        }
        return true;
    }
    
}
