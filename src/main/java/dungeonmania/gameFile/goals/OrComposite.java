package dungeonmania.gameFile.goals;

import java.util.ArrayList;
import java.util.List;

public class OrComposite implements Goal{

    private List<Goal> goals = new ArrayList<Goal>();
    
    /** 
     * Constructor for OR composite
     * @param goals
    */
    public OrComposite(List<Goal> goals) {
        this.goals = goals;
    }

    /**
     * Check whether goals are complete
     * @return Value of whether goals are complete
     */
    public boolean isComplete() {
        for (Goal g: goals) {
            if (g.isComplete()) {
                return true;
            } 
        }
        return false;
    }
    
}
