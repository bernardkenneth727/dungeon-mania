package dungeonmania.gameFile.goals;

import java.util.List;

import dungeonmania.entities.*;

public class ExitGoal implements Goal{
    private List<Entity> entities; 

	/**
	 * constructor for exit Goal
	 * @param entities
	 */
    public ExitGoal(List<Entity> entities) {
        this.entities = entities;
    }

	/**
	 * Check whether Exit goal is complete
	 * @return Value of whether goal is complete
	 */
	@Override
    public boolean isComplete() {
		for (Entity entity : entities) {
			if (entity instanceof Exit) {
				Exit exit = (Exit) entity;
				if (exit.isExited()) {
					return true;
				}
			}
		}
		return false;
	}

    
}
