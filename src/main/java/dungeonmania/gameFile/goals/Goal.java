package dungeonmania.gameFile.goals;

public interface Goal {

    /**
     * @return bool for if the goal has been completed or not
     */
    boolean isComplete(); 
    
}
