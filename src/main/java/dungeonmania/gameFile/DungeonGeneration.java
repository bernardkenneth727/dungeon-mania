package dungeonmania.gameFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import dungeonmania.entities.Entity;
import dungeonmania.entities.Exit;
import dungeonmania.entities.Player;
import dungeonmania.entities.Wall;
import dungeonmania.util.Position;

/**
 * Generates a dungeon using a randomised version of Prim's Algorithm from the M3 Spec.
 */
public class DungeonGeneration {
    private Position start;
    private Position end;
    private String gameMode;
    private int width = 50;
    private int height = 50;

    private Random rand;
    private Map<Position, Boolean> maze;
    private List<Position> boundaries;

    /**
     * The constructor for DungeonGeneration.
     * @param xStart (int) - The x coordinate for start.
     * @param yStart (int) - The y coordinate for start.
     * @param xEnd (int) - The x coordinate for end.
     * @param yEnd (int) - The y coordinate for end.
     * @param gameMode (String)
     */
    public DungeonGeneration(int xStart, int yStart, int xEnd, int yEnd, String gameMode) {
        this.start = new Position(xStart, yStart);
        this.end = new Position(xEnd, yEnd);
        this.gameMode = gameMode;

        this.rand = new Random();
        this.maze = createMaze();
        this.boundaries = getBoundaries();

    }

    /**
     * Creates the basic maze with all positions set to false (wall) as a Map with Position and Boolean.
     * @return Map - key is Position and value is Boolean.
     */
    private Map<Position, Boolean> createMaze() {
        // Generating the 2D array of booleans as a HashMap with Position and Boolean. They are all set to false corresponding to all walls.
        Map<Position, Boolean> maze = new HashMap<>();
        for (int i = 0; i <= this.width; i++) {
            for (int j = 0; j <= this.height; j++) {
                maze.put(new Position(i, j), false);
            }
        }

        return maze;
    }

    /**
     * Generates the dungeon using a maze of walls randomly each time. This follows the Randomised Prim's algorithm from the M3 Spec.
     * @return (Map) - Returns the map of the updated maze with Position as key and Boolean as value.
     */
    private void randomizedPrims() {
        // The start of the maze is empty.
        this.maze.replace(start, true);

        // options is a list of positions. Adding to options all neighbours of start position that are not on boundary that are of distance 2 and are walls.
        List<Position> options = addToListDistanceTwoAway(false, this.start);

        // While options is not empty.
        while (!options.isEmpty()) {
            // Gets and removes a random square from options and stores the position as next.
            Position next = options.remove(rand.nextInt(options.size()));

            // Neighbours is each neighbour of distance 2 from next not on boundary that is empty.
            List<Position> neighbours = addToListDistanceTwoAway(true, next);
            
            // If neighbours is not empty, picks a random neighbour from neighbours, sets next in maze to empty, sets the position between next and neighbour to empty and sets neighbour to empty.
            if (!neighbours.isEmpty()) {
                Position neighbour = neighbours.get(rand.nextInt(neighbours.size()));
                maze.replace(next, true);
                Position positionInBetweenNextNeighbour = getPositionInBetween(next, neighbour);
                maze.replace(positionInBetweenNextNeighbour, true);
                maze.replace(neighbour, true);
            }

            // Adding to options all neighbours of next that are not on boundary that are of distance 2 away and are walls.
            options.addAll(addToListDistanceTwoAway(false, next));

        }

        // If the end is a wall, then set it to empty.
        if (!this.maze.get(this.end)) {
            this.maze.replace(this.end, true);
            
            // neighbours are a list of Positions with a distance of 1 from the end.
            List<Position> neighbours = new ArrayList<>();
            for (Position pos : this.maze.keySet()) {
                if (pos.distanceBetween(pos, this.end) == 1) {
                    neighbours.add(pos);
                }
            }

            // Finds whether any neighbours are empty.
            boolean areNeighboursEmpty = false;
            for (Position neighbour : neighbours) {
                if (this.maze.get(neighbour)) {
                    areNeighboursEmpty = true;
                }
            }

            // If there are no empty neighbours, then makes a random neighbour empty.
            if (!areNeighboursEmpty) {
                Position neighbour = neighbours.get(rand.nextInt(neighbours.size()));
                this.maze.replace(neighbour, true);
            }
        }

    }

    /**
     * Gets a list of boundaries which are positions, either having x/y of 0 or x of width or y of height.
     * @return List - returns a list of positions.
     */
    private List<Position> getBoundaries() {
        List<Position> allPositions = new ArrayList<>(this.maze.keySet());
        List<Position> boundaries = new ArrayList<>();
        for (Position pos : allPositions) {
            if (pos.getX() == 0 || pos.getY() == 0 || pos.getX() == this.width || pos.getY() == this.height) {
                boundaries.add(pos);
            }
        }

        return boundaries;
    }

    /**
     * Returns true if a position is on a boundary.
     * @param position (Position) - The position to check.
     * @return boolean - true if the position is on the boundary.
     */
    private boolean isOnBoundary(Position position) {
        return this.boundaries.contains(position);
    }

    /**
     * Gets the position in between 2 positions separated by 1 position.
     * @param a (Position)
     * @param b (Position)
     * @return Position - returns the position that is between those 2 positions.
     * @pre Distance between a and b is 2.
     */
    private Position getPositionInBetween(Position a, Position b) {
        // Gets all the cardinally adjacent positions from a and checks if they are also adjacent to b.
        List<Position> adjacentToA = a.getCardinalPositions();
        for (Position pos : adjacentToA) {
            if (Position.isAdjacent(b, pos)) {
                return pos;
            }
        }

        return null;
    }

    /**
     * Adds to a list neighbours that are of distance 2 away from original that are not on the boundary and are empty or walls depending on the parameter.
     * @param isEmpty (boolean) - Will it add if they are empty or walls?
     * @param maze (Map) - The maze containing all positions and whether they are walls or empty.
     * @param original (Position) - The position to check from.
     * @return List - returns the list of neighbours that satisfy the parameters.
     */
    private List<Position> addToListDistanceTwoAway(boolean isEmpty, Position original) {
        List<Position> positions = new ArrayList<>();
        // Goes through each entry in the maze to check the conditions.
        for (Map.Entry<Position, Boolean> entry : this.maze.entrySet()) {
            Position entryPosition = entry.getKey();
            // If distance between entry position and original is 2, is not on boundary and either is empty or is a wall, then adds it to positions.
            if (isCardinalDistanceBetweenTwo(original, entryPosition) && !isOnBoundary(entryPosition) && entry.getValue() == isEmpty) {
                positions.add(entryPosition);
            }
        }

        return positions;

    }

    /**
     * Determines whether the cardinal distance between two positions is 2.
     * @param a (Position) - The first position.
     * @param b (Position) - The second position.
     * @return boolean - Returns true if their cardinal distance is 2.
     */
    private boolean isCardinalDistanceBetweenTwo(Position a, Position b) {
        int x = Math.abs(a.getX() - b.getX());
        int y = Math.abs(a.getY() - b.getY());
        return (x == 2 && y == 0) || (x == 0 && y == 2);
    }
    

    /**
     * Generates a GameFile for the dungeon using the randomisedPrims method and converts the booleans from true or false to wall or empty, adds the player and the exit and returns it as a GameFile.
     * @return GameFile
     */
    public GameFile generateGameFile() {
        randomizedPrims();
        List<Entity> entities = new ArrayList<>();

        // For each entry in the completedMaze, if they had a value of false, then they are now a wall.
        for (Map.Entry<Position, Boolean> entry : this.maze.entrySet()) {
            if (!entry.getValue()) {
                entities.add(new Wall(entry.getKey(), "-100"));
            }
        }

        // Adds the player with their correct health determined by the gameMode and at start position.
        if (this.gameMode.equals("hard")) {
            entities.add(new Player(this.start, "-100", 8));
        } else {
            entities.add(new Player(this.start, "-100", 10.11));
        }

        // Adds the exit at the end position.
        entities.add(new Exit(this.end, "-100"));
        
        return new GameFile(entities, this.gameMode);
    }
    
}
