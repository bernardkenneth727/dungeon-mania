package dungeonmania;

import java.util.List;
import java.util.Random;

import dungeonmania.entities.Entity;
import dungeonmania.entities.EntityFactory;
import dungeonmania.gameFile.GameFile;
import dungeonmania.util.Position;

public class EnemySpawner {
    private int tick;
    private int maxSpiders = 6;
    private int spawnMercenaryTick = 30;
    private int spawnHydraTick = 50;
    private int spawnSpiderTick = 25;

    public EnemySpawner() { 
        this.tick = 0;
    }

     /**
     * Counts the number of spiders in the current game.
     * @param game (GameFile) - The game currently being played.
     * @return int - Returns the number of spiders in game.
     */
    private int getSpiderCount(GameFile game) {
        // Filters out so there are only spiders and counts that amount, returning that.
        List<Entity> entitiesList = GameFile.getEntities();
        Long spiderCountLong = entitiesList
            .stream()
            .filter(e -> e.getType().equals("spider"))
            .count();
        return spiderCountLong.intValue();
    }

    /**
     * @param game
     * @return a random position to spawn the spider 
     */
    private Position getRandomSpawnPosition(GameFile game) {
        // Finds the player position.
        List<Entity> entitiesList = GameFile.getEntities();
        Position playerPosition = entitiesList
            .stream()
            .filter(e -> e.getType()
            .equals("player"))
            .findFirst()
            .get()
            .getPosition();

        // Returns a random position within 5 blocks of the player to spawn in.
        return playerPosition.getRandomPositionInRange(5);
    }

    /**
     * When the game ticks
     *      Spider spawns every 25 ticks
     *      Mercenary/assassin spawns every 30 ticks 
     *      Hydra spawns every 40 ticks 
     * @param game (GameFile) - The current game in progress.
     */
    public void tick(GameFile game) {
        this.tick++;

        // reset the counter so it does not get too high
        if (this.tick ==  600){
            this.tick = 0;
        }

        // If the tick is equal to or greater than the spawn tick, then sets the tick to 0.
        if (this.tick % this.spawnSpiderTick == 0) {
            // If there are less spiders currently in game than the maximum, then spawns a new spider in a random position adjacent to the player and places it in game.
            if (getSpiderCount(game) < this.maxSpiders) {
                Entity newSpider = EntityFactory.getSpider(getRandomSpawnPosition(game));
                GameFile.getEntities().add(newSpider);
            }
        }

        if (this.tick % this.spawnMercenaryTick == 0) {
            // cannot spawn if no enemy in map
            Random random = new Random();
            int randomNum = (int) random.nextInt(10);
            if (randomNum > 7) {
                Entity newAssassin = EntityFactory.getAssassin();
                GameFile.getEntities().add(newAssassin);
            }
            else {
                Entity newMercenary = EntityFactory.getMercenary();
                GameFile.getEntities().add(newMercenary);
            }
        }
        if (this.tick % this.spawnHydraTick == 0) {
            // check gameMode 
            if (game.getGameMode().equals("hard")) {
                // spawn the hydra 
                Entity newHydra = EntityFactory.getHydra(getRandomSpawnPosition(game));
                GameFile.getEntities().add(newHydra);
            }
        }
    }
}